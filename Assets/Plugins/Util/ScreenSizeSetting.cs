﻿using UnityEngine;
using System.Collections;

public class ScreenSizeSetting : MonoBehaviour {

	private static bool isInitedScreenSize = false;
	public static float ScreenRate = 0f;
	public static Vector2 ScreenSize;

	public string CameraName = "Main Camera";
	public string CanvasName = "Canvas";

	void Awake() {
		if (!isInitedScreenSize) {
			ScreenRate = 1.0f*Screen.height/Screen.width;
			if (ScreenRate > 1.5f) {
				ScreenSize = new Vector2(640f, 640f*ScreenRate);
			} else {
				ScreenSize = new Vector2(960f/ScreenRate, 960f);
			}
			isInitedScreenSize = true;
		}
	}

	// Use this for initialization
	void Start () {
		if (ScreenRate >= 1.5f) {
			float changed_rate = ScreenRate/1.5f;
			if(CanvasName != "")
			{
				GameObject canvas_obj = GameObject.Find(CanvasName);
				RectTransform canvas_trans = canvas_obj.GetComponent<RectTransform>();
				canvas_trans.position = new Vector3(320.0f*changed_rate, 480.0f*changed_rate, 0.0f);
				canvas_trans.sizeDelta = new Vector2(640.0f*changed_rate, 960.0f*changed_rate);
			}
			if(CameraName != "")
			{
				GameObject camera_obj = GameObject.Find(CameraName);
				Camera camera_camera = camera_obj.GetComponent<Camera>();
				camera_camera.orthographicSize = 480.0f*changed_rate;
				camera_obj.transform.position = new Vector3(320.0f*changed_rate, 480.0f*changed_rate, -10.0f);
			}
		} else {
			float changed_rate = 1.5f/ScreenRate;
			if(CanvasName != "")
			{
				GameObject canvas_obj = GameObject.Find(CanvasName);
				RectTransform canvas_trans = canvas_obj.GetComponent<RectTransform>();
				canvas_trans.position = new Vector3(320.0f*changed_rate, 480.0f, 0.0f);
//				canvas_trans.sizeDelta = new Vector2(640.0f*changed_rate, 960.0f*changed_rate);
				canvas_trans.sizeDelta = ScreenSize;
			}
			if(CameraName != "")
			{
				GameObject camera_obj = GameObject.Find(CameraName);
//				Camera camera_camera = camera_obj.GetComponent<Camera>();
//				camera_camera.orthographicSize = 480.0f*changed_rate;
				camera_obj.transform.position = new Vector3(320.0f*changed_rate, 480.0f, -10.0f);
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
