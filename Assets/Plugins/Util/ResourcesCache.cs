﻿using UnityEngine;
using System.Collections.Generic;
using System;

namespace BigJamLib {
    #region Singleton
	public partial class ResourcesCache : MonoBehaviour {
		private ActionManager _actionManager;
		private AudioSource _audioSource;

		void Awake() {
			DontDestroyOnLoad (this);
		}

		void Start() {
			_actionManager = GetComponent<ActionManager> ();
			_audioSource = GetComponent<AudioSource> ();
            _instance = this;
		}

		private static ResourcesCache _instance = null;
    }
    #endregion

    #region Sprite
    public interface SpriteCacheController {
        Sprite loadSprite(string tPath);
        Sprite loadSprite(string tPath1, string tPath2);
    }

    public partial class ResourcesCache : SpriteCacheController {
        public static SpriteCacheController Sprite
        {
            get {
//                #if DEBUG
//
//                if(_instance == null)
//                {
//                    GameObject newInstance = new GameObject();
//
//					_instance = newInstance.AddComponent<ResourcesCache>();
//					_instance._audioSource = newInstance.AddComponent<AudioSource>();
//                }
//
//                #endif

                return _instance;
            }
        }

		private Dictionary<int, Sprite> _spritesCache = new Dictionary<int, Sprite> ();
		public Sprite loadSprite(string tPath) {
			return _loadSprite (tPath);
		}

		private Sprite _loadSprite(string tPath) {
			int tHashCode = tPath.GetHashCode ();
			if (!_spritesCache.ContainsKey (tHashCode)) {
				_spritesCache.Add (tHashCode, Resources.Load<Sprite> (tPath));
			}

			return _spritesCache [tHashCode];
		}

		public Sprite loadSprite(string tPath1, string tPath2) {
			return _loadSprite (tPath1, tPath2);
		}

		private Sprite _loadSprite(string tPath1, string tPath2) {
			int tHashCode = (tPath1 + "/" + tPath2).GetHashCode ();
			if (!_spritesCache.ContainsKey (tHashCode)) {
				Sprite[] textures = Resources.LoadAll<Sprite> (tPath1);
				int tLength = textures.Length;
				for (int i = 0; i < tLength; ++i) {
					_spritesCache.Add ((tPath1 + "/" + textures [i].name).GetHashCode(), textures[i]);
				}
			}

			return _spritesCache [tHashCode];
		}
    }
	#endregion Sprite

	#region MonoBehaviour
	public interface MonoBehaviourCacheController {
        MonoBehaviour loadMonoBehaviour(string tPath);
        MonoBehaviour loadMonoBehaviour(int tPathHash);
	}

	public partial class ResourcesCache : MonoBehaviourCacheController {
		public static MonoBehaviourCacheController MonoBehaviour
		{
			get {
//                #if DEBUG
//
//                if(_instance == null)
//                {
//                    GameObject newInstance = new GameObject();
//
//					_instance = newInstance.AddComponent<ResourcesCache>();
//					_instance._audioSource = newInstance.AddComponent<AudioSource>();
//                }
//
//                #endif

				return _instance;
			}
		}

		private Dictionary<int, MonoBehaviour> _monoBehavioursCache = new Dictionary<int, MonoBehaviour> ();
		public MonoBehaviour loadMonoBehaviour(string tPath) {
			return _loadMonoBehaviour (tPath);
		}

        public MonoBehaviour loadMonoBehaviour(int tPathHash)
        {
            return _loadMonoBehaviour(tPathHash);
        }

		private MonoBehaviour _loadMonoBehaviour(string tPath) {
			int tHashCode = tPath.GetHashCode ();
			if (!_monoBehavioursCache.ContainsKey (tHashCode)) {
				MonoBehaviour tMono = Resources.Load<MonoBehaviour> (tPath);
				if (tMono == null) {
					throw new NullReferenceException("Invalid path! : " + tPath);
				}
				_monoBehavioursCache.Add (tHashCode, tMono);
			}

			return _monoBehavioursCache [tHashCode];
		}

        private MonoBehaviour _loadMonoBehaviour(int tPathHash) {
            if (!_monoBehavioursCache.ContainsKey (tPathHash)) {
                throw new NullReferenceException("Invalid reference object!");
            }

            return _monoBehavioursCache [tPathHash];
        }
	}
	#endregion MonoBehaviour

	#region GameObject
	public interface GameObjectCacheController {
		GameObject loadGameObject(string tPath);
		GameObject loadGameObject(int tPathHash);
	}

	public partial class ResourcesCache : GameObjectCacheController {
		public static GameObjectCacheController GameObject
		{
			get {
//				#if DEBUG
//
//				if(_instance == null)
//				{
//					GameObject newInstance = new GameObject();
//
//					_instance = newInstance.AddComponent<ResourcesCache>();
//					_instance._audioSource = newInstance.AddComponent<AudioSource>();
//				}
//
//				#endif

				return _instance;
			}
		}

		private Dictionary<int, GameObject> _gameObjectsCache = new Dictionary<int, GameObject> ();
		public GameObject loadGameObject(string tPath) {
			return _loadGameObject (tPath);
		}

		public GameObject loadGameObject(int tPathHash)
		{
			return _loadGameObject(tPathHash);
		}

		private GameObject _loadGameObject(string tPath) {
			int tHashCode = tPath.GetHashCode ();
			if (!_gameObjectsCache.ContainsKey (tHashCode)) {
				GameObject tGameObject = Resources.Load<GameObject> (tPath);
				if (tGameObject == null) {
					throw new NullReferenceException("Invalid path! : " + tPath);
				}
				_gameObjectsCache.Add (tHashCode, tGameObject);
			}

			return _gameObjectsCache [tHashCode];
		}

		private GameObject _loadGameObject(int tPathHash) {
			if (!_gameObjectsCache.ContainsKey (tPathHash)) {
				throw new NullReferenceException("Invalid reference object!");
			}

			return _gameObjectsCache [tPathHash];
		}
	}
	#endregion GameObject

	#region AudioClip
    public interface AudioClipCacheController {
        AudioClip loadAudioClip(string tPath) ;
		void play(string tPath, bool tLoop = false, float tDelay = 0f, AudioSource tAudioSource = null);
    }

    public partial class ResourcesCache : AudioClipCacheController {
        #region Getter
        public static AudioClipCacheController AudioClip {
            get {
//				#if DEBUG
//
//				if(_instance == null)
//				{
//					GameObject newInstance = new GameObject();
//
//					_instance = newInstance.AddComponent<ResourcesCache>();
//					_instance._audioSource = newInstance.AddComponent<AudioSource>();
//				}
//
//				#endif

                return _instance;
            }
        }
        #endregion

		private Dictionary<int, AudioClip> _audioClipsCache = new Dictionary<int, AudioClip> ();
		public AudioClip loadAudioClip(string tPath) {
			return _loadAudioClip (tPath);
		}

		private AudioClip _loadAudioClip(string tPath) {
			int tHashCode = tPath.GetHashCode ();
			if (!_audioClipsCache.ContainsKey (tHashCode)) {
				_audioClipsCache.Add (tHashCode, Resources.Load<AudioClip> (tPath));
			}

			return _audioClipsCache [tHashCode];
		}

		public void play(string tPath, bool tLoop = false, float tDelay = 0f, AudioSource tAudioSource = null) {
			if (tDelay <= 0f) {
				_instance._play (tPath, tLoop, tDelay);
			} else {
				ActionData tDelayData = _instance._actionManager.addAction ();
				tDelayData.addDelayTime (tDelay, () => {
					_instance._play (tPath, tLoop, tDelay, tAudioSource);
				});
				tDelayData.onAction ();
			}
		}

		private void _play(string tPath, bool tLoop = false, float tDelay = 0f, AudioSource tAudioSource = null) {
			if(tAudioSource != null) {
				tAudioSource.clip = _instance._loadAudioClip(tPath);
				tAudioSource.loop = tLoop;
				tAudioSource.Play();
			} else {
				_instance._audioSource.clip = _instance._loadAudioClip(tPath);
				_instance._audioSource.loop = tLoop;
				_instance._audioSource.Play();
			}
		}
	}
    #endregion AudioClip
}