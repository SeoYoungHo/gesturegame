﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using BicJSON;
using System;

public class DataController {

	private static DataController _instance = null;
	public static DataController getInstance() {
		if (_instance == null) {
			_instance = new DataController ();
			_instance.init ();
		}

		return _instance;
	}

	private Dictionary<string, JSONValue> _cachedData;

	private void init() {
		_cachedData = new Dictionary<string, JSONValue> ();
	}

	public static JSONValue loadFileToJson(string tFilename, bool tIsUserFile) {
		DataController tController = getInstance ();
		if (tController._cachedData.ContainsKey (tFilename)) {
			return tController._cachedData [tFilename];
		} else {
			if (tIsUserFile) {
				string tPath = Application.persistentDataPath + "/" + tFilename;

				if (File.Exists(tPath))
				{
					FileStream tFile = new FileStream (tPath, FileMode.Open, FileAccess.Read);
					StreamReader tReader = new StreamReader (tFile);
					string tData = tReader.ReadToEnd ();
					tReader.Close ();
					JSONValue tJson = JSONValue.Parse (tData);
					tController._cachedData.Add (tFilename, tJson);
					tFile.Close();
					return tJson;
				}
				else
				{
					return new JSONValue(JSONValueType.Array);
				}
			} else {
//				StreamReader tReader = File.OpenText ("Assets/Resources/" + tFilename);
//				string tData = tReader.ReadToEnd ();
//				tReader.Close ();
				string tData = Resources.Load<TextAsset>(tFilename).text;
				JSONValue tJson = JSONValue.Parse (tData);
				tController._cachedData.Add (tFilename, tJson);
				return tJson;
			}
		}
	}

	public static void saveJsonToFile(JSONValue tJson, string tFilename) {
		DataController tController = getInstance ();
		if (tController._cachedData.ContainsKey (tFilename)) {
			tController._cachedData [tFilename] = tJson;
		} else {
			tController._cachedData.Add (tFilename, tJson);
		}

		string path = Application.persistentDataPath + "/" + tFilename;
		FileStream file = new FileStream (path, FileMode.Create, FileAccess.Write);
		StreamWriter tWriter = new StreamWriter (file);
		tWriter.Write (tJson.ToJsonString ());
		tWriter.Flush ();
		tWriter.Close ();
		file.Close();
	}
}
