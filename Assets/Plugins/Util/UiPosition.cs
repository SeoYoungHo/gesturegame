﻿using UnityEngine;
using System.Collections;

public enum LeftRight
{
    Left = -1,
    Center = 0,
    Right = 1
}

public enum UpDown
{
    Down = -1,
    Center = 0,
    Up = 1
}

public class UiPosition : MonoBehaviour
{
    public UpDown uUpDown = UpDown.Center;
    public float uUpDownRate = 1f;
    public LeftRight uLeftRight = LeftRight.Center;
    public float uLeftRightRate = 1f;

    public virtual void Awake ()
    {
        Vector3 tEditorPosition = transform.localPosition;

        #if DEBUG

        if(ScreenSizeSetting.ScreenSize == Vector2.zero)
        {
            ScreenSizeSetting.ScreenSize = new Vector2(640, 960);
        }

        #endif

        if (uUpDown != UpDown.Center) {
            float tLongValue = (ScreenSizeSetting.ScreenSize.y - 960f) / 2f;
            if (uUpDown == UpDown.Up) {
                tEditorPosition.y += tLongValue * uUpDownRate;
            } else {//if (uUpDown == UpDown.Down) {
                tEditorPosition.y -= tLongValue * uUpDownRate;
            }
        }

        if (uLeftRight != LeftRight.Center) {
            float tLongValue = (ScreenSizeSetting.ScreenSize.x - 640f) / 2f;
            if (uLeftRight == LeftRight.Left) {
                tEditorPosition.x -= tLongValue * uLeftRightRate;
            } else {//if (uLeftRight == LeftRight.Right) {
                tEditorPosition.x += tLongValue * uLeftRightRate;
            }
        }

        transform.localPosition = tEditorPosition;
    }
}
