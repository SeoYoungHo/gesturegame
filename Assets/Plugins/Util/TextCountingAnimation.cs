﻿using UnityEngine;
using System.Collections;

public class TextCountingAnimation : MonoBehaviour {

	public string text_format = "{0:0}";
	public double counting_time = 0.5;
	public double base_value = 0.0;
	float time_value;
	double before_value;
	bool is_changing = false;

	UnityEngine.UI.Text ugui_text = null;

	public void initValue(double t_value)
	{
		is_changing = false;
		base_value = t_value;
		before_value = base_value;

		if (ugui_text == null)
			ugui_text = GetComponent<UnityEngine.UI.Text> ();
		ugui_text.text = string.Format (text_format, before_value);
		
	}

	public void changeValue(int t_value)
	{
		changeValue ((double)t_value);
	}

	public void changeValue(float t_value)
	{
		changeValue ((double)t_value);
	}

	public void changeValue(double t_value)
	{
		time_value = 0f;

		if (ugui_text == null)
			ugui_text = GetComponent<UnityEngine.UI.Text> ();
		ugui_text.text = string.Format (text_format, before_value);
		

		base_value = t_value;
		is_changing = true;
	}

	void Awake() {
		if (ugui_text == null)
			ugui_text = GetComponent<UnityEngine.UI.Text> ();
		
		before_value = base_value;
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (is_changing) {
			float b_t = time_value;
			time_value += Time.deltaTime;
			if(time_value >= counting_time) {
				before_value = base_value;
				is_changing = false;
			} else {
				before_value += (base_value - before_value) * (time_value - b_t) / (counting_time - b_t);
			}

			ugui_text.text = string.Format(text_format, before_value);
			
		}
	}
}
