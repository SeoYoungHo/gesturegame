// #define USE_SharpZipLib
#if !UNITY_WEBPLAYER
#define USE_FileIO
#endif

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

namespace BicJSON 
{
	public enum JSONValueType
	{
		Array 	= 1,
		Dict  	= 2,
		Int   	= 3,
		Double	= 4,
		Bool	= 5,
		Float	= 7,
		String	= 8,
		Long 	= 9,
		None
	}

	public class JSONValue
	{
		private Dictionary<string, JSONValue> objectValue = new Dictionary<string, JSONValue>();
		private long intValue = 0;
		private string stringValue = string.Empty;
		private double doubleValue = 0;
		private JSONValueType type = JSONValueType.None;

		public JSONValue()
		{
			//construct
		}

		public JSONValue(JSONValueType _type)
		{
			ValueType = _type;
		}

		public JSONValue(string v, bool isAuto = false)
		{
			if (isAuto) {
				this.AsAuto = v;
			} else {
				this.AsString = v;
			}
		}
		
		public JSONValue(int v)
		{
			this.AsInt = v;
		}

		public JSONValue(long v)
		{
			this.AsLong = v;
		}

		public JSONValue(float v)
		{
			this.AsFloat = v;
		}
		
		public JSONValue(double v)
		{
			this.AsDouble = v;
		}
		
		public JSONValue(bool v)
		{
			this.AsBool = v;
		}





		public JSONValueType ValueType
		{
			get {
				return type;
			}

			set{
				this.type = value;
			}
		}

		#region get/set value
		public int Count
		{
			get { 

				stringToObject();

				return objectValue.Count; 
			}

		}

		public IEnumerable<JSONValue> Childs
		{
			get
			{
				stringToObject();
				foreach (KeyValuePair<string, JSONValue> n in objectValue) {
					yield return n.Value;
				}
			}
		}

		public IEnumerable<string> Keys
		{

			get 
			{
				stringToObject();
				foreach (var key in objectValue.Keys) {
					yield return key;
				}
			}
		}

		public string AsAuto
		{
			set{
				this.ValueType = JSONValueType.None;
				long vlong = 0;
				if (long.TryParse (value, out vlong)) {
					this.AsLong = vlong;
				}

				double vdouble = 0;
				if (double.TryParse (value, out vdouble)) {
					this.AsDouble = vdouble;
				}

				if (value.ToLower () == "true") {
					this.AsBool = true;
				} else if (value.ToLower () == "false") {
					this.AsBool = false;
				}

				if (this.ValueType == JSONValueType.None) {
					this.AsString = value;
				}
			}
		}

		public int AsInt
		{
			get{
				if (ValueType == JSONValueType.Double || ValueType == JSONValueType.Float)
				{
					return (int)AsDouble;
				}
				else if (ValueType == JSONValueType.String)
				{
					int dv = 0;
					if (int.TryParse (this.AsString, out dv)) {
						return dv;
					}
				}

				return (int)this.intValue;
			}

			set{
				this.intValue = (long)value;
				ValueType = JSONValueType.Int;
			}
		}

		public long AsLong
		{
			get{
				if (ValueType == JSONValueType.Double || ValueType == JSONValueType.Float) {
					return (long)AsDouble;
				} else if (ValueType == JSONValueType.String) {
					long dv = 0;
					if (long.TryParse (this.AsString, out dv)) {
						return dv;
					}
				}

				return this.intValue;
			}

			set{
				this.intValue = value;
				ValueType = JSONValueType.Long;
			}
		}

		public bool AsBool
		{
			get{
				if (ValueType == JSONValueType.String)
				{
					if (this.AsString == "true") {
						return true;
					}

					if (this.AsString == "false") {
						return false;
					}
				}

				return this.intValue != 0 ? true : false;
			}

			set{
				if (value == true)
				{
					this.intValue = 1;
				} else {
					this.intValue = 0;
				}

				ValueType = JSONValueType.Bool;
			}
		}

		public string AsString
		{
			get{
				if (ValueType == JSONValueType.Double || ValueType == JSONValueType.Float)
				{
					return AsDouble.ToString();
				} else if (ValueType == JSONValueType.Int || ValueType == JSONValueType.Long)
				{
					return AsLong.ToString();
				} else if (ValueType == JSONValueType.Bool) {
					return this.AsBool ? "true" : "false";
				}

				return this.stringValue;
			}

			set{
				this.stringValue = value;
				ValueType = JSONValueType.String;
			}
		}
		
		public float AsFloat
		{
			get{
				if (ValueType == JSONValueType.Long || ValueType == JSONValueType.Int)
				{
					return (float)AsLong;
				} else if (ValueType == JSONValueType.String){
					float dv = 0;
					if (float.TryParse (this.AsString, out dv)) {
						return dv;
					}
				}

				return (float)doubleValue;
			}

			set{
				doubleValue = (double)value;
				ValueType = JSONValueType.Float;
			}
		}
		
		public double AsDouble
		{
			get{
				if(ValueType == JSONValueType.Long || ValueType == JSONValueType.Int)
				{
					return (double)AsLong;
				}else if(ValueType == JSONValueType.String){
					double dv = 0;
					if (double.TryParse (this.AsString, out dv)) {
						return dv;
					}
				}

				return doubleValue;
			}

			set{
				doubleValue = value;
				ValueType = JSONValueType.Double;
			}
		}

		public JSONValue AsObject
		{
			get{
				return this;
			}

			set{
				this.objectValue = value.AsDictionary;
				this.AsLong = value.AsLong;
				this.AsDouble = value.AsDouble;
				this.AsString = value.AsString;
				this.ValueType = value.ValueType;
			}
		}

		public Dictionary<string, JSONValue> AsDictionary
		{
			get{
				return objectValue;
			}
		}

		#endregion get/set value
		
		#region operator/index
		public JSONValue this[string keyname]
		{
			get
			{
				//Debug.Log("jsonvalue this [] get"+aKey);
				stringToObject();
				
				this.ValueType = JSONValueType.Dict;
				if (!objectValue.ContainsKey(keyname)){
					//Debug.Log("add new");
					objectValue.Add(keyname, new JSONValue(JSONValueType.None));
				}

				return objectValue[keyname];
			}

			set
			{
				//Debug.Log("------------------------> jsonvalue[]="+aKey);
				
				//Debug.Log("jsonvalue this [] set "+aKey+" value->"+value.ValueType.ToString()+","+value.Count.ToString());
				stringToObject();

				if (objectValue.ContainsKey (keyname)) {
					objectValue [keyname] = value;
				} else {
					objectValue.Add (keyname, value);
				}

				this.ValueType = JSONValueType.Dict;
			}
		}

		public JSONValue this[int index]
		{
			get
			{
				//Debug.Log("jsonvalue this [] get"+aIndex);
				stringToObject();
				
				this.ValueType = JSONValueType.Array;
				if (index < 0 || index > objectValue.Count) {
					return null;
				}

				if(index == objectValue.Count)
				{
					objectValue.Add(Guid.NewGuid().ToString(), new JSONValue(JSONValueType.None));
				}

				return objectValue.ElementAt(index).Value;
			}

			set
			{
				stringToObject();

				if (index < 0 || index > objectValue.Count) {
					throw new Exception ("JSONValue : out of index");
				}

				if(index == objectValue.Count)
				{
					objectValue.Add(Guid.NewGuid().ToString(), value);
				}else{
					string key = objectValue.ElementAt(index).Key;
					objectValue[key] = value;
				}

				this.ValueType = JSONValueType.Array;
			}
		}


		#endregion operator/index
	
		public static JSONValue Param(params string[] args)
		{
			var p = new JSONValue (JSONValueType.Dict);
			string v = string.Empty;

			foreach (var a in args) {
				if(v == string.Empty)
				{
					v = a;
				}else{
					p[v].AsString = a;
					v = string.Empty;
				}
			}

			return p;
		}

		public static JSONValue ParamArray(params string[] args)
		{
			var p = new JSONValue (JSONValueType.Dict);

			foreach (var a in args) {
				p.Add(new JSONValue(a));
			}

			return p;
		}

		public static bool TryParse(string jsonstr, out JSONValue result)
		{
			try{
				result = Parse(jsonstr);
				if(result == null){
					return false;
				}
			}
			catch
			{
				Debug.Log("parse error");
				result = null;
				return false;
			}

			return true;
		}

		public static JSONValue MergeDictionary(JSONValue v1, JSONValue v2)
		{
			JSONValue r = new JSONValue (JSONValueType.Dict);

			if (v1 != null) {
				foreach (var k in v1.AsDictionary.Keys) {
					r [k].AsObject = v1 [k].AsObject;
				}
			}

			if (v2 != null) {
				foreach (var k in v2.AsDictionary.Keys) {
					r [k].AsObject = v2 [k].AsObject;
				}
			}

			return r;

		}

		public static JSONValue Parse(string jsonText)
		{
			Stack<JSONValue> stack = new Stack<JSONValue>();
			JSONValue ctx = null;
			int i = 0;
			string token = string.Empty;
			string tokenName = string.Empty;
			bool isNoneStr = false;
			bool quoteMode = false;

			while (i < jsonText.Length)
			{
				if(i >= 2)
				{
					if(jsonText[i - 1] == '\"' && jsonText[i - 2] == '\"' && token == string.Empty)
					{
						isNoneStr = true;
					}else{
						isNoneStr = false;
					}
				}else{
					isNoneStr = false;
				}

				switch (jsonText[i])
				{
				case '{':
					if (quoteMode)
					{
						token += jsonText[i];
						break;
					}

					stack.Push(new JSONValue(JSONValueType.Dict));
					if (ctx != null)
					{
						tokenName = tokenName.Trim();
						if (ctx.ValueType == JSONValueType.Array) {
							ctx.Add (stack.Peek ());
						} else if (tokenName != string.Empty) {
							ctx.Add (tokenName, stack.Peek ());
						}
					}

					tokenName = string.Empty;
					token = string.Empty;
					ctx = stack.Peek();
					break;

				case '[':
					if (quoteMode) {
						token += jsonText [i];
						break;
					}

					stack.Push (new JSONValue (JSONValueType.Array));
					if (ctx != null) {
						tokenName = tokenName.Trim ();
						if (ctx.ValueType == JSONValueType.Array) {
							ctx.Add (stack.Peek ());
						} else if (tokenName != string.Empty) {
							ctx.Add (tokenName, stack.Peek ());
						}
					}

					tokenName = string.Empty;
					token = string.Empty;
					ctx = stack.Peek();
					break;

				case '}':
				case ']':
					if (quoteMode) {
						token += jsonText [i];
						break;
					}

					if (stack.Count == 0) {
						throw new Exception ("JSON Parse: Too many closing brackets");
					}

					stack.Pop ();
					if (token != string.Empty || isNoneStr == true) {
						tokenName = tokenName.Trim ();
						if (ctx.ValueType == JSONValueType.Array) {
							ctx.Add (new JSONValue (token, true));
						} else if (tokenName != string.Empty) {
							ctx.Add (tokenName, new JSONValue (token, true));
						}
					}

					tokenName = string.Empty;
					token = string.Empty;
					if (stack.Count > 0) {
						ctx = stack.Peek ();
					}

					break;

				case ':':
					if (quoteMode)
					{
						token += jsonText[i];
						break;
					}

					tokenName = token;
					token = string.Empty;
					break;

				case '"':
					quoteMode ^= true;
					break;

				case ',':
					if (quoteMode)
					{
						token += jsonText[i];
						break;
					}

					if (token != string.Empty || isNoneStr == true)
					{
						if (ctx.ValueType == JSONValueType.Array){
							ctx.Add(new JSONValue(token, true));
						}else if (tokenName != string.Empty){
							ctx.Add(tokenName, new JSONValue(token, true));
						}
					}

					tokenName = string.Empty;
					token = string.Empty;
					break;

				case '\r':
				case '\n':
					break;

				case ' ':
				case '\t':
					if (quoteMode) {
						token += jsonText [i];
					}

					break;

				case '\\':
					++i;
					if (quoteMode)
					{
						char c = jsonText[i];
						switch (c)
						{
						case 't': token += '\t'; break;
						case 'r': token += '\r'; break;
						case 'n': token += '\n'; break;
						case 'b': token += '\b'; break;
						case 'f': token += '\f'; break;
						case 'u':
							{
								string s = jsonText.Substring(i + 1, 4);
								token += (char)int.Parse(s, System.Globalization.NumberStyles.AllowHexSpecifier);
								i += 4;
								break;
							}

						default: token += c; break;
						}
					}

					break;
				default:
					token += jsonText[i];
					break;
				}
				++i;
			}

			if (quoteMode)
			{
				throw new Exception("JSON Parse: Quotation marks seems to be messed up.");
			}

			return ctx;
		}

		#region objectvalue

		public void stringToObject()
		{
			if(ValueType == JSONValueType.String)
			{
				//Debug.Log("---------------- auto parse by indexer "+AsString);
				//parse
				JSONValue v;
				if(JSONValue.TryParse(AsString, out v))
				{
					this.AsString = string.Empty;
					this.AsObject = v.AsObject;
				}
			}
		}

		public bool hasValue(JSONValue value)
		{
			return hasValue (value.AsString);
		}

		public bool hasKey(JSONValue key)
		{
			return hasKey (key.AsString);
		}

		public bool hasKey(string key)
		{
			foreach (var v in objectValue) {
				if(v.Key == key)
				{
					return true;
				}
			}

			return false;
		
		}

		public bool hasValue(string value)
		{
			foreach (var v in objectValue) {
				if(v.Value.AsString == value)
				{
					return true;
				}
			}

			return false;
		}
		
		public void Add(string key, JSONValue item)
		{
			
			stringToObject();
			
			if (!string.IsNullOrEmpty (key)) {
				if (objectValue.ContainsKey (key)) {
					objectValue [key] = item;
				} else {
					objectValue.Add (key, item);
				}
			} else {
				objectValue.Add (Guid.NewGuid ().ToString (), item);
			}
			
			ValueType = JSONValueType.Dict;
		}
		
		public void Add(JSONValue item)
		{
			
			stringToObject();

			objectValue.Add(Guid.NewGuid().ToString(), item);
			
			ValueType = JSONValueType.Array;
		}

		public JSONValue RemoveByKey(string key)
		{
			
			stringToObject();

			if (!objectValue.ContainsKey (key)) {
				return null;
			}

			JSONValue tmp = objectValue[key];
			objectValue.Remove(key);
			return tmp;        
		}

		public JSONValue RemoveByIndex(int index)
		{
			
			stringToObject();

			if (index < 0 || index >= objectValue.Count) {
				return null;
			}

			var item = objectValue.ElementAt(index);
			objectValue.Remove(item.Key);
			return item.Value;
		}

		public JSONValue Remove(JSONValue node)
		{
			
			stringToObject();

			try
			{
				var item = objectValue.Where(k => k.Value == node).First();
				objectValue.Remove(item.Key);
				return node;
			}
			catch
			{
				return null;
			}
		}

		public void Clear()
		{
			
			stringToObject();

			objectValue.Clear ();
		}
		#endregion objectvalue

		public string ToJsonString()
		{

			if (ValueType == JSONValueType.Dict) {
				string result = "{";
				foreach (KeyValuePair<string, JSONValue> obj in objectValue) {
					if (result.Length > 2) {
						result += ", ";
					}

					result += "\"" + Escape (obj.Key) + "\":" + obj.Value.ToJsonString ();
				}

				result += "}";

				return result;
			} else if (ValueType == JSONValueType.Array) {
				string result = "[ ";
				foreach (KeyValuePair<string, JSONValue> obj in objectValue) {
					if (result.Length > 2) {
						result += ", ";
					}

					result += obj.Value.ToJsonString ();
				}

				result += " ]";

				return result;
			} else if (ValueType == JSONValueType.String) {
				//Debug.Log("string bbb"+"\"" + Escape (this.AsString) + "\"");
				return "\"" + Escape (this.AsString) + "\"";
			} else if (ValueType == JSONValueType.Int || ValueType == JSONValueType.Long) {
				//Debug.Log("int bbb"+this.AsLong.ToString ());
				return this.AsLong.ToString ();
			} else if (ValueType == JSONValueType.Double || ValueType == JSONValueType.Float) {
				//Debug.Log("double bbb"+this.AsDouble.ToString ());
				return this.AsDouble.ToString ();
			} else if(ValueType == JSONValueType.Bool) {
				//Debug.Log("bool bbb"+this.AsBool.ToString());
				if (this.AsBool) {
					return "true";
				} else { 
					return "false";
				}
			} else {
				//Debug.Log ("else bbb null");
				return "null";
			}
		}
		

		internal static string Escape(string text)
		{
			if(string.IsNullOrEmpty(text))
				return text;
			
			string result = string.Empty;
			foreach(char c in text)
			{
				switch(c)
				{
				case '\\': result += "\\\\"; break;
				case '\"': result += "\\\""; break;
				case '\n': result += "\\n"; break;
				case '\r': result += "\\r"; break;
				case '\t': result += "\\t"; break;
				case '\b': result += "\\b"; break;
				case '\f': result += "\\f"; break;
				default: result += c; break;
				}
			}

			return result;
		}
		
	}


	
}