﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BicJSON;

public class DataSingleton {
	private static DataSingleton _instance = null;
	public static DataSingleton instance() {
		if (_instance == null) {
			_instance = new DataSingleton ();
		}

		return _instance;
	}

	Dictionary<int, JSONValue> _datas = new Dictionary<int, JSONValue>();

	public static void keepData(string tKey, JSONValue tData) {
		instance ()._keepData (tKey, tData);
	}

	private void _keepData(string tKey, JSONValue tData) {
		int tHashCode = tKey.GetHashCode ();
		if (_datas.ContainsKey (tHashCode)) {
			_datas [tHashCode] = JSONValue.MergeDictionary (_datas [tHashCode], tData);
		} else {
			_datas.Add (tHashCode, tData);
		}
	}

	public static JSONValue getData(string tKey) {
		return instance ()._getData (tKey);
	}

	private JSONValue _getData(string tKey) {
		int tHashCode = tKey.GetHashCode ();
		if (_datas.ContainsKey (tHashCode)) {
			return _datas [tHashCode];
		} else {
			Debug.LogFormat ("get not have data : {0}", tKey);
			return new JSONValue ();
		}
	}

	public static void removeData(string tKey) {
		instance ()._removeData (tKey);
	}

	private void _removeData(string tKey) {
		int tHashCode = tKey.GetHashCode ();
		if (_datas.ContainsKey (tHashCode)) {
			Debug.LogFormat ("remove not have data : {0}", tKey);
		} else {
			_datas.Remove (tHashCode);
		}
	}
}

public enum DataSingletonKey {
	SelectedCharacter = 0
}