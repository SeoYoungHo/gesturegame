﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BigFloat {
	public float mantissa;
	public int exponent;

	public BigFloat() {
		mantissa = 0f;
		exponent = 1;
	}

	public BigFloat(BigFloat tOriginal) {
		mantissa = tOriginal.mantissa;
		exponent = tOriginal.exponent;
	}

	public BigFloat(float tMantissa, int tExponent) {
		mantissa = tMantissa;
		exponent = tExponent;
	}

	public override bool Equals(System.Object obj) {
		if (obj == null) {
			return false;
		}

		BigFloat tFloat = obj as BigFloat;
		if ((System.Object)tFloat == null) {
			return false;
		}

		return mantissa == tFloat.mantissa && exponent == tFloat.exponent;
	}

	public bool Equals(BigFloat tFloat) {
		if ((object)tFloat == null) {
			return false;
		}

		return mantissa == tFloat.mantissa && exponent == tFloat.exponent;
	}

	public override int GetHashCode() {
		return mantissa.GetHashCode () ^ exponent.GetHashCode ();
	}

	public static bool operator == (BigFloat t1, BigFloat t2) {
		return t1.mantissa == t2.mantissa && t1.exponent == t2.exponent;
	}

	public static bool operator != (BigFloat t1, BigFloat t2) {
		return !(t1.mantissa == t2.mantissa);
	}

	public static BigFloat operator + (BigFloat t1, BigFloat t2) {
		if (t1.mantissa >= 0 && t2.mantissa >= 0) {
			BigFloat tBig;
			BigFloat tSmall;
			if (t1.exponent >= t2.exponent) {
				tBig = new BigFloat (t1);
				tSmall = new BigFloat (t2);
			} else {
				tBig = new BigFloat (t2);
				tSmall = new BigFloat (t1);
			}

			int tSubExp = tBig.exponent - tSmall.exponent;
			if (tSubExp >= 6) {
				return new BigFloat (tBig);
			} else {
				float tMantissa = tBig.mantissa * Mathf.Pow(10f, tSubExp);
				tMantissa += tSmall.mantissa;
				int tAddExp = Mathf.FloorToInt(Mathf.Log10 (tMantissa));
				tMantissa /= Mathf.Pow (10f, tAddExp);
				int tExponent = tBig.exponent - tSubExp + tAddExp;
				return new BigFloat (tMantissa, tExponent);
			}
		} else if (t1.mantissa >= 0 && t2.mantissa < 0) {
			return t1 - t2;
		} else if (t1.mantissa < 0 && t2.mantissa >= 0) {
			return t2 - t1;
		} else {
			BigFloat tCopy1 = new BigFloat (t1);
			BigFloat tCopy2 = new BigFloat (t2);
			tCopy1.mantissa = -tCopy1.mantissa;
			tCopy2.mantissa = -tCopy2.mantissa;

			BigFloat tAdd = tCopy1 + tCopy2;
			tAdd.mantissa = -tAdd.mantissa;
			return tAdd;
		}
	}

	public static BigFloat operator - (BigFloat t1, BigFloat t2) {
		if (t1.mantissa >= 0 && t2.mantissa >= 0) {
			BigFloat tBig;
			BigFloat tSmall;
			if (t1.exponent > t2.exponent) {
				tBig = new BigFloat (t1);
				tSmall = new BigFloat (t2);
			} else if (t1.exponent == t2.exponent) {
				if (t1.mantissa >= t2.mantissa) {
					tBig = new BigFloat (t1);
					tSmall = new BigFloat (t2);
				} else {
					tBig = new BigFloat (t2);
					tSmall = new BigFloat (t1);
				}
			} else {
				tBig = new BigFloat (t2);
				tSmall = new BigFloat (t1);
			}

			int tSubExp = tBig.exponent - tSmall.exponent;
			if (tSubExp >= 6) {
				return new BigFloat (tBig);
			} else {
				float tMantissa = tBig.mantissa * Mathf.Pow(10f, tSubExp);
				tMantissa -= tSmall.mantissa;
				if (tMantissa < 1f) {
					tMantissa *= 10f;
					++tSubExp;
				}
				int tAddExp = Mathf.FloorToInt(Mathf.Log10 (tMantissa));
				tMantissa /= Mathf.Pow (10f, tAddExp);
				int tExponent = tBig.exponent - tSubExp + tAddExp;
				return new BigFloat (tMantissa, tExponent);
			}
		} else if (t1.mantissa >= 0 && t2.mantissa < 0) {
			BigFloat tCopy1 = new BigFloat (t1);
			BigFloat tCopy2 = new BigFloat (t2);
			tCopy2.mantissa = -tCopy2.mantissa;

			BigFloat tAdd = tCopy1 + tCopy2;
			return tAdd;
		} else if (t1.mantissa < 0 && t2.mantissa >= 0) {
			BigFloat tCopy1 = new BigFloat (t1);
			BigFloat tCopy2 = new BigFloat (t2);
			tCopy1.mantissa = -tCopy1.mantissa;

			BigFloat tAdd = tCopy1 + tCopy2;
			tAdd.mantissa = -tAdd.mantissa;
			return tAdd;
		} else {
			BigFloat tCopy1 = new BigFloat (t1);
			BigFloat tCopy2 = new BigFloat (t2);
			tCopy1.mantissa = -tCopy1.mantissa;
			tCopy2.mantissa = -tCopy2.mantissa;

			BigFloat tSub = tCopy1 - tCopy2;
			tSub.mantissa = -tSub.mantissa;
			return tSub;
		}
	}

	public static BigFloat operator * (BigFloat t1, BigFloat t2) {
		BigFloat tMulti = new BigFloat ();
		tMulti.mantissa = t1.mantissa * t2.mantissa;
		int tAddExp = Mathf.FloorToInt(Mathf.Log10 (tMulti.mantissa));
		tMulti.mantissa /= Mathf.Pow (10f, tAddExp);
		tMulti.exponent = t1.exponent + t2.exponent + tAddExp;
		return tMulti;
	}

	public static BigFloat operator / (BigFloat t1, BigFloat t2) {
		Debug.Assert (t2.mantissa != 0f, "devide zero");

		bool tIsMinus = false;
		if ((t1.mantissa >= 0 && t2.mantissa < 0) || (t1.mantissa < 0 && t2.mantissa >= 0)) {
			tIsMinus = true;
		}

		BigFloat tDevide = new BigFloat ();
		tDevide.mantissa = Mathf.Abs(t1.mantissa) / Mathf.Abs(t2.mantissa);
		tDevide.exponent = t1.exponent - t2.exponent;
		if (tDevide.mantissa < 1f) {
			tDevide.mantissa *= 10f;
			--tDevide.exponent;
		}

		if (tIsMinus) {
			tDevide.mantissa = -tDevide.mantissa;
		}
		return tDevide;
	}
}
