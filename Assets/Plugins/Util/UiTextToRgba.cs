﻿using UnityEngine;
using System.Collections;

public class UiTextToRgba : MonoBehaviour {

	public int from_r;
	public int from_g;
	public int from_b;
	public int from_a;

	public int to_r;
	public int to_g;
	public int to_b;
	public int to_a;

	public float to_time;

	float time_value;
	float time_rate;
	bool is_end;

	UnityEngine.UI.Text m_text;

	// Use this for initialization
	void Start () {
		is_end = false;
		time_value = 0.0f;
		time_rate = 0.0f;
		m_text = GetComponent<UnityEngine.UI.Text> ();
		m_text.color = new Vector4 (from_r/255.0f, from_g/255.0f, from_b/255.0f, from_a/255.0f);
	}
	
	// Update is called once per frame
	void Update () {
		if (!is_end) {
			time_value += Time.deltaTime;
			if (time_value >= to_time) {
				m_text.color = new Vector4 (to_r/255.0f, to_g/255.0f, to_b/255.0f, to_a/255.0f);
				time_value = to_time;
				time_rate = 1.0f;
				is_end = true;
			} else {
				time_rate = time_value/to_time;
				m_text.color = new Vector4 ((from_r + time_rate*(to_r-from_r))/255.0f, (from_g + time_rate*(to_g-from_g))/255.0f, (from_b + time_rate*(to_b-from_b))/255.0f, (from_a + time_rate*(to_a-from_a))/255.0f);
			}
		}
	}
}
