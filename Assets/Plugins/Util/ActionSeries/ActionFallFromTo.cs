﻿using UnityEngine;
using System.Collections;
using System;

public class ActionFallFromTo : ActionBase {
	
	static float gravity_value = 2200f;
	static float v0 = 30f;
	bool is_moving = false;
	
	Vector3 from_vec;
	Vector3 to_vec;

	Transform target_obj;

	public override void Update (float tDeltaTime)
	{
		if (is_moving) {
			time_value += tDeltaTime;
			if (time_value >= end_time) {
				target_obj.localPosition = to_vec;

				if(end_action != null)
					end_action(time_value-end_time);

				is_moving = false;

				//				DestroyObject(gameObject);
			}
			else
			{
				//				transform.localPosition = new Vector3(from_vec.x, from_vec.y - gravity_value*time_value*time_value/2f, from_vec.z);
				target_obj.localPosition = new Vector3(from_vec.x, from_vec.y - (v0*time_value + gravity_value*time_value*time_value/2f), from_vec.z);
			}
		}
	}

	public static float checkEndTime(Vector3 t_from, Vector3 t_to)
	{
		return (-v0 + Mathf.Sqrt (v0 * v0 + 2f * gravity_value * (t_from.y - t_to.y))) / gravity_value;//Mathf.Sqrt (2f*(t_from.y - t_to.y)/gravity_value);
	}

	public void setInfo(Transform t_target, Vector3 t_from, Vector3 t_to, Action<float> t_end_action)
	{
		target_obj = t_target;
		from_vec = t_from;
		to_vec = t_to;
		end_time = (-v0 + Mathf.Sqrt (v0 * v0 + 2f * gravity_value * (from_vec.y - to_vec.y))) / gravity_value;//Mathf.Sqrt (2f*(t_from.y - t_to.y)/gravity_value);
		end_action = t_end_action;
	}
	
	public void startFall (float t_start_time)
	{
		if (end_time <= 0f) {
			target_obj.localPosition = to_vec;
			if (end_action != null)
				end_action (t_start_time);
			is_moving = false;
			
//			DestroyObject (gameObject);
		} else if (t_start_time >= end_time) {
			target_obj.localPosition = to_vec;
			if (end_action != null)
				end_action (t_start_time - end_time);
			is_moving = false;
			
//			DestroyObject (gameObject);
		} else {
			time_value = t_start_time;
			target_obj.localPosition = new Vector3(from_vec.x, from_vec.y - (v0*time_value + gravity_value*time_value*time_value/2f), from_vec.z);
			
			is_moving = true;
		}
	}

//	public float fall(Vector3 t_from, Vector3 t_to, float t_g = 2500f, float t_v0 = 100f)
//	{
//		gravity_value = t_g;
//		v0 = t_v0;
//		from_vec = t_from;
//		to_vec = t_to;
//		end_time = (-v0 + Mathf.Sqrt (v0 * v0 + 2f * gravity_value * (from_vec.y - to_vec.y))) / gravity_value;//Mathf.Sqrt (2f*(t_from.y - t_to.y)/gravity_value);
//		
//		transform.localPosition = from_vec;
//		
//		time_value = 0f;
//		
//		from_vec = t_from;
//		to_vec = t_to;
//		
//		is_moving = true;
//		
//		return end_time;
//	}
}
