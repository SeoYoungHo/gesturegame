﻿using UnityEngine;
using System.Collections;
using System;

public class ActionMoveFromTo : ActionBase {
	
	bool is_moving = false;
	
	Vector3 from_vec;
	Vector3 to_vec;
	Vector3 mid_vec;

	bool isAbsoluteValue = true;

	Transform target_obj;

	public override void Update (float tDeltaTime)
	{
		if (is_moving) {
			float before_time_value = time_func (time_value);
			time_value += tDeltaTime;
			float t_time_value = time_func (time_value);

			if (time_value >= end_time) {
				if (isAbsoluteValue) {
					target_obj.localPosition = to_vec;
				} else {
//					float time_rate = (t_time_value - before_time_value) / end_time;
//					Vector3 move_vec = (to_vec - from_vec) * time_rate;
//					mid_vec -= move_vec;
//					target_obj.Translate(move_vec);
					target_obj.Translate (mid_vec);
				}
				if(end_action != null)
					end_action(time_value-end_time);
				is_moving = false;

//				DestroyObject(gameObject);
			}
			else
			{
				if (isAbsoluteValue) {
					float time_rate = t_time_value/end_time;
					Vector3 sub_vec = to_vec - from_vec;
					sub_vec = sub_vec * time_rate;
//					sub_vec.x *= time_rate;
//					sub_vec.y *= time_rate;
//					sub_vec.z *= time_rate;

					target_obj.localPosition = from_vec + sub_vec;
				} else {
					float time_rate = (t_time_value - before_time_value) / end_time;
					Vector3 move_vec = (to_vec - from_vec) * time_rate;
					mid_vec -= move_vec;
					target_obj.Translate(move_vec);
				}
			}
		}
	}

	public void setInfo(Transform t_target, float t_due_time, Vector3 t_from, Vector3 t_to, Action<float> t_end_action, int t_time_type = 0, bool tIsAbsoluteValue = true)
	{
		target_obj = t_target;
		end_time = t_due_time;
		from_vec = t_from;
		to_vec = t_to;
		mid_vec = t_to - t_from;
		end_action = t_end_action;
		setTimeType (t_time_type);
		isAbsoluteValue = tIsAbsoluteValue;
	}

	public void startMove (float t_start_time)
	{
		if (end_time <= 0f) {
			if (isAbsoluteValue) {
				target_obj.localPosition = to_vec;
			} else {
				target_obj.Translate(to_vec - from_vec);
			}
			if (end_action != null)
				end_action (t_start_time);
			is_moving = false;
			
//			DestroyObject (gameObject);
		} else if (t_start_time >= end_time) {
			if (isAbsoluteValue) {
				target_obj.localPosition = to_vec;
			} else {
				target_obj.Translate(to_vec - from_vec);
			}
			if (end_action != null)
				end_action (t_start_time - end_time);
			is_moving = false;
			
//			DestroyObject (gameObject);
		} else {
			time_value = t_start_time;

			is_moving = true;
		}
	}

//	public void move(GameObject t_target, float t_due_time, Vector3 t_from, Vector3 t_to, float t_start_time, Action<float> t_end_action = null)
//	{
//		target_obj = t_target;
//		end_action = t_end_action;
//		if (t_due_time <= 0f) {
//			target_obj.transform.localPosition = t_to;
//			if(end_action != null)
//				end_action(0f);
//			is_moving = false;
//
//			DestroyObject(gameObject);
//		} else {
//			time_value = t_start_time;
//			end_time = t_due_time;
//			from_vec = t_from;
//			to_vec = t_to;
//			
//			is_moving = true;
//		}
//	}
}
