﻿using UnityEngine;
using System.Collections;
using System;

public class ActionSwapFromTo : ActionBase {

	bool is_moving = false;

	Vector3 to_vec;
	
	Vector3 origin_local_scale;

	Transform target_obj;

	public override void Update (float tDeltaTime)
	{
		if (is_moving) {
			time_value += tDeltaTime;
			if (time_value >= end_time) {
				target_obj.localPosition = to_vec;
				target_obj.localScale = origin_local_scale;

				if(end_action != null)
					end_action(time_value-end_time);

				is_moving = false;

				//				DestroyObject(gameObject);
			}
			else if(time_value < end_time/2f)
			{
				float time_rate = 1f - time_value/(end_time/2f);
				target_obj.localScale = new Vector3(origin_local_scale.x*time_rate, origin_local_scale.y*time_rate, origin_local_scale.z*time_rate);
			}
			else
			{
				target_obj.localPosition = to_vec;

				float time_rate = (time_value - end_time/2f)/(end_time/2f);
				target_obj.localScale = new Vector3(origin_local_scale.x*time_rate, origin_local_scale.y*time_rate, origin_local_scale.z*time_rate);
			}
		}
	}

	public void setInfo(Transform t_target, float t_due_time, Vector3 t_from, Vector3 t_to, Vector3 t_origin_scale, Action<float> t_end_func)
	{
		target_obj = t_target;
		end_time = t_due_time;
		to_vec = t_to;
		origin_local_scale = t_origin_scale;
		end_action = t_end_func;
	}

	public void startSwap (float t_start_time)
	{
		if (end_time <= 0f) {
			target_obj.localPosition = to_vec;
			target_obj.localScale = origin_local_scale;
			if (end_action != null)
				end_action (t_start_time);
			is_moving = false;
			
//			DestroyObject (gameObject);
		} else if (t_start_time >= end_time) {
			target_obj.localPosition = to_vec;
			target_obj.localScale = origin_local_scale;
			if (end_action != null)
				end_action (t_start_time - end_time);
			is_moving = false;
			
//			DestroyObject (gameObject);
		} else {
			time_value = t_start_time;
			
			is_moving = true;
		}
	}

//	public void swap(float t_due_time, Vector3 t_from, Vector3 t_to, Vector3 t_origin_scale)
//	{
//		origin_local_scale = t_origin_scale;
//		
//		if (t_due_time <= 0f) {
//			transform.localPosition = t_to;
//			is_moving = false;
//		} else {
//			time_value = 0f;
//			end_time = t_due_time;
//			from_vec = t_from;
//			to_vec = t_to;
//			
//			is_moving = true;
//		}
//	}
}
