﻿using UnityEngine;
using System.Collections;
using System;

public class ActionScaleFromTo : ActionBase {

	bool is_ing = false;
	
	Vector3 from_scale;
	Vector3 to_scale;
	
	Transform target_obj;

	public override void Update (float tDeltaTime)
	{
		if (is_ing) {
			time_value += tDeltaTime;
			float t_time_value = time_func (time_value);

			if (time_value >= end_time) {
				target_obj.localScale = to_scale;
				if(end_action != null)
					end_action(time_value-end_time);
				is_ing = false;

				//				DestroyObject(gameObject);
			}
			else
			{
				float time_rate = t_time_value/end_time;
				Vector3 sub_scale = to_scale - from_scale;
				sub_scale.x *= time_rate;
				sub_scale.y *= time_rate;
				sub_scale.z *= time_rate;

				target_obj.localScale = from_scale + sub_scale;
			}
		}
	}

	public void setInfo(Transform t_target, float t_due_time, Vector3 t_from, Vector3 t_to, Action<float> t_end_action, int t_time_type = 0)
	{
		target_obj = t_target;
		end_time = t_due_time;
		from_scale = t_from;
		to_scale = t_to;
		end_action = t_end_action;
		setTimeType (t_time_type);
	}
	
	public void startScale (float t_start_time)
	{
		if (end_time <= 0f) {
			target_obj.localScale = to_scale;
			if (end_action != null)
				end_action (t_start_time);
			is_ing = false;
			
//			DestroyObject (gameObject);
		} else if (t_start_time >= end_time) {
			target_obj.localScale = to_scale;
			if (end_action != null)
				end_action (t_start_time - end_time);
			is_ing = false;
			
//			DestroyObject (gameObject);
		} else {
			time_value = t_start_time;
			
			is_ing = true;
		}
	}
}
