﻿using UnityEngine;
using System.Collections;
using System;

public class ActionSizeFromTo : ActionBase {

	bool is_moving = false;
	
	Vector2 from_size;
	Vector2 to_size;
	
	GameObject target_obj;
	RectTransform target_trans;

	public override void Update (float tDeltaTime)
	{
		if (is_moving) {
			time_value += tDeltaTime;
			float t_time_value = time_func(time_value);

			if (time_value >= end_time) {
				target_trans.sizeDelta = to_size;
				if(end_action != null)
					end_action(time_value-end_time);
				is_moving = false;

				//				DestroyObject(gameObject);
			}
			else
			{
				float time_rate = t_time_value/end_time;
				Vector2 sub_vec = to_size - from_size;
				sub_vec.x *= time_rate;
				sub_vec.y *= time_rate;

				target_trans.sizeDelta = from_size + sub_vec;
			}
		}
	}

	public void setInfo(GameObject t_target, float t_due_time, Vector2 t_from, Vector2 t_to, Action<float> t_end_action, int t_time_type = 0)
	{
		target_obj = t_target;
		target_trans = target_obj.GetComponent<RectTransform> ();
		end_time = t_due_time;
		from_size = t_from;
		to_size = t_to;
		end_action = t_end_action;
		setTimeType (t_time_type);
	}
	
	public void startSize (float t_start_time)
	{
		if (end_time <= 0f) {
			target_trans.sizeDelta = to_size;
			if (end_action != null)
				end_action (t_start_time);
			is_moving = false;
			
//			DestroyObject (gameObject);
		} else if (t_start_time >= end_time) {
			target_trans.sizeDelta = to_size;
			if (end_action != null)
				end_action (t_start_time - end_time);
			is_moving = false;
			
//			DestroyObject (gameObject);
		} else {
			time_value = t_start_time;
			
			is_moving = true;
		}
	}
}
