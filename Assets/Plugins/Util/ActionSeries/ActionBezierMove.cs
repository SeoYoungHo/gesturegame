﻿using UnityEngine;
using System.Collections;
using System;

public class ActionBezierMove : ActionBase {

	bool is_moving = false;
	
	Vector3 from_vec;
	Vector3 t_from_vec;
	Vector3 mid_vec1;
	Vector3 mid_vec2;
	Vector3 to_vec;
	Vector3 previous_vec;
	bool auto_rotation = false;
	
	Transform target_obj;

	public override void Update (float tDeltaTime)
	{
		if (is_moving) {
			time_value += tDeltaTime;
			float t_time_value = time_func(time_value);

			if (time_value >= end_time) {
				target_obj.localPosition = to_vec + from_vec;
				if(end_action != null)
					end_action(time_value-end_time);
				is_moving = false;

				//				DestroyObject(gameObject);
			}
			else
			{
				float time_rate = t_time_value/end_time;

				float xa = 0f;
				float xb = mid_vec1.x;
				float xc = mid_vec2.x;
				float xd = to_vec.x;

				float ya = 0;
				float yb = mid_vec1.y;
				float yc = mid_vec2.y;
				float yd = to_vec.y;

				float x = bezierat(xa, xb, xc, xd, time_rate);
				float y = bezierat(ya, yb, yc, yd, time_rate);

				Vector3 currentPos = target_obj.localPosition;
				Vector3 diff = currentPos - previous_vec;
				t_from_vec = t_from_vec + diff;

				Vector3 newPos = t_from_vec + new Vector3(x,y);

				if (auto_rotation) {
					float tAngle = Mathf.Atan2 (newPos.y - target_obj.localPosition.y, newPos.x - target_obj.localPosition.x) * Mathf.Rad2Deg;
					target_obj.localEulerAngles = new Vector3 (0f, 0f, tAngle);
				}

				target_obj.localPosition = newPos;

				previous_vec = newPos;
			}
		}
	}

	float bezierat( float a, float b, float c, float d, float t )
	{
		return (Mathf.Pow(1f-t,3f) * a + 
		        3f*t*(Mathf.Pow(1f-t,2f))*b + 
		        3f*Mathf.Pow(t,2f)*(1f-t)*c +
		        Mathf.Pow(t,3f)*d );
	}
	
	public void setInfo(Transform t_target, float t_due_time, Vector3 t_from, Vector3 t_mid_vec1, Vector3 t_mid_vec2, Vector3 t_to, Action<float> t_end_action, int t_time_type = 0, bool tAutoRotation = false)
	{
		target_obj = t_target;
		end_time = t_due_time;
		from_vec = t_from;
		t_from_vec = t_from;
		previous_vec = from_vec;
		mid_vec1 = t_mid_vec1 - from_vec;
		mid_vec2 = t_mid_vec2 - from_vec;
		to_vec = t_to - from_vec;
		end_action = t_end_action;
		setTimeType (t_time_type);
		auto_rotation = tAutoRotation;
	}
	
	public void startMove (float t_start_time)
	{
		if (end_time <= 0f) {
			if (auto_rotation) {
				float tAngle = Mathf.Atan2 (to_vec.y - from_vec.y, to_vec.x - from_vec.x) * Mathf.Rad2Deg;
				target_obj.localEulerAngles = new Vector3 (0f, 0f, tAngle);
			}
			target_obj.localPosition = to_vec + from_vec;
			if (end_action != null)
				end_action (t_start_time);
			is_moving = false;


//			DestroyObject (gameObject);
		} else if (t_start_time >= end_time) {
			if (auto_rotation) {
				float tAngle = Mathf.Atan2 (to_vec.y - from_vec.y, to_vec.x - from_vec.x) * Mathf.Rad2Deg;
				target_obj.localEulerAngles = new Vector3 (0f, 0f, tAngle);
			}
			target_obj.localPosition = to_vec + from_vec;
			if (end_action != null)
				end_action (t_start_time - end_time);
			is_moving = false;
			
//			DestroyObject (gameObject);
		} else {
			time_value = t_start_time;
			
			is_moving = true;
		}
	}
}
