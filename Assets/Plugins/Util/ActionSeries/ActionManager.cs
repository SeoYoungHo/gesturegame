﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

class ActionInfo{
	public int m_type;
}

class ActionInfoMoveFromTo : ActionInfo{
	public Transform target_obj;
	public float due_time;
	public Vector3 from_vec;
	public Vector3 to_vec;
	public int time_type;
	public bool is_absolute;
}

class ActionInfoCallFunc : ActionInfo{
	public Action call_func;
}

class ActionInfoDelayTime : ActionInfo{
	public float due_time;
	public Action call_func;
}

class ActionInfoSwapFromTo : ActionInfo{
	public Transform target_obj;
	public float due_time;
	public Vector3 from_vec;
	public Vector3 to_vec;
	public Vector3 origin_scale;
}

class ActionInfoFallFromTo : ActionInfo{
	public Transform target_obj;
	public Vector3 from_vec;
	public Vector3 to_vec;
}

class ActionInfoFadeFromTo : ActionInfo{
	public UnityEngine.UI.Image target_img;
	public float due_time;
	public Color from_color;
	public Color to_color;
}

class ActionInfoRotateFromTo : ActionInfo{
	public Transform target_obj;
	public float due_time;
	public double from_angle;
	public double to_angle;
	public int time_type;
}

class ActionInfoSizeFromTo : ActionInfo{
	public GameObject target_obj;
	public float due_time;
	public Vector2 from_size;
	public Vector2 to_size;
	public int time_type;
}

class ActionInfoBezierMove : ActionInfo{
	public Transform target_obj;
	public float due_time;
	public Vector3 from_vec;
	public Vector3 mid_vec1;
	public Vector3 mid_vec2;
	public Vector3 to_vec;
	public int time_type;
	public bool auto_rotation;
}

class ActionInfoVecFallFromTo : ActionInfo{
	public Transform target_obj;
	public Vector3 from_vec;
	public Vector3 to_vec;
	public Vector3 v0;
	public float gravity_value;
}

class ActionInfoFadeFromToSpriteRenderer : ActionInfo{
	public UnityEngine.SpriteRenderer target_img;
	public float due_time;
	public Color from_color;
	public Color to_color;
}

class ActionInfoFadeFromToText : ActionInfo{
	public UnityEngine.UI.MaskableGraphic target_img;
	public float due_time;
	public Color from_color;
	public Color to_color;
	public bool is_tm_pro;
}

public class ActionData
{
	List<ActionInfo> action_list = new List<ActionInfo>();
	List<Action<float>> action_stack = new List<Action<float>> ();
	List<ActionBase> acting_objs = new List<ActionBase> ();

	public ActionManager a_manager;
//	bool is_acting = false;

//	public void clear()
//	{
//		foreach(var t_obj in acting_objs)
//			DestroyObject(t_obj);
//		
//		acting_objs.Clear ();
//		
//		is_acting = false;
//		action_list.Clear ();
//		action_stack.Clear ();
//	}

	
	public void addMoveBy(GameObject t_target, float t_due_time, Vector3 t_by, int t_time_type = 0, bool t_is_absolute = true)
	{
		Vector3 t_from = t_target.transform.localPosition;
		Vector3 t_to = t_from + t_by;
		addMoveFromTo (t_target, t_due_time, t_from, t_to, t_time_type, t_is_absolute);
	}

	public void addMoveTo(GameObject t_target, float t_due_time, Vector3 t_to, int t_time_type = 0, bool t_is_absolute = true)
	{
		Vector3 t_from = t_target.transform.localPosition;
		addMoveFromTo (t_target, t_due_time, t_from, t_to, t_time_type, t_is_absolute);
	}

	public void addMoveFromTo(GameObject t_target, float t_due_time, Vector3 t_from, Vector3 t_to, int t_time_type = 0, bool t_is_absolute = true)
	{
		ActionInfoMoveFromTo t_info = new ActionInfoMoveFromTo ();
		t_info.target_obj = t_target.transform;
		t_info.due_time = t_due_time;
		t_info.from_vec = t_from;
		t_info.to_vec = t_to;
		t_info.m_type = 1;
		t_info.time_type = t_time_type;
		t_info.is_absolute = t_is_absolute;
		
		action_list.Add (t_info);
	}
	
	public void addCallFunc(Action t_func)
	{
		ActionInfoCallFunc t_info = new ActionInfoCallFunc ();
		t_info.call_func = t_func;
		t_info.m_type = 2;
		
		action_list.Add (t_info);
	}
	
	public void addDelayTime(float t_due_time, Action t_func)
	{
		ActionInfoDelayTime t_info = new ActionInfoDelayTime ();
		t_info.due_time = t_due_time;
		t_info.call_func = t_func;
		t_info.m_type = 3;
		
		action_list.Add (t_info);
	}

	public void addSwapFromTo(GameObject t_target, float t_due_time, Vector3 t_from, Vector3 t_to, Vector3 t_origin_scale)
	{
		ActionInfoSwapFromTo t_info = new ActionInfoSwapFromTo ();
		t_info.target_obj = t_target.transform;
		t_info.due_time = t_due_time;
		t_info.from_vec = t_from;
		t_info.to_vec = t_to;
		t_info.origin_scale = t_origin_scale;
		t_info.m_type = 4;
		
		action_list.Add (t_info);
	}

	public float addFallFromTo(GameObject t_target, Vector3 t_from, Vector3 t_to)
	{
		ActionInfoFallFromTo t_info = new ActionInfoFallFromTo ();
		t_info.target_obj = t_target.transform;
		t_info.from_vec = t_from;
		t_info.to_vec = t_to;
		t_info.m_type = 5;
		
		action_list.Add (t_info);

		return ActionFallFromTo.checkEndTime (t_info.from_vec, t_info.to_vec);
	}

	public void addFadeFromTo(UnityEngine.UI.Image t_target, float t_due_time, Color t_from, Color t_to)
	{
		ActionInfoFadeFromTo t_info = new ActionInfoFadeFromTo ();
		t_info.target_img = t_target;
		t_info.due_time = t_due_time;
		t_info.from_color = t_from;
		t_info.to_color = t_to;
		t_info.m_type = 6;

		action_list.Add (t_info);
	}
	
	public void addScaleTo(GameObject t_target, float t_due_time, Vector3 t_to)
	{
		var t_from = t_target.transform.localScale;
		addScaleFromTo (t_target,t_due_time,t_from,t_to);
	}

	public void addScaleFromTo(GameObject t_target, float t_due_time, Vector3 t_from, Vector3 t_to, int t_time_type = 0)
	{
		ActionInfoMoveFromTo t_info = new ActionInfoMoveFromTo ();
		t_info.target_obj = t_target.transform;
		t_info.due_time = t_due_time;
		t_info.from_vec = t_from;
		t_info.to_vec = t_to;
		t_info.m_type = 7;
		t_info.time_type = t_time_type;

		action_list.Add (t_info);
	}

	public void addRotateFromTo(GameObject t_target, float t_due_time, double t_from, double t_to, int t_time_type = 0)
	{
		ActionInfoRotateFromTo t_info = new ActionInfoRotateFromTo ();
		t_info.target_obj = t_target.transform;
		t_info.due_time = t_due_time;
		t_info.from_angle = t_from;
		t_info.to_angle = t_to;
		t_info.m_type = 8;
		t_info.time_type = t_time_type;

		action_list.Add (t_info);
	}

	public void addSizeFromTo(GameObject t_target, float t_due_time, Vector2 t_from, Vector2 t_to, int t_time_type = 0)
	{
		ActionInfoSizeFromTo t_info = new ActionInfoSizeFromTo ();
		t_info.target_obj = t_target;
		t_info.due_time = t_due_time;
		t_info.from_size = t_from;
		t_info.to_size = t_to;
		t_info.m_type = 9;
		t_info.time_type = t_time_type;
		
		action_list.Add (t_info);
	}

	public void addBezierMove(GameObject t_target, float t_due_time, Vector3 t_from, Vector3 t_mid1, Vector3 t_mid2, Vector3 t_to, int t_time_type = 0, bool tAutoRotation = false)
	{
		ActionInfoBezierMove t_info = new ActionInfoBezierMove ();
		t_info.target_obj = t_target.transform;
		t_info.due_time = t_due_time;
		t_info.from_vec = t_from;
		t_info.mid_vec1 = t_mid1;
		t_info.mid_vec2 = t_mid2;
		t_info.to_vec = t_to;
		t_info.m_type = 10;
		t_info.time_type = t_time_type;
		t_info.auto_rotation = tAutoRotation;
		
		action_list.Add (t_info);
	}

	public float addVecFallFromTo(GameObject t_target, Vector3 t_from, Vector3 t_to, Vector3 t_v0, float t_gravity_value)
	{
		ActionInfoVecFallFromTo t_info = new ActionInfoVecFallFromTo ();
		t_info.target_obj = t_target.transform;
		t_info.from_vec = t_from;
		t_info.to_vec = t_to;
		t_info.v0 = t_v0;
		t_info.gravity_value = t_gravity_value;
		t_info.m_type = 11;
		
		action_list.Add (t_info);
		
		return ActionVecFallFromTo.checkEndTime (t_info.from_vec, t_info.to_vec, t_info.v0.x, t_info.v0.y, t_info.gravity_value);
	}

	public void addFadeFromToSpriteRenderer(UnityEngine.SpriteRenderer t_target, float t_due_time, Color t_from, Color t_to)
	{
		ActionInfoFadeFromToSpriteRenderer t_info = new ActionInfoFadeFromToSpriteRenderer ();
		t_info.target_img = t_target;
		t_info.due_time = t_due_time;
		t_info.from_color = t_from;
		t_info.to_color = t_to;
		t_info.m_type = 12;

		action_list.Add (t_info);
	}

	public void addFadeFromToText(UnityEngine.UI.MaskableGraphic t_target, float t_due_time, Color t_from, Color t_to, bool t_is_tm_pro = false)
	{
		ActionInfoFadeFromToText t_info = new ActionInfoFadeFromToText ();
		t_info.target_img = t_target;
		t_info.due_time = t_due_time;
		t_info.from_color = t_from;
		t_info.to_color = t_to;
		t_info.is_tm_pro = t_is_tm_pro;
		t_info.m_type = 13;

		action_list.Add (t_info);
	}

	public void onAction()
	{
		action_stack.Clear ();
		action_stack.Add ((t_start_time) => {
			a_manager.removeActionData(this);
//			DestroyObject(gameObject);
		});
		
		for (int i=action_list.Count-1; i>=0; --i) {
			
			int action_stack_count = action_stack.Count;
			
			if(action_list[i].m_type == 1)
			{
				ActionInfoMoveFromTo t_info = (ActionInfoMoveFromTo)action_list[i];
				
				Action<float> t_action = (t_start_time) => {
//					GameObject t_obj = new GameObject ();
//					t_obj.transform.SetParent (transform);
//					acting_objs.Add(t_obj);
					ActionMoveFromTo t_action_move = new ActionMoveFromTo();// t_obj.AddComponent<ActionMoveFromTo> ();
					acting_objs.Add(t_action_move);
					t_action_move.setInfo(t_info.target_obj, t_info.due_time, t_info.from_vec, t_info.to_vec, (tt_start_time) => {
						acting_objs.Remove(t_action_move);
						action_stack[action_stack_count-1](tt_start_time);
					}, t_info.time_type, t_info.is_absolute); 
					t_action_move.startMove(t_start_time);
				};
				
				action_stack.Add(t_action);
			} else if(action_list[i].m_type == 2) {
				ActionInfoCallFunc t_info = (ActionInfoCallFunc)action_list[i];
				
				Action<float> t_action = (t_start_time) => {
					if(t_info.call_func != null)
					{
						t_info.call_func();
					}
					action_stack[action_stack_count-1](t_start_time);
				};
				
				action_stack.Add (t_action);
			} else if(action_list[i].m_type == 3) {
				ActionInfoDelayTime t_info = (ActionInfoDelayTime)action_list[i];
				
				Action<float> t_action = (t_start_time) => {
//					GameObject t_obj = new GameObject();
//					t_obj.transform.SetParent(transform);
//					acting_objs.Add(t_obj);
					ActionDelayTime t_action_delay = new ActionDelayTime();//t_obj.AddComponent<ActionDelayTime>();
					acting_objs.Add(t_action_delay);
					t_action_delay.setInfo(t_info.due_time, (tt_start_time) => {
						if(t_info.call_func != null)
						{
							t_info.call_func();
						}
						acting_objs.Remove(t_action_delay);
						action_stack[action_stack_count-1](tt_start_time);
					});
					t_action_delay.startDelay(t_start_time);
				};
				
				action_stack.Add (t_action);
			} else if(action_list[i].m_type == 4)
			{
				ActionInfoSwapFromTo t_info = (ActionInfoSwapFromTo)action_list[i];
				
				Action<float> t_action = (t_start_time) => {
//					GameObject t_obj = new GameObject ();
//					t_obj.transform.SetParent (transform);
//					acting_objs.Add(t_obj);
					ActionSwapFromTo t_action_move = new ActionSwapFromTo();//t_obj.AddComponent<ActionSwapFromTo> ();
					acting_objs.Add(t_action_move);
					t_action_move.setInfo(t_info.target_obj, t_info.due_time, t_info.from_vec, t_info.to_vec, t_info.origin_scale, (tt_start_time) => {
						acting_objs.Remove(t_action_move);
						action_stack[action_stack_count-1](tt_start_time);
					}); 
					t_action_move.startSwap(t_start_time);
				};
				
				action_stack.Add(t_action);
			} else if(action_list[i].m_type == 5)
			{
				ActionInfoFallFromTo t_info = (ActionInfoFallFromTo)action_list[i];
				
				Action<float> t_action = (t_start_time) => {
//					GameObject t_obj = new GameObject ();
//					t_obj.transform.SetParent (transform);
//					acting_objs.Add(t_obj);
					ActionFallFromTo t_action_move = new ActionFallFromTo();// t_obj.AddComponent<ActionFallFromTo> ();
					acting_objs.Add(t_action_move);
					t_action_move.setInfo(t_info.target_obj, t_info.from_vec, t_info.to_vec, (tt_start_time) => {
						acting_objs.Remove(t_action_move);
						action_stack[action_stack_count-1](tt_start_time);
					}); 
					t_action_move.startFall(t_start_time);
				};
				
				action_stack.Add(t_action);
			} else if(action_list[i].m_type == 6)
			{
				ActionInfoFadeFromTo t_info = (ActionInfoFadeFromTo)action_list[i];
				
				Action<float> t_action = (t_start_time) => {
//					GameObject t_obj = new GameObject ();
//					t_obj.transform.SetParent (transform);
//					acting_objs.Add(t_obj);
					ActionFadeFromTo t_action_move = new ActionFadeFromTo();// t_obj.AddComponent<ActionFadeFromTo> ();
					acting_objs.Add(t_action_move);
					t_action_move.setInfo(t_info.target_img, t_info.due_time, t_info.from_color, t_info.to_color, (tt_start_time) => {
						acting_objs.Remove(t_action_move);
						action_stack[action_stack_count-1](tt_start_time);
					}); 
					t_action_move.startFade(t_start_time);
				};
				
				action_stack.Add(t_action);
			} else if(action_list[i].m_type == 7)
			{
				ActionInfoMoveFromTo t_info = (ActionInfoMoveFromTo)action_list[i];
				
				Action<float> t_action = (t_start_time) => {
//					GameObject t_obj = new GameObject ();
//					t_obj.transform.SetParent (transform);
//					acting_objs.Add(t_obj);
					ActionScaleFromTo t_action_move = new ActionScaleFromTo();// t_obj.AddComponent<ActionScaleFromTo> ();
					acting_objs.Add(t_action_move);
					t_action_move.setInfo(t_info.target_obj, t_info.due_time, t_info.from_vec, t_info.to_vec, (tt_start_time) => {
						acting_objs.Remove(t_action_move);
						action_stack[action_stack_count-1](tt_start_time);
					}, t_info.time_type); 
					t_action_move.startScale(t_start_time);
				};
				
				action_stack.Add(t_action);
			} else if(action_list[i].m_type == 8)
			{
				ActionInfoRotateFromTo t_info = (ActionInfoRotateFromTo)action_list[i];
				
				Action<float> t_action = (t_start_time) => {
//					GameObject t_obj = new GameObject ();
//					t_obj.transform.SetParent (transform);
//					acting_objs.Add(t_obj);
					ActionRotateFromTo t_action_move = new ActionRotateFromTo();// t_obj.AddComponent<ActionRotateFromTo> ();
					acting_objs.Add(t_action_move);
					t_action_move.setInfo(t_info.target_obj, t_info.due_time, t_info.from_angle, t_info.to_angle, (tt_start_time) => {
						acting_objs.Remove(t_action_move);
						action_stack[action_stack_count-1](tt_start_time);
					}, t_info.time_type); 
					t_action_move.startRotate(t_start_time);
				};
				
				action_stack.Add(t_action);
			} else if(action_list[i].m_type == 9)
			{
				ActionInfoSizeFromTo t_info = (ActionInfoSizeFromTo)action_list[i];
				
				Action<float> t_action = (t_start_time) => {
//					GameObject t_obj = new GameObject ();
//					t_obj.transform.SetParent (transform);
//					acting_objs.Add(t_obj);
					ActionSizeFromTo t_action_move = new ActionSizeFromTo();// t_obj.AddComponent<ActionSizeFromTo> ();
					acting_objs.Add(t_action_move);
					t_action_move.setInfo(t_info.target_obj, t_info.due_time, t_info.from_size, t_info.to_size, (tt_start_time) => {
						acting_objs.Remove(t_action_move);
						action_stack[action_stack_count-1](tt_start_time);
					}, t_info.time_type); 
					t_action_move.startSize(t_start_time);
				};
				
				action_stack.Add(t_action);
			} else if(action_list[i].m_type == 10)
			{
				ActionInfoBezierMove t_info = (ActionInfoBezierMove)action_list[i];
				
				Action<float> t_action = (t_start_time) => {
//					GameObject t_obj = new GameObject ();
//					t_obj.transform.SetParent (transform);
//					acting_objs.Add(t_obj);
					ActionBezierMove t_action_move = new ActionBezierMove();// t_obj.AddComponent<ActionBezierMove> ();
					acting_objs.Add(t_action_move);
					t_action_move.setInfo(t_info.target_obj, t_info.due_time, t_info.from_vec, t_info.mid_vec1, t_info.mid_vec2, t_info.to_vec, (tt_start_time) => {
						acting_objs.Remove(t_action_move);
						action_stack[action_stack_count-1](tt_start_time);
					}, t_info.time_type, t_info.auto_rotation); 
					t_action_move.startMove(t_start_time);
				};
				
				action_stack.Add(t_action);
			} else if(action_list[i].m_type == 11)
			{
				ActionInfoVecFallFromTo t_info = (ActionInfoVecFallFromTo)action_list[i];
				
				Action<float> t_action = (t_start_time) => {
//					GameObject t_obj = new GameObject ();
//					t_obj.transform.SetParent (transform);
//					acting_objs.Add(t_obj);
					ActionVecFallFromTo t_action_move = new ActionVecFallFromTo();// t_obj.AddComponent<ActionVecFallFromTo> ();
					acting_objs.Add(t_action_move);
					t_action_move.setInfo(t_info.target_obj, t_info.from_vec, t_info.to_vec, (tt_start_time) => {
						acting_objs.Remove(t_action_move);
						action_stack[action_stack_count-1](tt_start_time);
					}, t_info.v0.x, t_info.v0.y, t_info.gravity_value); 
					t_action_move.startFall(t_start_time);
				};
				
				action_stack.Add(t_action);
			} else if(action_list[i].m_type == 12)
			{
				ActionInfoFadeFromToSpriteRenderer t_info = (ActionInfoFadeFromToSpriteRenderer)action_list[i];

				Action<float> t_action = (t_start_time) => {
//					GameObject t_obj = new GameObject ();
//					t_obj.transform.SetParent (transform);
//					acting_objs.Add(t_obj);
					ActionFadeFromToSpriteRenderer t_action_move = new ActionFadeFromToSpriteRenderer();// t_obj.AddComponent<ActionFadeFromToSpriteRenderer> ();
					acting_objs.Add(t_action_move);
					t_action_move.setInfo(t_info.target_img, t_info.due_time, t_info.from_color, t_info.to_color, (tt_start_time) => {
						acting_objs.Remove(t_action_move);
						action_stack[action_stack_count-1](tt_start_time);
					}); 
					t_action_move.startFade(t_start_time);
				};

				action_stack.Add(t_action);
			} else if(action_list[i].m_type == 13)
			{
				ActionInfoFadeFromToText t_info = (ActionInfoFadeFromToText)action_list[i];

				Action<float> t_action = (t_start_time) => {
//					GameObject t_obj = new GameObject ();
//					t_obj.transform.SetParent (transform);
//					acting_objs.Add(t_obj);
					ActionFadeFromToText t_action_move = new ActionFadeFromToText();// t_obj.AddComponent<ActionFadeFromToText> ();
					acting_objs.Add(t_action_move);
					t_action_move.setInfo(t_info.target_img, t_info.due_time, t_info.from_color, t_info.to_color, (tt_start_time) => {
						acting_objs.Remove(t_action_move);
						action_stack[action_stack_count-1](tt_start_time);
					}, t_info.is_tm_pro); 
					t_action_move.startFade(t_start_time);
				};

				action_stack.Add(t_action);
			}
		}
		action_list.Clear ();
		if (action_stack.Count > 0) {
//			is_acting = true;
			action_stack [action_stack.Count - 1] (0f);
		}
	}

	public void Update(float tDeltaTime, float tUnscaledDeltaTime) {
		int tActingObjsCount = acting_objs.Count;

		if (tActingObjsCount > 0) {
			if (acting_objs [tActingObjsCount - 1].isUnscaledDeltaTime) {
				acting_objs [tActingObjsCount - 1].Update (tUnscaledDeltaTime);
			} else {
				acting_objs [tActingObjsCount - 1].Update (tDeltaTime);
			}
		}
	}
}

public class ActionManager : MonoBehaviour {

	List<ActionData> action_data_list = new List<ActionData>();
//	List<GameObject> action_objs = new List<GameObject>();

//	bool _isTick = false;
//	float _addDeltaTime = 0f;
//	float _addUnscaledDeltaTime = 0f;

	// Use this for initialization
	void Start () {
	
	}

	void Update() {
//		_isTick = !_isTick;
//		if (_isTick) {
		if (action_data_list.Count > 0) {
			float tDeltaTime = Time.deltaTime;// + _addDeltaTime;
			float tUnscaledDeltaTime = Time.unscaledDeltaTime;// + _addUnscaledDeltaTime;
			List<ActionData> tCopy = new List<ActionData> (action_data_list);
			int tActionDataListCount = tCopy.Count;
			for (int i = 0; i < tActionDataListCount; ++i) {
				tCopy [i].Update (tDeltaTime, tUnscaledDeltaTime);
			}
		}
//		} else {
//			if (action_data_list.Count > 0) {
//				_addDeltaTime = Time.deltaTime;
//				_addUnscaledDeltaTime = Time.unscaledDeltaTime;
//			}
//		}
	}

	public void clearAllAction()
	{
//		foreach (var t_obj in action_objs) {
//			Destroy(t_obj);
//		}

		action_data_list.Clear ();
//		action_objs.Clear ();
	}

	public bool isActing()
	{
		return action_data_list.Count > 0;
	}

	public void removeActionData(ActionData t_data)
	{
		action_data_list.Remove (t_data);
	}

	public ActionData addAction()
	{
//		GameObject t_obj = new GameObject ();
//		t_obj.transform.SetParent (transform);
//		action_objs.Add (t_obj);

		ActionData t_action_data = new ActionData ();// t_obj.AddComponent<ActionData> ();
		t_action_data.a_manager = this;
		action_data_list.Add (t_action_data);

		return t_action_data;
	}
}
