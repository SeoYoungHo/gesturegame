﻿using UnityEngine;
using System.Collections;
using System;
//using TMPro;

public class ActionFadeFromToText : ActionBase {

	bool is_ing = false;

	Color from_color;
	Color to_color;
	
	UnityEngine.UI.MaskableGraphic target_img;
//	TextMeshProUGUI tm_pro;
	bool is_tm_pro;

	public override void Update (float tDeltaTime)
	{
		if (is_ing) {
			time_value += tDeltaTime;
			if (time_value >= end_time) {
				if (is_tm_pro) {
//					tm_pro.color = to_color;
				} else {
					target_img.color = to_color;
				}
				if(end_action != null)
					end_action(time_value-end_time);
				is_ing = false;

//				DestroyObject(gameObject);
			}
			else
			{
				float time_rate = time_value/end_time;
				Color sub_vec = to_color - from_color;
				sub_vec.r *= time_rate;
				sub_vec.g *= time_rate;
				sub_vec.b *= time_rate;
				sub_vec.a *= time_rate;

				if (is_tm_pro) {
//					tm_pro.color = from_color + sub_vec;
				} else {
					target_img.color = from_color + sub_vec;
				}
			}
		}
	}

	public void setInfo(UnityEngine.UI.MaskableGraphic t_target, float t_due_time, Color t_from, Color t_to, Action<float> t_end_action, bool t_is_tm_pro)
	{
		target_img = t_target;
		end_time = t_due_time;
		from_color = t_from;
		to_color = t_to;
		is_tm_pro = t_is_tm_pro;
		if (is_tm_pro) {
//			tm_pro = target_img.GetComponent<TextMeshProUGUI> ();
		}
		end_action = t_end_action;
	}
	
	public void startFade (float t_start_time)
	{
		if (end_time <= 0f) {
			if (is_tm_pro) {
//				tm_pro.color = to_color;
			} else {
				target_img.color = to_color;
			}
			if (end_action != null)
				end_action (t_start_time);
			is_ing = false;
			
//			DestroyObject (gameObject);
		} else if (t_start_time >= end_time) {
			if (is_tm_pro) {
//				tm_pro.color = to_color;
			} else {
				target_img.color = to_color;
			}
			if (end_action != null)
				end_action (t_start_time - end_time);
			is_ing = false;
			
//			DestroyObject (gameObject);
		} else {
			time_value = t_start_time;
			
			is_ing = true;
		}
	}
}
