﻿using UnityEngine;
using System.Collections;
using System;

public abstract class ActionBase {

	protected Action<float> end_action = null;
	public delegate float timeFunc(float t_time);
	public timeFunc time_func = null;
	public int time_type = 0;
	protected float time_value;
	protected float end_time;
	public bool isUnscaledDeltaTime = false;

	public abstract void Update (float tDeltaTime);

	public void setTimeType(int t_time_type)
	{
		time_type = t_time_type;
		if (time_type == 1) { // ease in out
			time_func = easeInOutTime;
		} else if (time_type == 2) { // elastic in out
			time_func = elasticInOutTime;
		} else if (time_type == 3) { // ease back out
			time_func = easeBackOutTime;
		} else if (time_type == 4) {
			time_func = easeOutCubicTime;
		} else if (time_type == 5) {
			time_func = easeInCubicTime;
		} else {
			time_func = defaultTime;
		}
	}

	float defaultTime(float t_time)
	{
		return t_time;
	}

	float easeInCubicTime(float t_time) {
		float time = t_time / end_time;
		time = time * time * time;
		return time * end_time;
	}

	float easeOutCubicTime(float t_time) {
		float time = t_time / end_time;
		time -= 1;
		time = (time * time * time + 1);
		return time * end_time;
	}

	float easeInOutTime(float t_time)
	{
		float rate = 2f;
		float time = t_time / end_time;
		time = Mathf.Clamp01 (time);
		time *= 2f;
		if (time < 1f)
		{
			return 0.5f * Mathf.Pow(time, rate)*end_time;
		}
		else
		{
			return (1f - 0.5f * Mathf.Pow(2f - time, rate))*end_time;
		}
	}

	float elasticInOutTime(float t_time)
	{
		float period = 0.3f;
		float time = t_time / end_time;
		time = Mathf.Clamp01 (time);
		float newT = 0;
		if (time == 0f || time == 1f)
		{
			newT = time;
		}
		else
		{
			time = time * 2;
			if (period == 0f)
			{
				period = 0.3f * 1.5f;
			}
			
			float s = period / 4;
			
			time = time - 1;
			if (time < 0)
			{
				newT = -0.5f * Mathf.Pow(2f, 10f * time) * Mathf.Sin((time -s) * Mathf.PI*2f / period);
			}
			else
			{
				newT = Mathf.Pow(2f, -10f * time) * Mathf.Sin((time - s) * Mathf.PI*2f / period) * 0.5f + 1f;
			}
		}
		return newT*end_time;
	}

	float easeBackOutTime(float t_time)
	{
		float overshoot = 1.70158f;
		float time = t_time / end_time;
		time = time - 1f;
		return (time * time * ((overshoot + 1f) * time + overshoot) + 1f)*end_time;
	}
}
