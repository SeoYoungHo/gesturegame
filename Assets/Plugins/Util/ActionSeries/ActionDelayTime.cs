﻿using UnityEngine;
using System.Collections;
using System;

public class ActionDelayTime : ActionBase {

	float due_time = -1f;
	bool is_ing = false;

	public override void Update (float tDeltaTime)
	{
		if (is_ing) {
			time_value += tDeltaTime;
			if (time_value >= due_time) {
				is_ing = false;
				if(end_action != null)
				{
					//					Debug.Log("delay end");
					end_action(time_value - due_time);
				}
				//				DestroyObject(gameObject);
			}
		}
	}

	public void setInfo(float t_due_time, Action<float> t_end_action)
	{
		due_time = t_due_time;
		end_action = t_end_action;
	}

	public void startDelay(float t_start_time)
	{
		time_value = t_start_time;

//		Debug.LogFormat ("delay due_time : {0} , end_action : {1}, time_value : {2}", due_time, end_action, time_value);

		if (due_time <= 0f) {
			if(end_action != null)
				end_action(t_start_time);
//			DestroyObject(gameObject);
		}else if (time_value >= due_time) {
			if(end_action != null)
				end_action(time_value - due_time);
//			DestroyObject(gameObject);
		} else {
			is_ing = true;
		}
	}

//	public void delay(float t_due_time, Action t_end)
//	{
//		time_value = 0f;
//		due_time = t_due_time;
//		end_action = t_end;
//		
//		if (due_time == 0f) {
//			due_time = -1f;
//			if(end_action != null)
//				end_action();
//		}
//	}
}
