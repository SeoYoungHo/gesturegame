﻿using UnityEngine;
using System.Collections;
using System;

public class ActionVecFallFromTo : ActionBase {
	
	float gravity_value = -2500f;
	Vector3 v0 = new Vector3();
	bool is_moving = false;
	
	Vector3 from_vec;
	Vector3 to_vec;
	
	Transform target_obj;

	public override void Update (float tDeltaTime)
	{
		if (is_moving) {
			time_value += tDeltaTime;
			if (time_value >= end_time) {
				target_obj.localPosition = to_vec;

				if(end_action != null)
					end_action(time_value-end_time);

				is_moving = false;

//				DestroyObject(gameObject);
			}
			else
			{
				//				transform.localPosition = new Vector3(from_vec.x, from_vec.y - gravity_value*time_value*time_value/2f, from_vec.z);
				target_obj.localPosition = new Vector3(from_vec.x + v0.x*time_value, Mathf.Clamp(from_vec.y + (v0.y*time_value + gravity_value*time_value*time_value/2f), Mathf.Min(to_vec.y, from_vec.y), Mathf.Max(to_vec.y, from_vec.y)), from_vec.z);
			}
		}
	}
	
	public static float checkEndTime(Vector3 t_from, Vector3 t_to, float t_v0_x = 0f, float t_v0_y = -100f, float t_gravity_value = -2500f)
	{
		if (t_v0_x != 0f)
			return Mathf.Abs((t_to.x - t_from.x) / t_v0_x);
		else {
			float tInRoot = t_v0_y * t_v0_y + 2f * t_gravity_value * (t_to.y - t_from.y);
			if (tInRoot >= 0f) {
				float tValue1 = (-t_v0_y + Mathf.Sqrt (tInRoot)) / t_gravity_value;
				float tValue2 = (-t_v0_y - Mathf.Sqrt (tInRoot)) / t_gravity_value;
				return Mathf.Max (tValue1, tValue2);
//				return (-t_v0_y + Mathf.Sqrt (tInRoot)) / t_gravity_value;
			} else {
				return 0f;
			}
		}
	}

	public static Vector3 checkEndVector(Vector3 t_from, Vector3 t_to, float t_v0_x = 0f, float t_v0_y = -100f, float t_gravity_value = -2500f)
	{
		float t_end_time = checkEndTime (t_from, t_to, t_v0_x, t_v0_y, t_gravity_value);
		return new Vector3(t_v0_x, t_v0_y) + new Vector3 (0f, t_gravity_value)*t_end_time;
	}
	
	public void setInfo(Transform t_target, Vector3 t_from, Vector3 t_to, Action<float> t_end_action, float t_v0_x = 0f, float t_v0_y = -100f, float t_gravity_value = -2500f)
	{
		target_obj = t_target;
		from_vec = t_from;
		to_vec = t_to;
		v0 = new Vector3(t_v0_x, t_v0_y);
		gravity_value = t_gravity_value;
		end_time = checkEndTime (from_vec, to_vec, v0.x, v0.y, gravity_value);
		end_action = t_end_action;
	}
	
	public void startFall (float t_start_time)
	{
		if (end_time <= 0f) {
			target_obj.localPosition = to_vec;
			if (end_action != null)
				end_action (t_start_time);
			is_moving = false;
			
//			DestroyObject (gameObject);
		} else if (t_start_time >= end_time) {
			target_obj.localPosition = to_vec;
			if (end_action != null)
				end_action (t_start_time - end_time);
			is_moving = false;
			
//			DestroyObject (gameObject);
		} else {
			time_value = t_start_time;
			target_obj.localPosition = new Vector3(from_vec.x + v0.x*time_value, from_vec.y + (v0.y*time_value + gravity_value*time_value*time_value/2f), from_vec.z);
			
			is_moving = true;
		}
	}
}
