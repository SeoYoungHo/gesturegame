﻿using UnityEngine;
using System.Collections;
using System;

public class ActionRotateFromTo : ActionBase {

	bool is_moving = false;
	
	double from_angle;
	double to_angle;
	
	Transform target_obj;

	public override void Update (float tDeltaTime)
	{
		if (is_moving) {
			time_value += tDeltaTime;
			if (time_value >= end_time) {
				target_obj.localEulerAngles = new Vector3(0f,0f, (float)to_angle);
				if(end_action != null)
					end_action(time_value-end_time);
				is_moving = false;

//				DestroyObject(gameObject);
			}
			else
			{
				float time_rate = time_value/end_time;
				double sub_angle = to_angle - from_angle;
				sub_angle *= time_rate;

				target_obj.localEulerAngles = new Vector3(0f, 0f, (float)(from_angle + sub_angle));
			}
		}
	}

	public void setInfo(Transform t_target, float t_due_time, double t_from, double t_to, Action<float> t_end_action, int t_time_type)
	{
		target_obj = t_target;
		end_time = t_due_time;
		from_angle = t_from;
		to_angle = t_to;
		end_action = t_end_action;
		setTimeType (t_time_type);
	}
	
	public void startRotate (float t_start_time)
	{
		if (end_time <= 0f) {
			target_obj.localEulerAngles = new Vector3(0f,0f, (float)to_angle);
			if (end_action != null)
				end_action (t_start_time);
			is_moving = false;
			
//			DestroyObject (gameObject);
		} else if (t_start_time >= end_time) {
			target_obj.localEulerAngles = new Vector3(0f,0f, (float)to_angle);
			if (end_action != null)
				end_action (t_start_time - end_time);
			is_moving = false;
			
//			DestroyObject (gameObject);
		} else {
			time_value = t_start_time;
			
			is_moving = true;
		}
	}
}
