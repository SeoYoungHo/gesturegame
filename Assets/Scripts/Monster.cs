﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BigJamLib;

public class Monster : MonoBehaviour {

	private float _baseY;
	public SpriteRenderer uImage;
	public GameManager uGameManager;

	enum MonsterState {
		kStand = 0,
		kEmpty,
		kTrace,
		kAttack
	}
	private MonsterState _state;
	private ActionManager _actionManager;
	private Vector3 _endPosition;
	public Vector3 getEndPosition() {
		return _endPosition;
	}
	[HideInInspector]
	public float viewDirection = -1f;
	public Rect uBodyRect;
	public float uBodyAirPosition;
	public float uBodyHeight;
	public SpaceInfo getBodyInfo() {
		SpaceInfo tBodyInfo = new SpaceInfo ();
		tBodyInfo.position = _endPosition;
		tBodyInfo.rect = uBodyRect;
		tBodyInfo.airPosition = uBodyAirPosition;
		tBodyInfo.height = uBodyHeight;
		return tBodyInfo;
	}

	private List<Sprite> _moveAnimationSprites;
	private HashSet<AttackManager> _attackManagers = new HashSet<AttackManager> ();
	void Awake() {
		_endPosition = transform.localPosition;
		_moveAnimationSprites = new List<Sprite> ();
		_monsterHp = uMaxMonsterHp;
		_sight = uBaseSight;
		_baseY = uImage.transform.localPosition.y;
	}

	void Start () {
		for (int i = 1; i <= uImageCount; ++i) {
			_moveAnimationSprites.Add (ResourcesCache.Sprite.loadSprite (uImagePath + "[" + i.ToString () + "]"));
		}
		_actionManager = GetComponent<ActionManager> ();
		_state = MonsterState.kEmpty;
	}

	private int _ingAnimationIndex;
	private float _ingAnimationTime;
	private float _animationTerm = 0.1f;

	public float uBaseSight = 150f;
	private float _sight;
	public float uMaxSight = 300f;
	public float uDecreaseSight = 1f;
	public Rect uAttackRect;
	public float uAttackAirPosition;
	public float uAttackHeight;
	public float uMoveDistance = 5f;
	public float uAttackCool = 2f;
	public float uDamage = 30f;
	public float uAttackLoadTime = 0.7f;
	public string uImagePath = "Monsters/C3_FABRE";
	public int uImageCount = 4;
	public float uMaxMonsterHp = 10f;
	private float _monsterHp;
	public int uLevel;
	public float uExp;
	public float uGold;

	void Update() {
		if (_state == MonsterState.kEmpty) {
			SpaceInfo tCharacterBodyInfo = uGameManager.getCharacterBodyInfo ();
			SpaceInfo tMonsterBodyInfo = getBodyInfo ();
			Vector3 tDistance = tCharacterBodyInfo.position - tMonsterBodyInfo.position;
			float tDistanceX = Mathf.Max (Mathf.Abs (tDistance.x) - (tCharacterBodyInfo.rect.width + tMonsterBodyInfo.rect.width) / 2f, 0f);
			float tDistanceY = Mathf.Max (Mathf.Abs (tDistance.y) - (tCharacterBodyInfo.rect.height + tMonsterBodyInfo.rect.height) / 2f, 0f);
			float tDistanceResult = Mathf.Max (tDistanceX, tDistanceY);

			SpaceInfo tAttackSpaceInfo = new SpaceInfo ();
			tAttackSpaceInfo.position = _endPosition;
			if (viewDirection < 0f) {
				tAttackSpaceInfo.position = tAttackSpaceInfo.position + new Vector3 (-uAttackRect.x - uAttackRect.width, 0f);
			}
			tAttackSpaceInfo.rect = uAttackRect;
			tAttackSpaceInfo.airPosition = uAttackAirPosition;
			tAttackSpaceInfo.height = uAttackHeight;

			if (SpaceInfo.isOverlaped(tCharacterBodyInfo, tAttackSpaceInfo)) {
				_state = MonsterState.kAttack;
				uImage.color = new Color(1f, 0.7f, 0.7f, 1f);
				ActionData tAttackAction = _actionManager.addAction ();
				tAttackAction.addMoveTo (uImage.gameObject, uAttackLoadTime, uImage.transform.localPosition + new Vector3 (20f, 0f));
				tAttackAction.addMoveFromTo (uImage.gameObject, 0.15f, uImage.transform.localPosition + new Vector3 (20f, 0f), uImage.transform.localPosition + new Vector3 (-uAttackRect.x - uAttackRect.width, 0f), 4);
				tAttackAction.addCallFunc (() => {
					AttackManager tAttack = Instantiate<GameObject> (ResourcesCache.GameObject.loadGameObject ("Prefabs/AttackManager")).GetComponent<AttackManager> ();
					tAttack.transform.SetParent (transform);
					tAttack.damageMin = uDamage;
					tAttack.damageMax = uDamage;
					tAttack.isCharacter = false;
					tAttack.spaceInfo = new SpaceInfo (tAttackSpaceInfo);
					tAttack.uGameManager = uGameManager;
					_attackManagers.Add (tAttack);

					tAttack.startAttack (0.1f, () => {
						_attackManagers.Remove(tAttack);
					});

					uImage.color = Color.white;
				});
				tAttackAction.addMoveFromTo (uImage.gameObject, 0.15f, uImage.transform.localPosition + new Vector3 (-uAttackRect.x - uAttackRect.width, 0f), uImage.transform.localPosition, 5);
				tAttackAction.addDelayTime (uAttackCool, () => {
					_state = MonsterState.kEmpty;
				});
				tAttackAction.onAction ();
			} else if (tDistanceResult <= _sight) {
				_state = MonsterState.kTrace;
				_ingAnimationIndex = 0;
				_ingAnimationTime = 0f;
				uImage.sprite = _moveAnimationSprites [_ingAnimationIndex];
				Vector3 tEndMovePosition;
				if (tDistanceResult < uMoveDistance * uImageCount) {
					tEndMovePosition = transform.localPosition + new Vector3 (tDistance.x > 0f ? tDistanceX : -tDistanceX, tDistance.y > 0f ? tDistanceY : -tDistanceY);
				} else {
					float tAngle = Mathf.Atan2 (tDistance.y, tDistance.x);
					tEndMovePosition = transform.localPosition + new Vector3 (Mathf.Cos(tAngle) * uMoveDistance * uImageCount, Mathf.Sin(tAngle) * uMoveDistance * uImageCount);
				}

				if (tDistance.x > 0f) {
					transform.localScale = new Vector3 (-1f, 1f, 1f);
					viewDirection = 1f;
				} else {
					transform.localScale = new Vector3 (1f, 1f, 1f);
					viewDirection = -1f;
				}

				ActionData tMoveAction = _actionManager.addAction ();
				tMoveAction.addMoveTo (gameObject, 0.1f * uImageCount, tEndMovePosition);
				tMoveAction.onAction ();
				_endPosition = tEndMovePosition;
			} else {
				_state = MonsterState.kStand;
				ActionData tStandAction = _actionManager.addAction ();
				tStandAction.addDelayTime (0.5f, () => {
					_state = MonsterState.kEmpty;
				});
				tStandAction.onAction ();
			}
		} else if (_state == MonsterState.kTrace) {
			_ingAnimationTime += Time.deltaTime;
			if (_ingAnimationTime >= _animationTerm) {
				_ingAnimationTime -= _animationTerm;
				_ingAnimationIndex = (_ingAnimationIndex + 1) % _moveAnimationSprites.Count;
				uImage.sprite = _moveAnimationSprites [_ingAnimationIndex];
				if (_ingAnimationIndex == 0) {
					_state = MonsterState.kEmpty;
				}
			}
		}

		if (_sight > uBaseSight) {
			_sight = Mathf.Clamp(_sight - uDecreaseSight, uBaseSight, uMaxSight);
		}
	}

	public void onDamaged(float tDamage) {
		if (_state == MonsterState.kAttack) {
			_actionManager.clearAllAction ();
			foreach (var tAttack in _attackManagers) {
				Destroy (tAttack.gameObject);
			}
			_attackManagers.Clear ();
			uImage.transform.localPosition = new Vector3 (0f, _baseY);
			ActionData tAttackedDelay = _actionManager.addAction ();
			tAttackedDelay.addDelayTime (1f, () => {
				_state = MonsterState.kEmpty;
			});
			tAttackedDelay.onAction ();
		}

		uGameManager.showDamageText (tDamage, transform.localPosition + uGameManager.transform.localPosition + new Vector3(0f + Random.Range(-5f, 5f), 20f + Random.Range(-5f, 5f)));

		_monsterHp = Mathf.Clamp(_monsterHp - tDamage, 0f, uMaxMonsterHp);
		uGameManager.damagedMonster (_monsterHp/uMaxMonsterHp, this);
		uImage.color = Color.red;
		ActionData tAttackedAction = _actionManager.addAction ();
		tAttackedAction.addDelayTime (0.1f, () => {
			uImage.color = Color.white;
		});
		tAttackedAction.onAction ();

		_sight = Mathf.Clamp(_sight * 1.5f, uBaseSight, uMaxSight);

		uGameManager.shakeAttacked (Mathf.Clamp(tDamage / 5f * 3f, 1f, 3f));
	}
}
