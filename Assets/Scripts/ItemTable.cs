﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BicJSON;

public class ItemTable {
	private static ItemTable _instance = null;
	private static ItemTable getInstance() {
		if (_instance == null) {
			_instance = new ItemTable ();
			HashSet<string> tMonsters = new HashSet<string> ();
			tMonsters.Add ("AntEgg");
			tMonsters.Add ("Fabre");
			tMonsters.Add ("Cookie");
			tMonsters.Add ("GiantHonet");
			tMonsters.Add ("LeafCat");
			tMonsters.Add ("Marionette");
			_instance.initMonsterDropItems (tMonsters);
		}
		return _instance;
	}

	public static HashSet<PropAndId> getDropList(string tName) {
		if (getInstance ().dropItems.ContainsKey (tName)) {
			return getInstance ().dropItems [tName];
		} else {
			return new HashSet<PropAndId> ();
		}
	}

	private Dictionary<string, HashSet<PropAndId>> dropItems = new Dictionary<string, HashSet<PropAndId>> ();
	private void initMonsterDropItems(HashSet<string> tMonsterNames) {
		dropItems.Clear ();
		HashSet<string> tItems = new HashSet<string> ();
		foreach (string tMonsterName in tMonsterNames) {
			HashSet<PropAndId> tPropAndIds = new HashSet<PropAndId> ();
			switch (tMonsterName) {
			case "AntEgg":
//				tPropAndIds.Add(new PropAndId(0.1f, "UsedKnife"));
				break;
			case "Fabre":
				tPropAndIds.Add(new PropAndId(0.2f, "UsedKnife"));
				break;
			case "Cookie":
				break;
			case "GiantHonet":
				break;
			case "LeafCat":
				break;
			case "Marionette":
				break;
			default:
				break;
			}
			dropItems.Add (tMonsterName, tPropAndIds);
			foreach (var tPropAndId in tPropAndIds) {
				tItems.Add (tPropAndId.itemId);
			}
		}
		initDropItemInfos (tItems);
	}

	public static JSONValue getDropItemInfo(string tItemId) {
		JSONValue tDropItemInfo;
		if (getInstance ().dropItemInfos.ContainsKey (tItemId)) {
			tDropItemInfo = getInstance ().dropItemInfos [tItemId];
		} else {
			Debug.Log ("not have item info!!! : " + tItemId);
			return new JSONValue ();
		}

		JSONValue tInvenItemInfo = new JSONValue ();
		string tItemType = tDropItemInfo ["type"].AsString;
		tInvenItemInfo ["type"].AsString = tItemType;
		tInvenItemInfo ["name"].AsString = tDropItemInfo ["name"].AsString;
		tInvenItemInfo ["baseImage"].AsString = tDropItemInfo ["baseImage"].AsString;
		tInvenItemInfo ["detailImage"].AsString = tDropItemInfo ["detailImage"].AsString;
		if (tItemType == "weapon1") {
			float tDropAtkRange = tDropItemInfo ["dropAtkRange"].AsFloat;
			float tRange = 1f + Random.Range (-tDropAtkRange, tDropAtkRange);
			tInvenItemInfo ["atkMin"].AsFloat = tDropItemInfo ["baseAtkMin"].AsFloat * tRange;
			tInvenItemInfo ["atkMax"].AsFloat = tDropItemInfo ["baseAtkMax"].AsFloat * tRange;
			tInvenItemInfo ["sellPrice"].AsFloat = tDropItemInfo ["baseSellPrice"].AsFloat * tRange;
		}
		return tInvenItemInfo;
	}

	private Dictionary<string, JSONValue> dropItemInfos = new Dictionary<string, JSONValue>();
	private void initDropItemInfos(HashSet<string> tItemIds) {
		dropItemInfos.Clear ();
		foreach (string tItemId in tItemIds) {
			JSONValue tItemInfo = new JSONValue ();
			switch (tItemId) {
			case "UsedKnife":
				tItemInfo ["name"].AsString = "낡은 나이프";
				tItemInfo ["type"].AsString = "weapon1";
				tItemInfo ["baseImage"].AsString = "swords";
				tItemInfo ["detailImage"].AsString = "swords_0";
				tItemInfo ["dropAtkRange"].AsFloat = 0.3f;
				tItemInfo ["baseAtkMin"].AsFloat = 4f;
				tItemInfo ["baseAtkMax"].AsFloat = 6f;
				tItemInfo ["baseSellPrice"].AsFloat = 1000f;
				break;
			default :
				break;
			}
			dropItemInfos.Add (tItemId, tItemInfo);
		}
	}
}

public class PropAndId {
	public float prop;
	public string itemId;

	public PropAndId(float tProp, string tItemId) {
		prop = tProp;
		itemId = tItemId;
	}
}
