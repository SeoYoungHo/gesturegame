﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelToValue {

	public static float levelToAtk(int tLevel) {
		float tAtk = 10f;

		tAtk *= PowValue.getPowValue (1.2f, tLevel);

		return tAtk;
	}
}

public class PowValue {
	private static PowValue _instance = null;
	private static PowValue getInstance() {
		if (_instance == null) {
			_instance = new PowValue ();
		}
		return _instance;
	}

	private Dictionary<float, List<float>> powValues = new Dictionary<float, List<float>>();

	public static float getPowValue(float tBase, int tExt) {
		if (tExt <= 0) {
			Debug.Log ("Ext is not nature value!!");
		}

		PowValue tInstance = getInstance ();
		if (!tInstance.powValues.ContainsKey (tBase)) {
			List<float> tValues = new List<float> ();
			tValues.Add (tBase);
			tInstance.powValues.Add (tBase, tValues);
		}

		int tValueCount = tInstance.powValues [tBase].Count;
		while (tExt > tValueCount) {
			tInstance.powValues [tBase].Add (tInstance.powValues [tBase] [tValueCount - 1] * tBase);
			++tValueCount;
		}

		return tInstance.powValues [tBase] [tExt - 1];
	}
}