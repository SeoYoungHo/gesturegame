﻿using UnityEngine;
using System.Collections;
using BicJSON;
using UnityEngine.SceneManagement;

public class CharacterSelect : MonoBehaviour {

	public PopupCreateCharacter uPopupCreateCharacter;

	public void onClickedCharacterCell(string tName) {
		DataSingleton.keepData("CharacterName", new JSONValue(tName));
		SceneManager.LoadScene ("Town1");
	}

	public void onClickedCreateCharacter() {
		uPopupCreateCharacter.open ();
	}
}
