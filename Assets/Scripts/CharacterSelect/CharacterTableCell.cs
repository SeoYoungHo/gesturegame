﻿using UnityEngine;
using System.Collections;
using Tacticsoft;

public class CharacterTableCell : TableViewCell {

	public UnityEngine.UI.Text uText;
	[HideInInspector]
	public CharacterSelect uCharacterSelect;
	[HideInInspector]
	public string uName;

	public void setText(string tText){
		uText.text = tText;
	}

	public void onClickedCell() {
		uCharacterSelect.onClickedCharacterCell (uName);
	}
}
