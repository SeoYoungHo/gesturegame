﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Tacticsoft;
using BicJSON;

public class CharacterTable : MonoBehaviour, ITableViewDataSource {

	public TableView uCharacterTable;
	public CharacterTableCell uCharacterTableCellPrefabs;
	public CharacterSelect uCharacterSelect;

	int _createdCellCount = 0;

	void Awake() {
		uCharacterTable.dataSource = this;
	}

	// Use this for initialization
	void Start () {
		reloadTable ();
	}

	List<string> _characterList = new List<string>();
	public void reloadTable() {
		_characterList.Clear ();
		JSONValue tCharacterListData = DataController.loadFileToJson (ConstData.getDataFile (ConstData.DataFile.kCharacterList), true);

		int tCharacterListCount = tCharacterListData.Count;
		for (int i = 0; i < tCharacterListCount; ++i) {
			string tName = tCharacterListData [i].AsString;
			_characterList.Add (tName);
		}

		uCharacterTable.ReloadData ();
		uCharacterTable.scrollDistance = uCharacterTable.scrollableDistance;
	}

	#region ITableViewDataSource

	public int GetNumberOfRowsForTableView (TableView tableView)
	{
		return _characterList.Count;
	}

	public float GetHeightForRowInTableView (TableView tableView, int row)
	{
		return 50f;
	}

	public TableViewCell GetCellForRowInTableView (TableView tableView, int row)
	{
		CharacterTableCell tCell = uCharacterTable.GetReusableCell (uCharacterTableCellPrefabs.reuseIdentifier) as CharacterTableCell;
		if (tCell == null) {
			tCell = Instantiate<CharacterTableCell> (uCharacterTableCellPrefabs);
			tCell.name = "CharacterTableCell_" + getTableCellNumberAndIncrease (1).ToString ();
			tCell.uCharacterSelect = uCharacterSelect;
		}

		JSONValue tCharacterData = DataController.loadFileToJson (_characterList[row] + ".data", true);

		tCell.uName = _characterList [row];
		tCell.setText (_characterList[row] + " Lv." + tCharacterData["level"].AsString);

		return tCell;
	}

	private int getTableCellNumberAndIncrease(int tAddNumber = 0) {
		_createdCellCount += tAddNumber;
		return _createdCellCount;
	}

	#endregion ITableViewDataSource
}
