﻿using UnityEngine;
using System.Collections;
using BicJSON;

public class PopupCreateCharacter : MonoBehaviour {

	public CharacterTable uCharacterTable;
	public UnityEngine.UI.InputField uNameInput;

	public void open() {
		gameObject.SetActive (true);
		uNameInput.text = "";
	}

	public void onClickedCreate() {

		string tName = uNameInput.text;

		if (tName == string.Empty) {
			Debug.Log ("Empty name!!");
			return;
		}

		JSONValue tCharacterListData = DataController.loadFileToJson (ConstData.getDataFile (ConstData.DataFile.kCharacterList), true);

		bool tIsSameName = false;

		int tCharacterListCount = tCharacterListData.Count;
		for (int i = 0; !tIsSameName && i < tCharacterListCount; ++i) {
			if (tCharacterListData [i].AsString == tName) {
				tIsSameName = true;
				break;
			}
		}

		if (tIsSameName) {
			Debug.Log ("Same name!!");
			return;
		}

		tCharacterListData.Add (new JSONValue (tName));
		DataController.saveJsonToFile (tCharacterListData, ConstData.getDataFile (ConstData.DataFile.kCharacterList));

		JSONValue tCharacterInfo = new JSONValue ();
		tCharacterInfo ["level"].AsInt = 1;
		tCharacterInfo ["exp"].AsFloat = 0f;
		tCharacterInfo ["gold"].AsFloat = 0f;
		DataController.saveJsonToFile (tCharacterInfo, tName + ".data");

		uCharacterTable.reloadTable ();
		gameObject.SetActive (false);
	}

	public void onClickedCancel() {
		gameObject.SetActive (false);
	}
}
