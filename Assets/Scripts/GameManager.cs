﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using BicJSON;

public class GameManager : MonoBehaviour {
	public PopupResult uPopupResult;
	public GestureTest uGestureTest;
	public UIManager uUIManager;
	public Character uCharacter;
	private List<Monster> _monsters = new List<Monster>();
	public float uEndMapX;
	public GameObject uGround;
	public GameObject uMonsterContainer;
	public ActionManager uShakeActionManager;

	private ActionManager _actionManager;
	private Rect _mapRect;

	void Awake() {
		if (DataSingleton.getData ("CharacterName").AsString.Length <= 0) {
			SceneManager.LoadScene ("CharacterSelect");
		}

		if (Application.platform == RuntimePlatform.IPhonePlayer) {
			Application.targetFrameRate = 60;
		}

		_mapRect.Set (-500f, -120f, 3000f, 240f);
	}

	// Use this for initialization
	void Start () {
		_actionManager = GetComponent<ActionManager> ();
		uGestureTest.initGestures ("SwordGirl");

		foreach (Transform tMonsterTransform in uMonsterContainer.transform) {
			_monsters.Add (tMonsterTransform.GetComponent<Monster> ());
		}
	}

	public void shakeAttacked(float tRate) {
		uShakeActionManager.clearAllAction ();
		uGround.transform.localPosition = new Vector3 (Random.Range (-2f, 2f) * tRate + 1800f, Random.Range (-3f, 3f) * tRate + 125f);
		ActionData tShakeAction = uShakeActionManager.addAction ();
		tShakeAction.addMoveTo (uGround, 0.1f, new Vector3 (1800f, 125f));
		tShakeAction.onAction ();
	}

	public void damagedCharacter(float tDamage) {
		uUIManager.damagedCharacter (tDamage);
	}

	private float _totalTakeExp = 0f;
	public void damagedMonster(float tRestRate, Monster tMonster) {
		uUIManager.damagedMonster (tRestRate);
		if (tRestRate <= 0f) {
			float tExp = ExpTable.expForLevelDiff(tMonster.uExp, uCharacter.getLevel(), tMonster.uLevel);
			uCharacter.addExp (tExp);
			uUIManager.takeExp (tExp);
			_totalTakeExp += tExp;
			uCharacter.addGold (tMonster.uGold);
			uUIManager.takeGold (tMonster.uGold);

			HashSet<PropAndId> tPropAndIds = ItemTable.getDropList (tMonster.name);
			float tPropValue = Random.Range (0f, 1f);
			HashSet<JSONValue> tDropItems = new HashSet<JSONValue> ();
			foreach (PropAndId tPropAndId in tPropAndIds) {
				if (tPropAndId.prop > tPropValue) {
					JSONValue tDropItem = ItemTable.getDropItemInfo (tPropAndId.itemId);
					uUIManager.takeItem (tDropItem ["name"].AsString);
					tDropItems.Add (tDropItem);
				}
			}
			uCharacter.addItems (tDropItems);

			_monsters.Remove (tMonster);
			Destroy (tMonster.gameObject);

			if (_monsters.Count <= 0) {
				uCharacter.addExp (_totalTakeExp);
				uUIManager.takeExp (_totalTakeExp);
				uCharacter.saveData ();
				uPopupResult.open (_totalTakeExp);
			}
		}
	}

	public void traceGameManagerToCharacter() {
		_actionManager.clearAllAction ();
		ActionData tTraceAction = _actionManager.addAction ();
		tTraceAction.addMoveTo (gameObject, 0.5f, new Vector3(-uCharacter.getEndPosition ().x - 50f * uCharacter.viewDirection, transform.localPosition.y));
		tTraceAction.onAction ();
	}

	public Vector3 getMoveablePosition(Vector3 tPosition) {
		tPosition.x = Mathf.Clamp (tPosition.x, _mapRect.xMin, _mapRect.xMax);
		tPosition.y = Mathf.Clamp (tPosition.y, _mapRect.yMin, _mapRect.yMax);
		return tPosition;
	}

	public SpaceInfo getCharacterBodyInfo() {
		return uCharacter.getBodyInfo();
	}

	private Vector3 _keepedCharacterPosition;
	private float _keepViewDirection;
	public void keepCharacterPosition() {
		_keepedCharacterPosition = uCharacter.transform.localPosition;
		_keepViewDirection = uCharacter.viewDirection;
	}

	public void startMove(float tAngleRad) {
		uCharacter.moveAngle (tAngleRad);
		traceGameManagerToCharacter ();
	}

	public void moving(float tAngleRad) {
		uCharacter.moveAngle (tAngleRad);
		traceGameManagerToCharacter ();
	}

	public void stopMove() {
		keepCharacterPosition ();
		uCharacter.endMove ();
	}

	public void cancelMove() {
		uCharacter.transform.localPosition = _keepedCharacterPosition;
		uCharacter.viewDirection = _keepViewDirection;
		uCharacter.resetEndPosition ();
		uCharacter.transform.localScale = new Vector3 (Mathf.Abs (uCharacter.transform.localScale.x) * _keepViewDirection, uCharacter.transform.localScale.y, uCharacter.transform.localScale.z);
		uCharacter.endMove ();
	}

	public void useGesture(string tGestureType) {
		switch (tGestureType) {
		case "Attack2":
			uGestureTest.setEnableGesture ("765", false);
			uGestureTest.setEnableGesture ("567", false);
			uUIManager.startCoolAttack2 ();
			break;
		case "Attack3":
			uGestureTest.setEnableGesture ("123", false);
			uGestureTest.setEnableGesture ("321", false);
			uUIManager.startCoolAttack3 ();
			break;
		case "Skill1":
			uGestureTest.setEnableGesture ("6543210123456", false);
			uGestureTest.setEnableGesture ("6701234321076", false);
			uUIManager.startCoolSkill1 ();
			break;
		case "Skill2":
			uGestureTest.setEnableGesture ("107", false);
			uGestureTest.setEnableGesture ("345", false);
			uUIManager.startCoolSkill2 ();
			break;
		default :
			break;
		}
	}

	public void onGesture(string tGestureCode) {
		switch (tGestureCode) {
		case "6543210123456":
			uCharacter.gestureSkill1 (1f);
			break;
		case "6701234321076":
			uCharacter.gestureSkill1 (-1f);
			break;
		case "107":
			uCharacter.gestureSkill2 (1f);
			break;
		case "345":
			uCharacter.gestureSkill2 (-1f);
			break;
		case "765":
			uCharacter.gestureAttack2 (1f);
			break;
		case "567":
			uCharacter.gestureAttack2 (-1f);
			break;
		case "123":
			uCharacter.gestureAttack3 (1f);
			break;
		case "321":
			uCharacter.gestureAttack3 (-1f);
			break;
		case "0":
			uCharacter.gestureBack (-1f);
			break;
		case "4":
			uCharacter.gestureBack (1f);
			break;
		default :
			uCharacter.gestureAttack1 (0f);
			break;
		}
	}

	public void onAttackFromCharacter (SpaceInfo tSpaceInfo, float tDamageMin, float tDamageMax, ref HashSet<GameObject> tAttackedObject) {
		HashSet<Monster> tAttackedMonsters = new HashSet<Monster> ();
		for (int i = 0; i < _monsters.Count; ++i) {
			if (!tAttackedObject.Contains (_monsters [i].gameObject) && SpaceInfo.isOverlaped (tSpaceInfo, _monsters [i].getBodyInfo ())) {
				tAttackedMonsters.Add (_monsters [i]);
				tAttackedObject.Add (_monsters [i].gameObject);
			}
		}

		foreach (var tMonster in tAttackedMonsters) {
			tMonster.onDamaged (Random.Range(tDamageMin, tDamageMax));
		}
	}

	public void onAttackFromMonster (SpaceInfo tSpaceInfo, float tDamageMin, float tDamageMax, ref HashSet<GameObject> tAttackedObject) {
		if (!tAttackedObject.Contains (uCharacter.gameObject) && SpaceInfo.isOverlaped (tSpaceInfo, uCharacter.getBodyInfo ())) {
			tAttackedObject.Add (uCharacter.gameObject);
			uCharacter.onDamaged (tDamageMin);
		}
	}

	public void showDamageText(float tDamage, Vector3 tPosition) {
		uUIManager.showDamageText (tDamage, tPosition);
	}

	public void showLevelUpText(int tUpCount, Vector3 tPosition) {
		uUIManager.showLevelUpText (tUpCount, tPosition);
	}

	public void setCharacterLevelExp(int tLevel, float tIngExp, float tMaxExp) {
		uUIManager.setCharacterLevelExp (tLevel, tIngExp, tMaxExp);
	}

	public JSONValue getCharacterItem() {
		return uCharacter.getItems ();
	}

	public float getCharacterGold() {
		return uCharacter.getGold ();
	}

	public float sellItem(int tIndex) {
		JSONValue tSellInfo = uCharacter.sellItem (tIndex);
		uCharacter.addGold (tSellInfo["sellPrice"].AsFloat);
		uUIManager.takeGoldForSellItem (tSellInfo["name"].AsString, tSellInfo["sellPrice"].AsFloat);
		return uCharacter.getGold ();
	}

	public void equipItem(int tIndex) {
		uCharacter.equipItem (tIndex);
	}

	public void releaseItem(JSONValue tItemInfo) {
		uCharacter.releaseItem (tItemInfo);
	}

	public JSONValue getCharacterInfo() {
		return uCharacter.getInfo ();
	}
}

public class SpaceInfo
{
	public Vector3 position;
	public Rect rect;
	public float airPosition;
	public float height;
	public SpaceInfo() {
		position = new Vector3();
		rect = new Rect();
		airPosition = 0f;
		height = 0f;
	}

	public SpaceInfo(SpaceInfo tInfo) {
		position = tInfo.position;
		rect = tInfo.rect;
		airPosition = tInfo.airPosition;
		height = tInfo.height;
	}

	public static bool isOverlaped(SpaceInfo tInfoA, SpaceInfo tInfoB) {
		Rect tRectA = Rect.MinMaxRect(tInfoA.rect.xMin + tInfoA.position.x, tInfoA.rect.yMin + tInfoA.position.y,
			tInfoA.rect.xMax + tInfoA.position.x, tInfoA.rect.yMax + tInfoA.position.y);
		Rect tRectB = Rect.MinMaxRect(tInfoB.rect.xMin + tInfoB.position.x, tInfoB.rect.yMin + tInfoB.position.y,
			tInfoB.rect.xMax + tInfoB.position.x, tInfoB.rect.yMax + tInfoB.position.y);
		if (tRectA.Overlaps (tRectB)) {
			if (tInfoA.airPosition > tInfoB.airPosition + tInfoB.height || tInfoA.airPosition + tInfoA.height < tInfoB.airPosition) {
				return false;
			} else {
				return true;
			}
		} else {
			return false;
		}
	}
}
