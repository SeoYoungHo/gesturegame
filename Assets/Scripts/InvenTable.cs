﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Tacticsoft;
using BicJSON;

public class InvenTable : MonoBehaviour, ITableViewDataSource {
	public TableView uInvenTable;
	public InvenCell uInvenCellPrefabs;
	public PopupInven uPopupInven;

	public JSONValue _items = new JSONValue();
	int _createdCellCount = 0;

	#region Mono
	void Awake() {
		uInvenTable.dataSource = this;
	}

	// Update is called once per frame
	void Update () {
		
	}
	#endregion Mono


	public void reloadTable() {
		uInvenTable.ReloadData ();
		uInvenTable.scrollDistance = 0f;
	}

	#region ITableViewDataSource

	public int GetNumberOfRowsForTableView (TableView tableView)
	{
		if (_items.Count == 0) {
			return 0;
		} else {
			return (_items.Count - 1) / 10 + 1;
		}
	}

	public float GetHeightForRowInTableView (TableView tableView, int row)
	{
		return 60f;
	}

	public TableViewCell GetCellForRowInTableView (TableView tableView, int row)
	{
		InvenCell tCell = uInvenTable.GetReusableCell (uInvenCellPrefabs.reuseIdentifier) as InvenCell;
		if (tCell == null) {
			tCell = Instantiate<InvenCell> (uInvenCellPrefabs);
			tCell.name = "InvenCell_" + getTableCellNumberAndIncrease (1).ToString ();
			tCell.uPopupInven = uPopupInven;
		}

		List<JSONValue> tItemInfos = new List<JSONValue> ();
		for (int i = row * 10; i < _items.Count && i < row * 10 + 10; ++i) {
			tItemInfos.Add (_items [i]);
		}
		tCell.setCellInCells (tItemInfos, row * 10);

		return tCell;
	}

	private int getTableCellNumberAndIncrease(int tAddNumber = 0) {
		_createdCellCount += tAddNumber;
		return _createdCellCount;
	}

	#endregion ITableViewDataSource
}
