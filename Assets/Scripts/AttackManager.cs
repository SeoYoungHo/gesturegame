﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class AttackManager : MonoBehaviour {
	public ActionManager uActionManager;
	public GameManager uGameManager;
	[HideInInspector]
	public bool isCharacter;
	[HideInInspector]
	public float damageMin;
	[HideInInspector]
	public float damageMax;
	[HideInInspector]
	public SpaceInfo spaceInfo;
	public SpriteRenderer uImage;

	public void startAttack(float tKeepTime, Action tEndAction) {
//		transform.localPosition = spaceInfo.position - transform.parent.localPosition +
//		new Vector3 ((spaceInfo.rect.xMin + spaceInfo.rect.xMax) / 2f, (spaceInfo.rect.yMin + spaceInfo.rect.yMax) / 2f);
//		uImage.transform.localScale = new Vector3 (spaceInfo.rect.width / 10f, spaceInfo.rect.height / 10f, 1f);

		ActionData tDestroyAction = uActionManager.addAction ();
		tDestroyAction.addDelayTime (tKeepTime, () => {
			tEndAction();
			Destroy(gameObject);
		});
		tDestroyAction.onAction ();
	}

	private HashSet<GameObject> _attackedObject = new HashSet<GameObject> ();
	void Update() {
		if (isCharacter) {
			uGameManager.onAttackFromCharacter (spaceInfo, damageMin, damageMax, ref _attackedObject);
		} else {
			uGameManager.onAttackFromMonster (spaceInfo, damageMin, damageMax, ref _attackedObject);
		}
	}
}
