﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Tacticsoft;
using BicJSON;
using BigJamLib;

public class InvenCell : TableViewCell {
	public List<InvenCellInCell> uCells;
	[HideInInspector]
	public PopupInven uPopupInven;

	void Awake() {
		for (int i = 0; i < 10; ++i) {
			uCells [i].uInvenCell = this;
		}
	}

	public void setCellInCells(List<JSONValue> tItemInfos, int tBaseIndex) {
		for (int i = 0; i < 10; ++i) {
			if (tItemInfos.Count > i) {
				uCells [i].isEnable = true;
				uCells [i].uButton.image.color = new Color (1f, 1f, 1f, 1f);
				uCells [i].uButton.image.sprite = ResourcesCache.Sprite.loadSprite (tItemInfos [i] ["baseImage"].AsString, tItemInfos [i] ["detailImage"].AsString);
			} else {
				uCells [i].isEnable = false;
				uCells [i].uButton.image.color = new Color (1f, 1f, 1f, 0f);
			}

			uCells [i].index = tBaseIndex + i;
			uCells [i].uButton.enabled = uCells [i].isEnable;
		}
	}

	public void onClickedCellInCell(int tIndex) {
		uPopupInven.onClickedItem (tIndex);
	}
}
