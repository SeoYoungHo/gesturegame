﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PopupResult : MonoBehaviour {
	public UnityEngine.UI.Text uAddExpText;

	public void open(float tTotalTakeExp) {
		uAddExpText.text = "추가 경험치 : " + tTotalTakeExp.ToString("###0");

		gameObject.SetActive (true);
	}

	public void onClickedOkButton() {
		SceneManager.LoadScene ("Town1");
	}
}
