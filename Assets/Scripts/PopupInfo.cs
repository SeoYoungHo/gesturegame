﻿using UnityEngine;
using System.Collections;
using BicJSON;
using BigJamLib;

public class PopupInfo : MonoBehaviour {
	public PopupInven uPopupInven;

	public UnityEngine.UI.Text uNameText;
	public UnityEngine.UI.Text uLevelText;
	public UnityEngine.UI.Text uAtkText;
	public UnityEngine.UI.Button uWeapon1Button;

	public PopupItemInfo uPopupItemInfo;
	public GameManager uGameManager;

	private JSONValue _equipWeapon1;
	public void open(JSONValue tInfo) {
		uNameText.text = tInfo ["name"].AsString;
		int tLevel = tInfo ["level"].AsInt;
		uLevelText.text = "Lv. " + tLevel.ToString() + " (" + tInfo["exp"].AsFloat.ToString("###0") + "/" + ExpTable.getMaxExp(tLevel).ToString("###0") + ")";
		uAtkText.text = "공격력 : " + tInfo["atkMin"].AsFloat.ToString("###0") + " ~ " + tInfo["atkMax"].AsFloat.ToString("###0");

		_equipWeapon1 = tInfo ["equipWeapon1"];
		if (_equipWeapon1 ["name"].AsString != string.Empty) {
			uWeapon1Button.image.sprite = ResourcesCache.Sprite.loadSprite (_equipWeapon1 ["baseImage"].AsString, _equipWeapon1 ["detailImage"].AsString);
		} else {
			uWeapon1Button.image.sprite = null;
		}
		gameObject.SetActive (true);
	}

	public void onClickedCloseButton() {
		gameObject.SetActive (false);
	}

	public void onClickedWeapon1Button() {
		if (_equipWeapon1 ["name"].AsString != string.Empty) {
			uPopupItemInfo.open (_equipWeapon1);
		}
	}

	public void releaseItem(JSONValue tItemInfo) {
		uGameManager.releaseItem (tItemInfo);

		open (uGameManager.getCharacterInfo ());

		if (uPopupInven.gameObject.activeSelf) {
			uPopupInven.uInvenTable.reloadTable ();
		}
	}
}
