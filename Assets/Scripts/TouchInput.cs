﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using BigJamLib;

public class TouchInput : MonoBehaviour {

	public GameObject uLineContainer;
	public GestureTest uGestureTest;
	public float maxGestureTime;

	private List<GameObject> _lines = new List<GameObject> ();
	private List<Vector3> _touchedPositions = new List<Vector3> ();

	private bool _isTouchOn = false;

	private Vector2 _touchedStartPosition;
	private float _touchedStartTime;
	private bool _isMoving = false;
	private bool _isOnlyMoving = false;
	private float _moveAngleRad = 0f;
	private float _ingMoveTime = 0f;
	public void pointerDown(BaseEventData tEventData)
	{
		if (!_isTouchOn) {
			return;
		}

		PointerEventData tPointerData = (PointerEventData)tEventData;

		Vector2 tTouchedPosition = getTouchedPosition (tPointerData.position);

		GameObject tLine = Instantiate<GameObject> (ResourcesCache.GameObject.loadGameObject ("Prefabs/Line"));
		tLine.transform.SetParent (uLineContainer.transform, false);
		tLine.transform.localPosition = tTouchedPosition;
		_lines.Add (tLine);

		_touchedPositions.Add (tLine.transform.localPosition);

		uGestureTest.keepCharacterPosition ();
		_touchedStartPosition = tTouchedPosition;
		_touchedStartTime = Time.time;
		_isOnlyMoving = false;
	}

	public void drag(BaseEventData tEventData)
	{
		if (!_isTouchOn || _lines.Count <= 0) {
			return;
		}

		PointerEventData tPointerData = (PointerEventData)tEventData;

		Vector2 tTouchedPosition = getTouchedPosition (tPointerData.position);

		GameObject tBeforeLine = _lines [_lines.Count - 1];
		Vector3 tBeforePosition = tBeforeLine.transform.localPosition;
		Vector3 tAfterPosition = tTouchedPosition;
		Vector3 tSubPosition = tAfterPosition - tBeforePosition;
		float tAngle = Mathf.Atan2 (tSubPosition.y, tSubPosition.x) * Mathf.Rad2Deg;
		float tDistance = Mathf.Sqrt (tSubPosition.x * tSubPosition.x + tSubPosition.y * tSubPosition.y);
		tBeforeLine.transform.localEulerAngles = new Vector3 (0f, 0f, tAngle);
		tBeforeLine.transform.localPosition = tBeforePosition + tSubPosition * 0.5f;
		tBeforeLine.transform.localScale = new Vector3(tDistance / 10f, 0.1f, 1f);

		GameObject tLine = Instantiate<GameObject> (ResourcesCache.GameObject.loadGameObject ("Prefabs/Line"));
		tLine.transform.SetParent (uLineContainer.transform, false);
		tLine.transform.localPosition = tAfterPosition;
		_lines.Add (tLine);

		if (_isOnlyMoving) {
			tLine.GetComponent<SpriteRenderer> ().color = new Color(1f, 1f, 1f, 0f);
		}

		_touchedPositions.Add (tLine.transform.localPosition);

		Vector2 tCheckMoveGestureSubPosition = tTouchedPosition - _touchedStartPosition;

		if (!_isMoving) {
			float tCheckMoveGestureDistance = Mathf.Sqrt (tCheckMoveGestureSubPosition.x * tCheckMoveGestureSubPosition.x + tCheckMoveGestureSubPosition.y * tCheckMoveGestureSubPosition.y);
			if (tCheckMoveGestureDistance > 20f) {
				_moveAngleRad = Mathf.Atan2 (tCheckMoveGestureSubPosition.y, tCheckMoveGestureSubPosition.x);
				uGestureTest.startMove (_moveAngleRad);
				_isMoving = true;
			}
		} else {
			_moveAngleRad = Mathf.Atan2 (tCheckMoveGestureSubPosition.y, tCheckMoveGestureSubPosition.x);
		}
	}

	public void pointerUp(BaseEventData tEventData)
	{
		if (!_isTouchOn || _lines.Count <= 0) {
			return;
		}

		PointerEventData tPointerData = (PointerEventData)tEventData;

		GameObject tBeforeLine = _lines [_lines.Count - 1];
		Vector3 tBeforePosition = tBeforeLine.transform.localPosition;
		Vector3 tAfterPosition = getTouchedPosition (tPointerData.position);
		Vector3 tSubPosition = tAfterPosition - tBeforePosition;
		float tAngle = Mathf.Atan2 (tSubPosition.y, tSubPosition.x) * Mathf.Rad2Deg;
		float tDistance = Mathf.Sqrt (tSubPosition.x * tSubPosition.x + tSubPosition.y * tSubPosition.y);
		tBeforeLine.transform.localEulerAngles = new Vector3 (0f, 0f, tAngle);
		tBeforeLine.transform.localPosition = tBeforePosition + tSubPosition * 0.5f;
		tBeforeLine.transform.localScale = new Vector3(tDistance / 10f, 0.1f, 1f);

		_touchedPositions.Add (tAfterPosition);

		_isTouchOn = false;

		float tTouchedEndTime = Time.time;
		_isMoving = false;
		uGestureTest.checkGesture (_touchedPositions, tTouchedEndTime - _touchedStartTime);
	}

	private Vector2 getTouchedPosition(Vector2 tPointerPosition) {
		Vector2 tTouchedPosition = new Vector2(tPointerPosition.x, tPointerPosition.y);
		tTouchedPosition.x *= ScreenSizeSetting.ScreenSize.x/Screen.width;
		tTouchedPosition.y *= ScreenSizeSetting.ScreenSize.y/Screen.height;
		tTouchedPosition -= ScreenSizeSetting.ScreenSize/2f;

		return tTouchedPosition;
	}

	public void resetTouch() {
		int tLoopCount = _lines.Count;
		for (int i = 0; i < tLoopCount; ++i) {
			Destroy (_lines [i].gameObject);
		}
		_lines.Clear ();
		_touchedPositions.Clear ();

		_isTouchOn = true;
	}

	public void onGesture() {
		int tLoopCount = _lines.Count;
		for (int i = 0; i < tLoopCount; ++i) {
			_lines [i].transform.localScale = new Vector3 (_lines [i].transform.localScale.x, 0.5f, 1f);
			_lines [i].GetComponent<SpriteRenderer> ().color = Color.blue;
		}
	}

	void Start() {
		_isTouchOn = true;
	}

	void Update() {
		if (_isMoving) {
			_ingMoveTime += Time.deltaTime;
			if (_ingMoveTime >= 1f / 60f) {
				_ingMoveTime -= 1f / 60f;
				uGestureTest.moving (_moveAngleRad);
				if (!_isOnlyMoving && Time.time - _touchedStartTime > maxGestureTime) {
					_isOnlyMoving = true;
					int tLoopCount = _lines.Count;
					for (int i = 0; i < tLoopCount; ++i) {
						_lines [i].GetComponent<SpriteRenderer> ().color = new Color(1f, 1f, 1f, 0f);
					}
				}
			}
		}
	}
}
