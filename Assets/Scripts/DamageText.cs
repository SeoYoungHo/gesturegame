﻿using UnityEngine;
using System.Collections;

public class DamageText : MonoBehaviour {

	public ActionManager uActionManager;
	public UnityEngine.UI.Text uText;

	public void startShow() {
		ActionData tColorAction = uActionManager.addAction ();
		tColorAction.addDelayTime (0.5f, null);
		tColorAction.addFadeFromToText (uText, 0.3f, uText.color, new Color (uText.color.r, uText.color.g, uText.color.b, 0f));
		tColorAction.addCallFunc (() => {
			Destroy(gameObject);
		});
		tColorAction.onAction ();

		ActionData tMoveAction = uActionManager.addAction ();
		tMoveAction.addMoveTo (gameObject, 0.8f, transform.localPosition + new Vector3 (0f, 30f));
		tMoveAction.onAction ();
	}
}
