﻿using UnityEngine;
using System.Collections;

public class ExpTable {
	public static float getMaxExp(int tLevel) {
		float tMaxExp = 0f;

		switch (tLevel) {
		case 1: tMaxExp = 100f; break;
		case 2: tMaxExp = 165f; break;
		case 3: tMaxExp = 289f; break;
		case 4: tMaxExp = 503f; break;
		case 5: tMaxExp = 793f; break;
		case 6: tMaxExp = 1170f; break;
		case 7: tMaxExp = 1642f; break;
		case 8: tMaxExp = 2221f; break;
		case 9: tMaxExp = 2917f; break;
		case 10: tMaxExp = 3740f; break;
		case 11: tMaxExp = 4700f; break;
		case 12: tMaxExp = 5807f; break;
		case 13: tMaxExp = 7072f; break;
		case 14: tMaxExp = 8505f; break;
		case 15: tMaxExp = 10115f; break;
		case 16: tMaxExp = 12245f; break;
		case 17: tMaxExp = 14716f; break;
		case 18: tMaxExp = 17566f; break;
		case 19: tMaxExp = 20747f; break;
		case 20: tMaxExp = 24372f; break;	
		case 21: tMaxExp = 28379f; break;
		case 22: tMaxExp = 33894f; break;
		case 23: tMaxExp = 40302f; break;
		case 24: tMaxExp = 46226f; break;
		case 25: tMaxExp = 52833f; break;
		case 26: tMaxExp = 60015f; break;
		case 27: tMaxExp = 67968f; break;
		case 28: tMaxExp = 76750f; break;
		case 29: tMaxExp = 86226f; break;
		case 30: tMaxExp = 96628f; break;
		case 31: tMaxExp = 107800f; break;
		case 32: tMaxExp = 120001f; break;
		case 33: tMaxExp = 133299f; break;
		case 34: tMaxExp = 147501f; break;
		case 35: tMaxExp = 162911f; break;
		case 36: tMaxExp = 179312f; break;
		case 37: tMaxExp = 197038f; break;
		case 38: tMaxExp = 216165f; break;
		case 39: tMaxExp = 236434f; break;
		case 40: tMaxExp = 258230f; break;
		case 41: tMaxExp = 281266f; break;
		case 42: tMaxExp = 315904f; break;
		case 43: tMaxExp = 354082f; break;
		case 44: tMaxExp = 383824f; break;
		case 45: tMaxExp = 415580f; break;
		case 46: tMaxExp = 448962f; break;
		case 47: tMaxExp = 484514f; break;
		case 48: tMaxExp = 522335f; break;
		case 49: tMaxExp = 561984f; break;
		case 50: tMaxExp = 604067f; break;
		case 51: tMaxExp = 648103f; break;
		case 52: tMaxExp = 716560f; break;
		case 53: tMaxExp = 790939f; break;
		case 54: tMaxExp = 845822f; break;
		case 55: tMaxExp = 903794f; break;
		case 56: tMaxExp = 964233f; break;
		case 57: tMaxExp = 1027960f; break;
		case 58: tMaxExp = 1095101f; break;
		case 59: tMaxExp = 1164964f; break;
		case 60: tMaxExp = 1238450f; break;
		case 61: tMaxExp = 1314817f; break;
		case 62: tMaxExp = 1437008f; break;
		case 63: tMaxExp = 1568379f; break;
		case 64: tMaxExp = 1661122f; break;
		case 65: tMaxExp = 1758331f; break;
		case 66: tMaxExp = 1859079f; break;
		case 67: tMaxExp = 1964540f; break;
		case 68: tMaxExp = 2074870f; break;
		case 69: tMaxExp = 2189053f; break;
		case 70: tMaxExp = 2308362f; break;
		case 71: tMaxExp = 2431719f; break;
		case 72: tMaxExp = 2634089f; break;
		case 73: tMaxExp = 2849910f; break;
		case 74: tMaxExp = 3078405f; break;
		case 75: tMaxExp = 3321588f; break;
		case 76: tMaxExp = 3578562f; break;
		case 77: tMaxExp = 3851533f; break;
		case 78: tMaxExp = 4141266f; break;
		case 79: tMaxExp = 4446673f; break;
		case 80: tMaxExp = 4770268f; break;
		case 81: tMaxExp = 5110819f; break;
		case 82: tMaxExp = 5591464f; break;
		case 83: tMaxExp = 6104052f; break;
		case 84: tMaxExp = 6647626f; break;
		case 85: tMaxExp = 7225994f; break;
		default : tMaxExp = 999999999999f; break;
		}

		return tMaxExp;
	}

	public static float expForLevelDiff(float tExp, int tCharacterLevel, int tMonsterLevel) {
		float tTakeExp = tExp;
		int tLevelDiff = tMonsterLevel - tCharacterLevel;
		if (tLevelDiff >= 5) {
			
		} else if (tLevelDiff <= -10) {
			tTakeExp *= 0.005f;
		} else {
			switch (tLevelDiff) {
			case 4: tTakeExp *= 0.97f; break;
			case 3: tTakeExp *= 0.94f; break;
			case 2: tTakeExp *= 0.91f; break;
			case 1: tTakeExp *= 0.88f; break;
			case 0: tTakeExp *= 0.85f; break;
			case -1: tTakeExp *= 0.80f; break;
			case -2: tTakeExp *= 0.75f; break;
			case -3: tTakeExp *= 0.68f; break;
			case -4: tTakeExp *= 0.58f; break;
			case -5: tTakeExp *= 0.45f; break;
			case -6: tTakeExp *= 0.30f; break;
			case -7: tTakeExp *= 0.15f; break;
			case -8: tTakeExp *= 0.05f; break;	
			case -9: tTakeExp *= 0.01f; break;
			default : break;
			}
		}
		return tTakeExp;
	}
}
