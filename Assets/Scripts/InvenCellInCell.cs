﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class InvenCellInCell : MonoBehaviour {

	public UnityEngine.UI.Button uButton;
	[HideInInspector]
	public bool isEnable = false;
	[HideInInspector]
	public int index;
	[HideInInspector]
	public InvenCell uInvenCell;

	public void onClickedCellInCell() {
		uInvenCell.onClickedCellInCell (index);
	}
}
