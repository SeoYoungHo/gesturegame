﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TouchState {
	public HashSet<int> expectations = new HashSet<int> ();
	public HashSet<int> allowables = new HashSet<int> ();
}
