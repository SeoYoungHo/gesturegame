﻿using UnityEngine;
using System;
using System.Collections;

public class PopupCheck : MonoBehaviour {

	public UnityEngine.UI.Text uText;
	public UnityEngine.UI.Text uOkText;
	private Action _okAction;

	public void open(string tText, string tOkText, Action tOkAction) {
		uText.text = tText;
		uOkText.text = tOkText;
		_okAction = tOkAction;

		gameObject.SetActive (true);
	}

	public void onClickedOkButton() {
		_okAction ();
		gameObject.SetActive (false);
	}

	public void onClickedCancelButton() {
		gameObject.SetActive (false);
	}
}
