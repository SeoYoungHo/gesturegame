﻿using UnityEngine;
using System.Collections;
using Tacticsoft;

public class TextCell : TableViewCell {

	public UnityEngine.UI.Text uText;

	public void setText(string tText){
		uText.text = tText;
	}
}
