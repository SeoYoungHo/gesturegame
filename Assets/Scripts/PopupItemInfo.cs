﻿using UnityEngine;
using System.Collections;
using BicJSON;
using BigJamLib;

public class PopupItemInfo : MonoBehaviour {

	public UnityEngine.UI.Text uNameText;
	public UnityEngine.UI.Text uDescriptionText;
	public UnityEngine.UI.Image uImage;

	public PopupCheck uPopupCheck;
	public PopupInven uPopupInven;
	public PopupInfo uPopupInfo;

	private int _index;
	private JSONValue _itemInfo;
	public void open(int tIndex, JSONValue tItemInfo) {
		_index = tIndex;
		open (tItemInfo);
	}

	public void onClickedSellButton() {
		uPopupCheck.open ("정말 판매 하겠습니까?", "판매", () => {
			uPopupInven.sellItem(_index);
			gameObject.SetActive(false);
		});
	}

	public void onClickedEquipButton() {
		uPopupInven.equipItem (_index);
		gameObject.SetActive (false);
	}

	public void open(JSONValue tItemInfo) {
		_itemInfo = tItemInfo;

		uImage.sprite = ResourcesCache.Sprite.loadSprite (_itemInfo ["baseImage"].AsString, _itemInfo ["detailImage"].AsString);
		uNameText.text = _itemInfo ["name"].AsString;
		string tType = _itemInfo ["type"].AsString;
		switch (tType) {
		case "weapon1":
			uDescriptionText.text = "공격력 : " + _itemInfo ["atkMin"].AsFloat.ToString ("####") + " ~ " + _itemInfo ["atkMax"].AsFloat.ToString ("####");
			break;
		default :
			uDescriptionText.text = "";
			break;
		}

		gameObject.SetActive (true);
	}

	public void onClickedReleaseButton() {
		uPopupInfo.releaseItem (_itemInfo);
		gameObject.SetActive (false);
	}

	public void onClickedClose() {
		gameObject.SetActive (false);
	}
}
