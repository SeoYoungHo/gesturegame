﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using BigJamLib;

public class UIManager : MonoBehaviour {

	public GestureTest uGestureTest;
	public GameManager uGameManager;

	private ActionManager _actionManager;
	void Start() {
		_actionManager = GetComponent<ActionManager> ();
	}

	public UnityEngine.UI.Image uAttack2CoolImage;
	public UnityEngine.UI.Image uAttack3CoolImage;
	public UnityEngine.UI.Image uSkill1CoolImage;
	public UnityEngine.UI.Image uSkill2CoolImage;

	private float _restAttack2Cool = 0f;
	private float _restAttack3Cool = 0f;
	private float _restSkill1Cool = 0f;
	private float _restSkill2Cool = 0f;
	private float _maxAttack2Cool = 2f;
	private float _maxAttack3Cool = 2f;
	private float _maxSkill1Cool = 8f;
	private float _maxSkill2Cool = 5f;

	public void startCoolAttack2() {
		uAttack2CoolImage.color = Color.gray;
		_restAttack2Cool = _maxAttack2Cool;
		refreshCoolAttack2 ();
	}

	private void refreshCoolAttack2() {
		uAttack2CoolImage.fillAmount = 1f - _restAttack2Cool/_maxAttack2Cool;
	}

	public void startCoolAttack3() {
		uAttack3CoolImage.color = Color.gray;
		_restAttack3Cool = _maxAttack3Cool;
		refreshCoolAttack3 ();
	}

	private void refreshCoolAttack3() {
		uAttack3CoolImage.fillAmount = 1f - _restAttack3Cool/_maxAttack3Cool;
	}

	public void startCoolSkill1() {
		uSkill1CoolImage.color = Color.gray;
		_restSkill1Cool = _maxSkill1Cool;
		refreshCoolSkill1 ();
	}

	private void refreshCoolSkill1() {
		uSkill1CoolImage.fillAmount = 1f - _restSkill1Cool/_maxSkill1Cool;
	}

	public void startCoolSkill2() {
		uSkill2CoolImage.color = Color.gray;
		_restSkill2Cool = _maxSkill2Cool;
		refreshCoolSkill2 ();
	}

	private void refreshCoolSkill2() {
		uSkill2CoolImage.fillAmount = 1f - _restSkill2Cool/_maxSkill2Cool;
	}

	void Update() {
		float tDeltaTime = Time.deltaTime;
		if (_restAttack2Cool > 0f) {
			_restAttack2Cool = Mathf.Clamp(_restAttack2Cool - tDeltaTime, 0f, _maxAttack2Cool);
			refreshCoolAttack2 ();
			if (_restAttack2Cool <= 0f) {
				uAttack2CoolImage.color = Color.white;
				uGestureTest.setEnableGesture ("765", true);
				uGestureTest.setEnableGesture ("567", true);
			}
		}

		if (_restAttack3Cool > 0f) {
			_restAttack3Cool = Mathf.Clamp(_restAttack3Cool - tDeltaTime, 0f, _maxAttack3Cool);
			refreshCoolAttack3 ();
			if (_restAttack3Cool <= 0f) {
				uAttack3CoolImage.color = Color.white;
				uGestureTest.setEnableGesture ("123", true);
				uGestureTest.setEnableGesture ("321", true);
			}
		}

		if (_restSkill1Cool > 0f) {
			_restSkill1Cool = Mathf.Clamp(_restSkill1Cool - tDeltaTime, 0f, _maxSkill1Cool);
			refreshCoolSkill1 ();
			if (_restSkill1Cool <= 0f) {
				uSkill1CoolImage.color = Color.white;
				uGestureTest.setEnableGesture ("6543210123456", true);
				uGestureTest.setEnableGesture ("6701234321076", true);
			}
		}

		if (_restSkill2Cool > 0f) {
			_restSkill2Cool = Mathf.Clamp(_restSkill2Cool - tDeltaTime, 0f, _maxSkill2Cool);
			refreshCoolSkill2 ();
			if (_restSkill2Cool <= 0f) {
				uSkill2CoolImage.color = Color.white;
				uGestureTest.setEnableGesture ("107", true);
				uGestureTest.setEnableGesture ("345", true);
			}
		}
	}

	public RectTransform uCharacterHp;
	public RectTransform uMonsterHp;

	public void damagedCharacter(float tDamage) {
		uCharacterHp.sizeDelta = new Vector2 (uCharacterHp.sizeDelta.x, Mathf.Clamp(uCharacterHp.sizeDelta.y - tDamage, 0f, 250f));
		if (uCharacterHp.sizeDelta.y <= 0f) {
			SceneManager.LoadScene ("Town1");
		}
	}

	public void damagedMonster(float tRestRate) {
		uMonsterHp.sizeDelta = new Vector2 (Mathf.Clamp(250f * tRestRate, 0f, 250f), uMonsterHp.sizeDelta.y);
	}

	public GameObject uGameEffect999;
	public void showDamageText(float tDamage, Vector3 tPosition) {
		DamageText tDamageText = Instantiate<GameObject> (ResourcesCache.GameObject.loadGameObject ("Prefabs/DamageText")).GetComponent<DamageText> ();
		tDamageText.transform.SetParent (uGameEffect999.transform, false);
		tDamageText.transform.localPosition = tPosition;
		tDamageText.uText.text = tDamage.ToString ("###0");
		tDamageText.startShow ();
	}

	public void showLevelUpText(int tUpCount, Vector3 tPosition) {
		ActionData tLevelUpAction = _actionManager.addAction ();
		for (int i = 0; i < tUpCount; ++i) {
			tLevelUpAction.addDelayTime (0.2f, () => {
				DamageText tText = Instantiate<GameObject> (ResourcesCache.GameObject.loadGameObject ("Prefabs/DamageText")).GetComponent<DamageText> ();
				tText.uText.color = Color.white;
				tText.transform.SetParent (uGameEffect999.transform, false);
				tText.transform.localPosition = tPosition + new Vector3(Random.Range(-3f, 3f), Random.Range(-3f, 3f));
				tText.uText.text = "LEVEL UP!!";
				tText.startShow ();
			});
		}
		tLevelUpAction.onAction ();
	}

	public RectTransform uCharacterExpGraph;
	public UnityEngine.UI.Text uCharacterLevelText;
	public void setCharacterLevelExp(int tLevel, float tIngExp, float tMaxExp) {
		uCharacterLevelText.text = "Lv. " + tLevel.ToString() + " (" + tIngExp.ToString("##0") + "/" + tMaxExp.ToString("##0") + ")";
		uCharacterExpGraph.sizeDelta = new Vector2 (530f*tIngExp/tMaxExp, 6f);
	}

	public TextTable uTextTable;
	public void takeExp(float tExp) {
		uTextTable.addText ("경험치 " + tExp.ToString("###0") + " 를 획득하였습니다.");
	}

	public void takeGold(float tGold) {
		uTextTable.addText (uTextTable.boxColor(uTextTable.Color32Yellow, tGold.ToString("###0") + " 골드를 획득하였습니다."));
	}

	public void takeGoldForSellItem(string tItemName, float tGold) {
		uTextTable.addText (uTextTable.boxColor(uTextTable.Color32Yellow, tItemName + "을 팔아 " + tGold.ToString("###0") + " 골드를 획득하였습니다."));
	}

	public void takeItem(string tItemName) {
		uTextTable.addText (uTextTable.boxColor(uTextTable.Color32Cyan, tItemName + " 를(을) 획득하였습니다."));
	}

	public PopupInfo uPopupInfo;
	public void onClickedInfo() {
		uPopupInfo.open (uGameManager.getCharacterInfo());
	}

	public PopupInven uPopupInven;
	public void onClickedInven() {
		uPopupInven.openPopupWithInitItems (uGameManager.getCharacterItem(), uGameManager.getCharacterGold());
	}

	public void onClickedSkill() {

	}
}
