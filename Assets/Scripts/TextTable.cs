﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Tacticsoft;

public class TextTable : MonoBehaviour, ITableViewDataSource {
	public TableView uTextTable;
	public TextCell uTextCellPrefabs;

	List<string> _testTexts = new List<string>();
	int _createdCellCount = 0;

	public string boxColor(Color32 tColor, string tText) {
		return "<color=#" + getHexFromColor(tColor) + ">" + tText + "</color>";
	}

	private string getHexFromColor(Color32 tColor) {
		return tColor.r.ToString ("X2") + tColor.g.ToString ("X2") + tColor.b.ToString ("X2") + tColor.a.ToString ("X2");
	}

	public void addText(string tText) {
		_testTexts.Add (" " + tText);
		if (_testTexts.Count > 100) {
			_testTexts.RemoveAt (0);
		}
		reloadTable ();
	}

	public readonly Color32 Color32Green = new Color32(0, 255, 0, 255);
	public readonly Color32 Color32Red = new Color32(255, 0, 0, 255);
	public readonly Color32 Color32Blue = new Color32(0, 0, 255, 255);
	public readonly Color32 Color32Purple = new Color32(255, 0, 255, 255);
	public readonly Color32 Color32Yellow = new Color32(255, 255, 0, 255);
	public readonly Color32 Color32Cyan = new Color32(0, 255, 255, 255);
	public readonly Color32 Color32Gray = new Color32(127, 127, 127, 255);

	#region Mono
	void Awake() {
//		Color32 tGreen = new Color32 (0, 255, 0, 255);
//
//		for (int i = 0; i < 50; ++i) {
//			_testTexts.Add (" <color=#" + getHexFromColor(tGreen) + ">" + i.ToString() + "</color>");
//		}

		uTextTable.dataSource = this;
	}

	// Use this for initialization
	void Start () {
		reloadTable ();
	}
	
	// Update is called once per frame
	void Update () {
//		Debug.Log (uTextTable.scrollDistance.ToString());
	}
	#endregion Mono


	private void reloadTable() {
		uTextTable.ReloadData ();
		uTextTable.scrollDistance = uTextTable.scrollableDistance;
	}

	#region ITableViewDataSource

	public int GetNumberOfRowsForTableView (TableView tableView)
	{
		return _testTexts.Count;
	}

	public float GetHeightForRowInTableView (TableView tableView, int row)
	{
		return 22f;
	}

	public TableViewCell GetCellForRowInTableView (TableView tableView, int row)
	{
		TextCell tCell = uTextTable.GetReusableCell (uTextCellPrefabs.reuseIdentifier) as TextCell;
		if (tCell == null) {
			tCell = Instantiate<TextCell> (uTextCellPrefabs);
			tCell.name = "TextCell_" + getTableCellNumberAndIncrease (1).ToString ();
		}

		tCell.setText (_testTexts [row]);

		return tCell;
	}

	private int getTableCellNumberAndIncrease(int tAddNumber = 0) {
		_createdCellCount += tAddNumber;
		return _createdCellCount;
	}

	#endregion ITableViewDataSource
}
