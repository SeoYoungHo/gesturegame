﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class GestureTest : MonoBehaviour {

	public GameManager uGameManager;
	public TouchInput uTouchInput;

	private float _minDistance = 30f;
	private ActionManager _actionManager;

	public void checkGesture(List<Vector3> tTouchedPositions, float tGestureTime) {
		int tTouchedPositionsCount = tTouchedPositions.Count;
		if (tTouchedPositionsCount < 2) {
			uGameManager.onGesture ("");
			uTouchInput.resetTouch ();
//			Debug.Log ("touched position count little !!");
			return;
		}

//		for (int i = 0; i < tTouchedPositionsCount; ++i) {
//			Debug.Log ("position : " + tTouchedPositions[i].x.ToString() + " , " + tTouchedPositions[i].y.ToString());
//		}

		List<int> tAngleValueSections = new List<int> ();
		for (int i = 1; i < tTouchedPositionsCount; ++i) {
			int tCheckedAngle = -1;
			for (int j = i - 1; tCheckedAngle == -1 && j >= 0; --j) {
				Vector3 tSubVec = tTouchedPositions [i] - tTouchedPositions[j];
				float tDistance = Mathf.Sqrt (tSubVec.x * tSubVec.x + tSubVec.y * tSubVec.y);
//				Debug.Log ("i : " + i.ToString() + " | j : " + j.ToString() + " | distance : " + tDistance.ToString());
				if (tDistance < _minDistance) {
//					Debug.Log ("continue");
					continue;
				}

				float tAngle = Mathf.Atan2 (tSubVec.y, tSubVec.x) * Mathf.Rad2Deg;
				tCheckedAngle = angleToAngleValue (tAngle);
			}

			if (tCheckedAngle != -1) {
				tAngleValueSections.Add (tCheckedAngle);
			}
		}

		int tAngleValueSectionsCount = tAngleValueSections.Count;
		if (tAngleValueSectionsCount <= 0) {
			uGameManager.onGesture ("");
			uTouchInput.resetTouch ();
//			Debug.Log ("angle value section count little !!");
			return;
		}

//		string tLogString = "Angle : ";
//		for (int i = 0; i < tAngleValueSectionsCount; ++i) {
//			tLogString += tAngleValueSections[i].ToString() + " ";
//		}
//		Debug.Log (tLogString);

		int tFindIndex = -1;
		int tGesturesCount = _gestures.Count;
		for (int i = 0; tFindIndex == -1 && i < tGesturesCount; ++i) {
			if (!_isEnablesForGesture [i]) {
				continue;
			}

//			Debug.Log ("gesture " + i.ToString() + " count : " + _gestures[i].Count.ToString());
			int tCheckingIndex = 0;
			bool tIsSuccess = false;
			for (int j = 0; tCheckingIndex != -1 && j < tAngleValueSectionsCount; ++j) {
				if (j > 0) {
					checkGestureForRevisionAngle (i, ref tCheckingIndex, tAngleValueSections [j - 1], tAngleValueSections [j], ref tIsSuccess);
				}

				if (tCheckingIndex != -1) {
					checkGestureForAngle (i, ref tCheckingIndex, tAngleValueSections [j], ref tIsSuccess);
				}
			}

			if (tCheckingIndex != -1 && tIsSuccess && _gesturableTime[i] >= tGestureTime) {
				tFindIndex = i;
//				Debug.Log ("find : " + tFindIndex.ToString());
			}

//			if (i == 0) {
//				Debug.Break ();
//			}
		}

		if (tFindIndex != -1) {
			uGameManager.cancelMove ();
//			uGestureImages [tFindIndex].gameObject.SetActive (true);
			uTouchInput.onGesture();
			ActionData tActionData = _actionManager.addAction ();
			tActionData.addDelayTime (0.1f, () => {
//				uGestureImages[tFindIndex].gameObject.SetActive(false);
				uTouchInput.resetTouch();
			});
			tActionData.onAction ();

			uGameManager.onGesture (_gestureCode[tFindIndex]);
		} else {
			uGameManager.stopMove ();
			uTouchInput.resetTouch ();
//			Debug.Log ("not found matched gesture !!");
		}
	}

	private void checkGestureForRevisionAngleClockwise(int tFromAngle, int tToAngle, int i, ref int tCheckingIndex, ref bool tIsSuccess) {
		List<int> tRevisionedSections = getClockwiseRevision (tFromAngle, tToAngle);
		for (int k = 0; tCheckingIndex != -1 && k < tRevisionedSections.Count; ++k) {
			checkGestureForAngle (i, ref tCheckingIndex, tRevisionedSections [k], ref tIsSuccess);
		}
	}

	private void checkGestureForRevisionAngleCounterclockwise(int tFromAngle, int tToAngle, int i, ref int tCheckingIndex, ref bool tIsSuccess) {
		List<int> tRevisionedSections = getCounterclockwiseRevision (tFromAngle, tToAngle);
		for (int k = 0; tCheckingIndex != -1 && k < tRevisionedSections.Count; ++k) {
			checkGestureForAngle (i, ref tCheckingIndex, tRevisionedSections [k], ref tIsSuccess);
		}
	}

	private void checkGestureForRevisionAngle(int i, ref int tCheckingIndex, int tFromAngle, int tToAngle, ref bool tIsSuccess) {
		if (_gestureCode[i] == "6543210123456") {
			if (tCheckingIndex >= 1 && tCheckingIndex <= 5) {
				checkGestureForRevisionAngleClockwise (tFromAngle, tToAngle, i, ref tCheckingIndex, ref tIsSuccess);
			} else if (tCheckingIndex >= 7 && tCheckingIndex <= 11) {
				checkGestureForRevisionAngleCounterclockwise (tFromAngle, tToAngle, i, ref tCheckingIndex, ref tIsSuccess);
			}
		} else if (_gestureCode[i] == "6701234321076") {
			if (tCheckingIndex >= 1 && tCheckingIndex <= 5) {
				checkGestureForRevisionAngleCounterclockwise (tFromAngle, tToAngle, i, ref tCheckingIndex, ref tIsSuccess);
			} else if (tCheckingIndex >= 7 && tCheckingIndex <= 11) {
				checkGestureForRevisionAngleClockwise (tFromAngle, tToAngle, i, ref tCheckingIndex, ref tIsSuccess);
			}
		} else if (_gestureCode[i] == "107") {
			if (tCheckingIndex == 1) {
				checkGestureForRevisionAngleClockwise (tFromAngle, tToAngle, i, ref tCheckingIndex, ref tIsSuccess);
			}
		} else if (_gestureCode[i] == "345") {
			if (tCheckingIndex == 1) {
				checkGestureForRevisionAngleCounterclockwise (tFromAngle, tToAngle, i, ref tCheckingIndex, ref tIsSuccess);
			}
		} else if (_gestureCode[i] == "765") {
			if (tCheckingIndex == 1) {
				checkGestureForRevisionAngleClockwise (tFromAngle, tToAngle, i, ref tCheckingIndex, ref tIsSuccess);
			}
		} else if (_gestureCode[i] == "567") {
			if (tCheckingIndex == 1) {
				checkGestureForRevisionAngleCounterclockwise (tFromAngle, tToAngle, i, ref tCheckingIndex, ref tIsSuccess);
			}
		} else if (_gestureCode[i] == "123") {
			if (tCheckingIndex == 1) {
				checkGestureForRevisionAngleCounterclockwise (tFromAngle, tToAngle, i, ref tCheckingIndex, ref tIsSuccess);
			}
		} else if (_gestureCode[i] == "321") {
			if (tCheckingIndex == 1) {
				checkGestureForRevisionAngleClockwise (tFromAngle, tToAngle, i, ref tCheckingIndex, ref tIsSuccess);
			}
		}
	}

	private void checkGestureForAngle(int tGestureIndex, ref int tCheckingIndex, int tAngleValue, ref bool tIsSuccess) {
//		Debug.Log ("checkGestureForAngle : " + tGestureIndex.ToString() + " , " + tCheckingIndex.ToString());
		if (_gestures [tGestureIndex] [tCheckingIndex].expectations.Contains (tAngleValue)) {
//			if (tGestureIndex == 2) {
//				Debug.Log ("in judge : " + tAngleValue.ToString () + " | checking index : " + tCheckingIndex.ToString ());
//			}
			++tCheckingIndex;
			if (_gestures [tGestureIndex].Count <= tCheckingIndex) {
				if (!tIsSuccess) {
					tIsSuccess = true;
				}
				--tCheckingIndex;
//				if (tGestureIndex == 2) {
//					Debug.Log ("max : " + tAngleValue.ToString () + " | checking index : " + tCheckingIndex.ToString ());
//				}
			}
		} else if (_gestures [tGestureIndex] [tCheckingIndex].allowables.Contains (tAngleValue)) {
//			if (tGestureIndex == 2) {
//				Debug.Log ("allow judge : " + tAngleValue.ToString () + " | checking index : " + tCheckingIndex.ToString ());
//			}
		} else {
//			if (tGestureIndex == 2) {
//				Debug.Log ("out judge : " + tAngleValue.ToString () + " | checking index : " + tCheckingIndex.ToString ());
//			}
			tCheckingIndex = -1;
		}
	}

	private List<int> getClockwiseRevision(int tBefore, int tAfter) {
		List<int> tSectionRevision = new List<int> ();
		tSectionRevision.Add (tBefore);
		tSectionRevision.Add (tAfter);
		bool tIsNothing = false;
		while (!tIsNothing) {
			tIsNothing = true;
			for (int i = 0; i < tSectionRevision.Count - 1; ++i) {
				if (getClockwiseRevisionAngleDiff(tSectionRevision [i], tSectionRevision [i + 1]) >= 9) {
					tIsNothing = false;
					int tAddAngleValue = tSectionRevision [i] - 8;
					if (tAddAngleValue < 0) {
						tAddAngleValue += 72;
					}
					tSectionRevision.Insert (i + 1, tAddAngleValue);
				}
			}
		}
		tSectionRevision.RemoveAt (0);
		tSectionRevision.RemoveAt (tSectionRevision.Count - 1);
		return tSectionRevision;
	}

	private int getClockwiseRevisionAngleDiff(int tBefore, int tAfter) {
		if (Mathf.Abs (tBefore - tAfter) >= 36) {
			if (tBefore < tAfter) {
				tBefore += 72;
			} else {
				tAfter += 72;
			}
		}

		if (Mathf.Abs (tBefore - tAfter) < 9) {
			return 0;
		}

		return tBefore - tAfter;
	}

	private List<int> getCounterclockwiseRevision(int tBefore, int tAfter) {
		List<int> tSectionRevision = new List<int> ();
		tSectionRevision.Add (tBefore);
		tSectionRevision.Add (tAfter);
		bool tIsNothing = false;
		while (!tIsNothing) {
			tIsNothing = true;
			for (int i = 0; i < tSectionRevision.Count - 1; ++i) {
				if (getCounterclockwiseRevisionAngleDiff(tSectionRevision [i], tSectionRevision [i + 1]) >= 9) {
					tIsNothing = false;
					int tAddAngleValue = tSectionRevision [i] + 8;
					if (tAddAngleValue >= 72) {
						tAddAngleValue -= 72;
					}
					tSectionRevision.Insert (i + 1, tAddAngleValue);
				}
			}
		}
		tSectionRevision.RemoveAt (0);
		tSectionRevision.RemoveAt (tSectionRevision.Count - 1);
		return tSectionRevision;
	}

	private int getCounterclockwiseRevisionAngleDiff(int tBefore, int tAfter) {
		if (Mathf.Abs (tBefore - tAfter) >= 36) {
			if (tBefore < tAfter) {
				tBefore += 72;
			} else {
				tAfter += 72;
			}
		}

		if (Mathf.Abs (tBefore - tAfter) < 9) {
			return 0;
		}

		return tAfter - tBefore;
	}

	public void setEnableGesture(string tCode, bool tIsEnable) {
		int tIndex = _gestureCode.FindIndex (x => x == tCode);
		_isEnablesForGesture [tIndex] = tIsEnable;
	}

	private List<List<TouchState>> _gestures = new List<List<TouchState>> ();
	private List<string> _gestureCode = new List<string> ();
	private List<bool> _isEnablesForGesture = new List<bool>();
	private List<float> _gesturableTime = new List<float> ();
	public void initGestures(string tCharacterType) {
		{
			List<TouchState> tStates1 = new List<TouchState> ();

			addTouchStates (RepresentativeAngle.k270, RepresentativeAngle.k0, ref tStates1);
			addTouchStates (RepresentativeAngle.k45, RepresentativeAngle.k270, ref tStates1);

			_gestures.Add (tStates1);
			_isEnablesForGesture.Add (true);
			_gesturableTime.Add (0.7f);
			_gestureCode.Add ("6543210123456");
		}

		{
			List<TouchState> tStates1 = new List<TouchState> ();

			addTouchStates (RepresentativeAngle.k270, RepresentativeAngle.k315, ref tStates1);
			addTouchStates (RepresentativeAngle.k0, RepresentativeAngle.k180, ref tStates1);
			addTouchStates (RepresentativeAngle.k135, RepresentativeAngle.k0, ref tStates1);
			addTouchStates (RepresentativeAngle.k315, RepresentativeAngle.k270, ref tStates1);

			_gestures.Add (tStates1);
			_isEnablesForGesture.Add (true);
			_gesturableTime.Add (0.7f);
			_gestureCode.Add ("6701234321076");
		}

		{
			List<TouchState> tStates1 = new List<TouchState> ();

			addTouchStates (RepresentativeAngle.k45, RepresentativeAngle.k0, ref tStates1);
			addTouchState (RepresentativeAngle.k315, ref tStates1);

			_gestures.Add (tStates1);
			_isEnablesForGesture.Add (true);
			_gesturableTime.Add (0.5f);
			_gestureCode.Add ("107");
		}

		{
			List<TouchState> tStates1 = new List<TouchState> ();

			addTouchStates (RepresentativeAngle.k135, RepresentativeAngle.k225, ref tStates1);

			_gestures.Add (tStates1);
			_isEnablesForGesture.Add (true);
			_gesturableTime.Add (0.5f);
			_gestureCode.Add ("345");
		}

		{
			List<TouchState> tStates1 = new List<TouchState> ();

			addTouchStates (RepresentativeAngle.k315, RepresentativeAngle.k225, ref tStates1);

			_gestures.Add (tStates1);
			_isEnablesForGesture.Add (true);
			_gesturableTime.Add (0.5f);
			_gestureCode.Add ("765");
		}

		{
			List<TouchState> tStates1 = new List<TouchState> ();

			addTouchStates (RepresentativeAngle.k225, RepresentativeAngle.k315, ref tStates1);

			_gestures.Add (tStates1);
			_isEnablesForGesture.Add (true);
			_gesturableTime.Add (0.5f);
			_gestureCode.Add ("567");
		}

		{
			List<TouchState> tStates1 = new List<TouchState> ();

			addTouchStates (RepresentativeAngle.k45, RepresentativeAngle.k135, ref tStates1);

			_gestures.Add (tStates1);
			_isEnablesForGesture.Add (true);
			_gesturableTime.Add (0.5f);
			_gestureCode.Add ("123");
		}

		{
			List<TouchState> tStates1 = new List<TouchState> ();

			addTouchStates (RepresentativeAngle.k135, RepresentativeAngle.k45, ref tStates1);

			_gestures.Add (tStates1);
			_isEnablesForGesture.Add (true);
			_gesturableTime.Add (0.5f);
			_gestureCode.Add ("321");
		}

		{
			List<TouchState> tStates1 = new List<TouchState> ();

			addTouchState (RepresentativeAngle.k180, ref tStates1);

			_gestures.Add (tStates1);
			_isEnablesForGesture.Add (true);
			_gesturableTime.Add (0.2f);
			_gestureCode.Add ("4");
		}

		{
			List<TouchState> tStates1 = new List<TouchState> ();

			addTouchState (RepresentativeAngle.k0, ref tStates1);

			_gestures.Add (tStates1);
			_isEnablesForGesture.Add (true);
			_gesturableTime.Add (0.2f);
			_gestureCode.Add ("0");
		}

		uTouchInput.maxGestureTime = 0f;
		for (int i = 0; i < _gesturableTime.Count; ++i) {
			if (uTouchInput.maxGestureTime < _gesturableTime [i]) {
				uTouchInput.maxGestureTime = _gesturableTime [i];
			}
		}
	}

	private void addTouchState(RepresentativeAngle tAngle, ref List<TouchState> tStates) {
		TouchState tState = new TouchState ();
		addRepresentativeAngle (tAngle, ref tState);
		tStates.Add (tState);
	}

	private void addTouchStates(RepresentativeAngle tFromAngle, RepresentativeAngle tToAngle, ref List<TouchState> tStates) {
		int tStart, tEnd, tStep;
		if ((int)tFromAngle > (int)tToAngle) {
			tStart = (int)tFromAngle;
			tEnd = (int)tToAngle - 1;
			tStep = -1;
		} else {
			tStart = (int)tFromAngle;
			tEnd = (int)tToAngle + 1;
			tStep = 1;
		}

		for (int i = tStart; i != tEnd; i += tStep) {
			addTouchState ((RepresentativeAngle)i, ref tStates);
		}
	}

	private int angleToAngleValue(float tAngle) {
		//		Debug.Log ("in angle : " + tAngle.ToString());
		int tReturnValue = -1;
		if (tAngle < 0f) {
			tAngle += 360f;
		}

		tReturnValue = Mathf.Clamp(Mathf.FloorToInt (tAngle / 5f), 0, 71);

		return tReturnValue;
	}

	enum RepresentativeAngle {
		k0 = 0,
		k45,
		k90,
		k135,
		k180,
		k225,
		k270,
		k315
	}

	private void addExpectationsFromTo(int tMin, int tMax, ref TouchState tState) {
		for (int i = tMin; i <= tMax; ++i) {
			tState.expectations.Add (i);
		}
	}

	private void addAllowablesFromTo(int tMin, int tMax, ref TouchState tState) {
		for (int i = tMin; i <= tMax; ++i) {
			tState.allowables.Add (i);
		}
	}

	private void addRepresentativeAngle(RepresentativeAngle tAngle, ref TouchState tState) {
		switch (tAngle) {
		case RepresentativeAngle.k0:
			addAllowablesFromTo (6, 17, ref tState);
			addExpectationsFromTo (0, 5, ref tState);
			addExpectationsFromTo (66, 71, ref tState);
			addAllowablesFromTo (54, 65, ref tState);
			break;
		case RepresentativeAngle.k45:
			addAllowablesFromTo (15, 26, ref tState);
			addExpectationsFromTo (3, 14, ref tState);
			addAllowablesFromTo (0, 2, ref tState);
			addAllowablesFromTo (63, 71, ref tState);
			break;
		case RepresentativeAngle.k90:
			addAllowablesFromTo (0, 11, ref tState);
			addExpectationsFromTo (12, 23, ref tState);
			addAllowablesFromTo (24, 35, ref tState);
			break;
		case RepresentativeAngle.k135:
			addAllowablesFromTo (9, 20, ref tState);
			addExpectationsFromTo (21, 32, ref tState);
			addAllowablesFromTo (33, 44, ref tState);
			break;
		case RepresentativeAngle.k180:
			addAllowablesFromTo (18, 29, ref tState);
			addExpectationsFromTo (30, 41, ref tState);
			addAllowablesFromTo (42, 53, ref tState);
			break;
		case RepresentativeAngle.k225:
			addAllowablesFromTo (27, 38, ref tState);
			addExpectationsFromTo (39, 50, ref tState);
			addAllowablesFromTo (51, 62, ref tState);
			break;
		case RepresentativeAngle.k270:
			addAllowablesFromTo (36, 47, ref tState);
			addExpectationsFromTo (48, 59, ref tState);
			addAllowablesFromTo (60, 71, ref tState);
			break;
		case RepresentativeAngle.k315:
			addAllowablesFromTo (45, 56, ref tState);
			addExpectationsFromTo (57, 68, ref tState);
			addAllowablesFromTo (69, 71, ref tState);
			addAllowablesFromTo (0, 8, ref tState);

			break;
		default:
			break;
		}
	}

	public void keepCharacterPosition() {
		uGameManager.keepCharacterPosition ();
	}

	public void startMove(float tAngleRad) {
		uGameManager.startMove (tAngleRad);
	}

	public void moving(float tAngleRad) {
		uGameManager.moving (tAngleRad);
	}

	public void stopMove() {
		uGameManager.stopMove ();
	}

	public void cancelMove() {
		uGameManager.cancelMove ();
	}

	void Awake() {
		
	}

	void Start() {
		_actionManager = GetComponent<ActionManager> ();
	}
}
