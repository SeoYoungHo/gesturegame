﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BigJamLib;
using System;
using BicJSON;

public class Character : MonoBehaviour {

	private float _baseY;
	public float viewDirection = 1f;
	public SpriteRenderer uImage;
	public ActionManager uNextGestureActionManager;
	public GameManager uGameManager;

	enum CharacterState {
		kStand = 0,
		kMove,
		kBack,
		kDamaged,
		kAttack1,
		kAttack2,
		kAttack3,
		kSkill1,
		kSkill2
	}

	private Dictionary<CharacterState, List<Sprite>> _animationsDic;
	private Vector3 _endPosition;
	public Vector3 getEndPosition() {
		return _endPosition;
	}
	public void resetEndPosition() {
		_endPosition = transform.localPosition;
	}
	void Awake() {
		setSavedData (DataSingleton.getData ("CharacterName").AsString);
		_baseY = uImage.transform.localPosition.y;
		_animationsDic = new Dictionary<CharacterState, List<Sprite>> ();

		resetEndPosition ();
	}

	private int _ingAnimationIndex;
	private List<Sprite> _ingAnimationSprites;
	private float _ingAnimationTime;

	private CharacterState _state;
	private ActionManager _actionManager;
	private ActionManager _imageActionManager;
	private HashSet<AttackManager> _attackManagers = new HashSet<AttackManager> ();
//	private BoxCollider2D _bodyCollider;
	private Rect _bodyRect;
	private float _bodyAirPosition;
	private float _bodyHeight;

	public SpaceInfo getBodyInfo() {
		SpaceInfo tBodyInfo = new SpaceInfo ();
		tBodyInfo.position = _endPosition;
		tBodyInfo.rect = _bodyRect;
		tBodyInfo.airPosition = _bodyAirPosition;
		tBodyInfo.height = _bodyHeight;
		return tBodyInfo;
	}

	private string _name;
	private int _level;
	private float _exp;
	private float _gold;
	private float _atkMin;
	private float _atkMax;
	private JSONValue _equipWeapon1;
	private JSONValue _items;
	private JSONValue _savedData;
	public void setSavedData(string tName) {
		_name = tName;
		_savedData = DataController.loadFileToJson (_name + ".data", true);
		_exp = _savedData ["exp"].AsFloat;
		_gold = _savedData ["gold"].AsFloat;
		_items = _savedData ["items"];

		_level = _savedData ["level"].AsInt;
		_equipWeapon1 = _savedData ["equipWeapon1"];
		resetAtk ();

		uGameManager.setCharacterLevelExp(_level, _exp, ExpTable.getMaxExp(_level));
//		Debug.Log ("level : " + _level.ToString() + " , exp : " + _exp.ToString() + " , gold : " + _gold.ToString());
	}

	public void saveData() {
		DataController.saveJsonToFile (_savedData, _name + ".data");
	}

	public JSONValue getInfo() {
		JSONValue tInfo = new JSONValue ();
		tInfo ["name"].AsString = _name;
		tInfo ["level"].AsInt = _level;
		tInfo ["exp"].AsFloat = _exp;
		tInfo ["atkMin"].AsFloat = _atkMin;
		tInfo ["atkMax"].AsFloat = _atkMax;
		tInfo ["equipWeapon1"] = _equipWeapon1;
		return tInfo;
	}

	public int getLevel() {
		return _level;
	}

	public void resetAtk() {
		_atkMin = LevelToValue.levelToAtk (_level);

		_atkMax = _atkMin * 1.1f;
		_atkMin *= 0.9f;

		float tWeaponAtkMin, tWeaponAtkMax;
		if (_equipWeapon1 ["name"].AsString != string.Empty) {
			float tPowValue = 1f;//PowValue.getPowValue (1.1f, _level);
			tWeaponAtkMin = _equipWeapon1 ["atkMin"].AsFloat * tPowValue;
			tWeaponAtkMax = _equipWeapon1 ["atkMax"].AsFloat * tPowValue;
//			Debug.Log ("weapon1 : " + _equipWeapon1["name"].AsString + tWeaponAtkMin.ToString("###0") + " ~ " + tWeaponAtkMax.ToString("###0"));
		} else {
			tWeaponAtkMin = tWeaponAtkMax = 0f;
		}

		_atkMin += tWeaponAtkMin;
		_atkMax += tWeaponAtkMax;
	}

	public void equipItem(int tIndex) {
		Debug.Log ("equip Item : " + tIndex.ToString());
		JSONValue tItemInfo = _items [tIndex];
		_items.RemoveByIndex (tIndex);
		if (tItemInfo ["type"].AsString == "weapon1") {
			if (_equipWeapon1 ["name"].AsString != string.Empty) {
				_items.Add (_equipWeapon1);
				_savedData ["items"] = _items;
			}
			_equipWeapon1 = tItemInfo;
			_savedData ["equipWeapon1"] = _equipWeapon1;
		}

		resetAtk ();
	}

	public void releaseItem(JSONValue tItemInfo) {
		_items.Add (tItemInfo);
		if (tItemInfo ["type"].AsString == "weapon1") {
			_equipWeapon1 = new JSONValue ();
			_equipWeapon1.Clear ();
			_savedData ["equipWeapon1"] = _equipWeapon1;
		}
		_savedData ["items"] = _items;

		resetAtk ();
	}

	public void addExp(float tExp) {
		_exp += tExp;
		float tMaxExp = ExpTable.getMaxExp (_level);
		int tLevelUpCount = 0;
		while (_exp >= tMaxExp) {
			_exp -= tMaxExp;
			++_level;
			++tLevelUpCount;
			tMaxExp = ExpTable.getMaxExp (_level);
		}

		_savedData ["exp"].AsFloat = _exp;
		uGameManager.setCharacterLevelExp (_level, _exp, tMaxExp);
		if (tLevelUpCount > 0) {
			_savedData ["level"].AsInt = _level;

			resetAtk ();

			uGameManager.showLevelUpText (tLevelUpCount, transform.localPosition + uGameManager.transform.localPosition + new Vector3 (0f, 90f));
		}
	}

	public void addGold(float tGold) {
		_gold += tGold;
		_savedData ["gold"].AsFloat = _gold;
	}

	public float getGold() {
		return _gold;
	}

	public void addItems(HashSet<JSONValue> tItems) {
		foreach (JSONValue tItem in tItems) {
			_items.Add (tItem);
		}
		_savedData ["items"] = _items;
	}

	public JSONValue getItems() {
		return _items;
	}

	public JSONValue sellItem(int tIndex) {
		JSONValue tReturnInfo = new JSONValue ();
		tReturnInfo["sellPrice"].AsFloat = _items [tIndex] ["sellPrice"].AsFloat;
		tReturnInfo["name"].AsString = _items [tIndex] ["name"].AsString;
		_items.RemoveByIndex (tIndex);
		_savedData ["items"] = _items;
		return tReturnInfo;
	}

	void Start () {
		List<Sprite> tStandAnimations = new List<Sprite> ();
		for (int i = 0; i <= 7; ++i) {
			tStandAnimations.Add (ResourcesCache.Sprite.loadSprite ("Characters/stand", "stand_" + i.ToString()));
			if (i == 4 || i == 0) {
				for (int j = 0; j < 6; ++j) {
					tStandAnimations.Add (ResourcesCache.Sprite.loadSprite ("Characters/stand", "stand_" + i.ToString()));
				}
			}
		}
		_animationsDic.Add (CharacterState.kStand, tStandAnimations);

		List<Sprite> tDashAnimations = new List<Sprite> ();
		for (int i = 0; i <= 3; ++i) {
			tDashAnimations.Add (ResourcesCache.Sprite.loadSprite ("Characters/dash", "dash_" + i.ToString()));
		}
		_animationsDic.Add (CharacterState.kMove, tDashAnimations);

		List<Sprite> tBackAnimations = new List<Sprite> ();
		for (int i = 0; i <= 2; ++i) {
			tBackAnimations.Add (ResourcesCache.Sprite.loadSprite ("Characters/back", "back_" + i.ToString()));
		}
		_animationsDic.Add (CharacterState.kBack, tBackAnimations);

		List<Sprite> tAttackedAnimations = new List<Sprite> ();
		for (int i = 0; i <= 1; ++i) {
			for (int j = 0; j < 3; ++j) {
				tAttackedAnimations.Add (ResourcesCache.Sprite.loadSprite ("Characters/hit", "hit_" + i.ToString ()));
			}
		}
		_animationsDic.Add (CharacterState.kDamaged, tAttackedAnimations);

		List<Sprite> tAttack1Animations = new List<Sprite> ();
		for (int i = 0; i <= 2; ++i) {
			tAttack1Animations.Add (ResourcesCache.Sprite.loadSprite ("Characters/atkStab1", "atkStab1_" + i.ToString()));
		}
		_animationsDic.Add (CharacterState.kAttack1, tAttack1Animations);

		List<Sprite> tAttack2Animations = new List<Sprite> ();
		for (int i = 0; i <= 4; ++i) {
			tAttack2Animations.Add (ResourcesCache.Sprite.loadSprite ("Characters/atkLow1", "atkLow1_" + i.ToString()));
		}
		_animationsDic.Add (CharacterState.kAttack2, tAttack2Animations);

		List<Sprite> tAttack3Animations = new List<Sprite> ();
		for (int i = 0; i <= 3; ++i) {
			tAttack3Animations.Add (ResourcesCache.Sprite.loadSprite ("Characters/atkCut1", "atkCut1_" + i.ToString()));
		}
		_animationsDic.Add (CharacterState.kAttack3, tAttack3Animations);

		List<Sprite> tSkill1Animations = new List<Sprite> ();
		for (int i = 0; i <= 12; ++i) {
			tSkill1Animations.Add (ResourcesCache.Sprite.loadSprite ("Characters/atkSpinDouble1", "atkSpinDouble1_" + i.ToString()));
		}
		_animationsDic.Add (CharacterState.kSkill1, tSkill1Animations);

		List<Sprite> tSkill2Animations = new List<Sprite> ();
		for (int i = 0; i <= 9; ++i) {
			tSkill2Animations.Add (ResourcesCache.Sprite.loadSprite ("Characters/atkSpin1", "atkSpin1_" + i.ToString()));
		}
		_animationsDic.Add (CharacterState.kSkill2, tSkill2Animations);

		_bodyRect.Set (-15f, -10f, 30f, 20f);
		_bodyAirPosition = 0f;
		_bodyHeight = 85f;
		_actionManager = GetComponent<ActionManager> ();
		_imageActionManager = uImage.GetComponent<ActionManager> ();
		_state = CharacterState.kStand;
		startStateAnimation ();
	}

	private float _animationTerm = 0.1f;
	void Update () {
		_ingAnimationTime += Time.deltaTime;
		if (_ingAnimationTime >= _animationTerm) {
			_ingAnimationTime -= _animationTerm;
			_ingAnimationIndex = (_ingAnimationIndex + 1) % _ingAnimationSprites.Count;
			uImage.sprite = _ingAnimationSprites [_ingAnimationIndex];
			if (_ingAnimationIndex == 0 && _state != CharacterState.kStand && _state != CharacterState.kMove) {
				endGestureAction ();
			} else if (_state == CharacterState.kAttack1) {
				if (_ingAnimationIndex == 1) {
					SpaceInfo tSpaceInfo = new SpaceInfo ();
					tSpaceInfo.position = _endPosition + new Vector3(80f * viewDirection, 0f);
					tSpaceInfo.rect.Set (-100f, -20f, 200f, 40f);
					tSpaceInfo.airPosition = 0f;
					tSpaceInfo.height = 50f;
					onAttackManager (_atkMin * 1f, _atkMax * 1f, tSpaceInfo, 0.1f);
				}
			} else if (_state == CharacterState.kAttack2) {
				if (_ingAnimationIndex == 2) {
					SpaceInfo tSpaceInfo = new SpaceInfo ();
					tSpaceInfo.position = _endPosition + new Vector3(85f * viewDirection, 0f);
					tSpaceInfo.rect.Set (-75f, -20f, 150f, 40f);
					tSpaceInfo.airPosition = 0f;
					tSpaceInfo.height = 50f;
					onAttackManager (_atkMin * 5f, _atkMax * 5f, tSpaceInfo, 0.1f);
				}
			} else if (_state == CharacterState.kAttack3) {
				if (_ingAnimationIndex == 1) {
					SpaceInfo tSpaceInfo = new SpaceInfo ();
					tSpaceInfo.position = _endPosition + new Vector3(20f * viewDirection, 0f);
					tSpaceInfo.rect.Set (-125f, -20f, 250f, 40f);
					tSpaceInfo.airPosition = 0f;
					tSpaceInfo.height = 50f;
					onAttackManager (_atkMin * 3f, _atkMax * 3f, tSpaceInfo, 0.1f);
				}
			} else if (_state == CharacterState.kSkill1) {
				if (_ingAnimationIndex == 1) {
					SpaceInfo tSpaceInfo = new SpaceInfo ();
					tSpaceInfo.position = _endPosition + new Vector3(25f * viewDirection, 0f);
					tSpaceInfo.rect.Set (-130f, -20f, 260f, 40f);
					tSpaceInfo.airPosition = 0f;
					tSpaceInfo.height = 50f;
					onAttackManager (_atkMin * 2f, _atkMax * 2f, tSpaceInfo, 0.1f);
				} else if (_ingAnimationIndex == 3) {
					SpaceInfo tSpaceInfo = new SpaceInfo ();
					tSpaceInfo.position = _endPosition + new Vector3(50f * viewDirection, 0f);
					tSpaceInfo.rect.Set (-75f, -20f, 150f, 40f);
					tSpaceInfo.airPosition = 0f;
					tSpaceInfo.height = 50f;
					onAttackManager (_atkMin * 2f, _atkMax * 2f, tSpaceInfo, 0.1f);
				} else if (_ingAnimationIndex == 5) {
					SpaceInfo tSpaceInfo = new SpaceInfo ();
					tSpaceInfo.position = _endPosition + new Vector3(15f * viewDirection, 0f);
					tSpaceInfo.rect.Set (-95f, -20f, 190f, 40f);
					tSpaceInfo.airPosition = 0f;
					tSpaceInfo.height = 50f;
					onAttackManager (_atkMin * 2f, _atkMax * 2f, tSpaceInfo, 0.1f);
				} else if (_ingAnimationIndex == 8) {
					SpaceInfo tSpaceInfo = new SpaceInfo ();
					tSpaceInfo.position = _endPosition + new Vector3(10f * viewDirection, 0f);
					tSpaceInfo.rect.Set (-120f, -20f, 240f, 40f);
					tSpaceInfo.airPosition = 0f;
					tSpaceInfo.height = 50f;
					onAttackManager (_atkMin * 2f, _atkMax * 2f, tSpaceInfo, 0.1f);
				} else if (_ingAnimationIndex == 10) {
					SpaceInfo tSpaceInfo = new SpaceInfo ();
					tSpaceInfo.position = _endPosition + new Vector3(5f * viewDirection, 0f);
					tSpaceInfo.rect.Set (-80f, -20f, 160f, 40f);
					tSpaceInfo.airPosition = 0f;
					tSpaceInfo.height = 50f;
					onAttackManager (_atkMin * 2f, _atkMax * 2f, tSpaceInfo, 0.1f);
				}
			} else if (_state == CharacterState.kSkill2) {
				if (_ingAnimationIndex == 5) {
					SpaceInfo tSpaceInfo = new SpaceInfo ();
					tSpaceInfo.position = _endPosition + new Vector3(15f * viewDirection, 0f);
					tSpaceInfo.rect.Set (-120f, -20f, 240f, 40f);
					tSpaceInfo.airPosition = 0f;
					tSpaceInfo.height = 50f;
					onAttackManager (_atkMin * 7f, _atkMax * 7f, tSpaceInfo, 0.1f);
				}
			}
		}
	}

	private void onAttackManager(float tDamageMin, float tDamageMax, SpaceInfo tSpaceInfo, float tKeepTime) {
		AttackManager tAttack = Instantiate<GameObject> (ResourcesCache.GameObject.loadGameObject ("Prefabs/AttackManager")).GetComponent<AttackManager> ();
		tAttack.transform.SetParent (transform, false);
		tAttack.damageMin = tDamageMin;
		tAttack.damageMax = tDamageMax;
		tAttack.isCharacter = true;
		tAttack.spaceInfo = new SpaceInfo (tSpaceInfo);
		tAttack.uGameManager = uGameManager;
		_attackManagers.Add (tAttack);

		tAttack.startAttack (tKeepTime, () => {
			_attackManagers.Remove(tAttack);
		});
	}

	private void startStateAnimation() {
		_ingAnimationSprites = _animationsDic [_state];
		_ingAnimationIndex = 0;
		_ingAnimationTime = 0;
		uImage.sprite = _ingAnimationSprites [_ingAnimationIndex];
	}

	private void setNextGesture(Action<float> tNextGestureFunc, float tNextGestureViewDirection) {
		_nextGestureFunc = tNextGestureFunc;
		_nextGestureViewDirection = tNextGestureViewDirection;
		uNextGestureActionManager.clearAllAction ();
		ActionData tAction = uNextGestureActionManager.addAction ();
		tAction.addDelayTime (0.2f, () => {
			_nextGestureFunc = null;
		});
		tAction.onAction ();
	}

	private void endGestureAction() {
//		if (_state == CharacterState.kBack || _state == CharacterState.kSkill2) {
			uGameManager.traceGameManagerToCharacter ();
//		}
		_animationTerm = 0.1f;
		_state = CharacterState.kStand;
		transform.localScale = new Vector3 (viewDirection, 1f, 1f);
		uImage.transform.localScale = new Vector3 (2f, 2f, 1f);
		if (_nextGestureFunc != null) {
			Action<float> tKeepNextGesture = _nextGestureFunc;
			_nextGestureFunc = null;
			tKeepNextGesture (_nextGestureViewDirection);
		} else {
			startStateAnimation ();
		}
	}

	private Action<float> _nextGestureFunc = null;
	private float _nextGestureViewDirection = 1f;

	public void gestureBack(float tViewDirection) {
		if (_state != CharacterState.kStand) {
			setNextGesture (gestureBack, tViewDirection);
			if (_state != CharacterState.kDamaged && _state != CharacterState.kBack) {
				_actionManager.clearAllAction ();
				_endPosition = transform.localPosition;
				foreach (var tAttack in _attackManagers) {
					Destroy (tAttack.gameObject);
				}
				_attackManagers.Clear ();
				uImage.transform.localPosition = new Vector3 (0f, _baseY);
				endGestureAction ();
			}
			return;
		}

		_state = CharacterState.kBack;
		uImage.transform.localScale = new Vector3 (1f, 1f, 1f);
		viewDirection = tViewDirection;
		transform.localScale = new Vector3 (viewDirection, 1f, 1f);
		startStateAnimation ();

		Vector3 tAfterPosition = uGameManager.getMoveablePosition(transform.localPosition + new Vector3(-70f * viewDirection, 0f));

		ActionData tMove1Action = _actionManager.addAction ();
		tMove1Action.addMoveFromTo (gameObject, 0.3f, transform.localPosition, tAfterPosition);
		tMove1Action.onAction ();

		ActionData tMove2Action = _actionManager.addAction ();
		tMove2Action.addMoveTo (uImage.gameObject, 0.15f, new Vector3(0f, _baseY) + new Vector3(0f, 15f), 4);
		tMove2Action.addMoveFromTo (uImage.gameObject, 0.15f, new Vector3(0f, _baseY) + new Vector3(0f, 15f), new Vector3(0f, _baseY), 5);
		tMove2Action.onAction ();

		_endPosition = tAfterPosition;
	}

	public void gestureAttack1(float tViewDirection) {
		if (_state != CharacterState.kStand) {
			setNextGesture (gestureAttack1, tViewDirection);
			return;
		}
		_animationTerm = 0.15f;
		_state = CharacterState.kAttack1;
		startStateAnimation ();
	}

	public void gestureAttack2(float tViewDirection) {
		if (_state != CharacterState.kStand) {
			setNextGesture (gestureAttack2, tViewDirection);
			return;
		}

		_state = CharacterState.kAttack2;
		viewDirection = tViewDirection;
		transform.localScale = new Vector3 (viewDirection, 1f, 1f);
		startStateAnimation ();

		uGameManager.useGesture ("Attack2");
	}

	public void gestureAttack3(float tViewDirection) {
		if (_state != CharacterState.kStand) {
			setNextGesture (gestureAttack3, tViewDirection);
			return;
		}

		_state = CharacterState.kAttack3;
		viewDirection = tViewDirection;
		transform.localScale = new Vector3 (viewDirection, 1f, 1f);
		startStateAnimation ();

		uGameManager.useGesture ("Attack3");
	}

	public void gestureSkill1(float tViewDirection) {
		if (_state != CharacterState.kStand) {
			setNextGesture (gestureSkill1, tViewDirection);
			return;
		}

		_state = CharacterState.kSkill1;
		uImage.transform.localScale = new Vector3 (1f, 1f, 1f);
		viewDirection = tViewDirection;
		transform.localScale = new Vector3 (viewDirection, 1f, 1f);
		startStateAnimation ();

		uGameManager.useGesture ("Skill1");
	}

	public void gestureSkill2(float tViewDirection) {
		if (_state != CharacterState.kStand) {
			setNextGesture (gestureSkill2, tViewDirection);
			return;
		}

		_state = CharacterState.kSkill2;
		viewDirection = tViewDirection;
		transform.localScale = new Vector3 (viewDirection, 1f, 1f);
		startStateAnimation ();

		Vector3 tAfterPosition = uGameManager.getMoveablePosition (transform.localPosition + new Vector3 (150f * viewDirection, 0f));

		ActionData tMoveAction = _actionManager.addAction ();
		tMoveAction.addMoveTo (gameObject, 0.4f * (tAfterPosition.x - transform.localPosition.x) / 150f, tAfterPosition);
		tMoveAction.onAction ();

		_endPosition = tAfterPosition;

		uGameManager.useGesture ("Skill2");
	}

	private float _moveSpeed = 3f;
	public void moveAngle(float tAngleRad) {
		if (_state != CharacterState.kMove && _state != CharacterState.kStand) {
			return;
		}

		if (_state == CharacterState.kStand) {
			_state = CharacterState.kMove;
			uImage.transform.localScale = new Vector3 (1f, 1f, 1f);
			startStateAnimation ();
		}

		float tAngleDeg = tAngleRad * Mathf.Rad2Deg;
		if (tAngleDeg < 0f) {
			tAngleDeg += 360f;
		}

		if (tAngleDeg > 90f && tAngleDeg < 270f) {
			viewDirection = -1f;
		} else {
			viewDirection = 1f;
		}

		transform.localScale = new Vector3 (1f * viewDirection, 1f, 1f);

		Vector3 tAfterPosition = uGameManager.getMoveablePosition(transform.localPosition + new Vector3 (Mathf.Cos(tAngleRad), Mathf.Sin(tAngleRad)) * _moveSpeed);

		transform.localPosition = tAfterPosition;
		_endPosition = tAfterPosition;
	}

	public void endMove() {
		if (_state != CharacterState.kMove) {
			return;
		}

		endGestureAction ();
	}

	public void onDamaged(float tDamage) {
		if (_state != CharacterState.kStand && _state != CharacterState.kDamaged) {
			_actionManager.clearAllAction ();
			_endPosition = transform.localPosition;
			foreach (var tAttack in _attackManagers) {
				Destroy (tAttack.gameObject);
			}
			_attackManagers.Clear ();
			uImage.transform.localPosition = new Vector3 (0f, _baseY);
		}

		_state = CharacterState.kDamaged;
		uImage.transform.localScale = new Vector3 (1f, 1f, 1f);
		startStateAnimation ();
		ActionData tAttackedDelay = _actionManager.addAction ();
		tAttackedDelay.addDelayTime (1f, () => {
			_nextGestureFunc = null;
			_state = CharacterState.kStand;
		});
		tAttackedDelay.onAction ();

		uGameManager.damagedCharacter (tDamage);
		uGameManager.shakeAttacked (5f);
	}
}
