﻿using UnityEngine;
using System.Collections;
using BicJSON;

public class PopupInven : MonoBehaviour {

	public InvenTable uInvenTable;
	public UnityEngine.UI.Text uHaveGoldText;
	public PopupItemInfo uPopupItemInfo;
	public PopupCheck uPopupCheck;
	public GameManager uGameManager;
	public PopupInfo uPopupInfo;

	public void openPopupWithInitItems(JSONValue tItems, float tHaveGold) {
		uInvenTable._items = tItems;
		uHaveGoldText.text = "보유 골드 : " + tHaveGold.ToString("###0");
		gameObject.SetActive (true);
		uInvenTable.reloadTable ();
	}

	public void onClickedCloseButton() {
		uPopupCheck.gameObject.SetActive (false);
		uPopupItemInfo.gameObject.SetActive (false);
		gameObject.SetActive (false);
	}

	public void onClickedItem(int tIndex) {
		uPopupItemInfo.open (tIndex, uInvenTable._items [tIndex]);
	}

	public void sellItem(int tIndex) {
		uHaveGoldText.text = "보유 골드 : " + uGameManager.sellItem (tIndex).ToString("###0");

		uInvenTable.reloadTable ();
	}

	public void equipItem(int tIndex) {
		uGameManager.equipItem (tIndex);

		uInvenTable.reloadTable ();

		if (uPopupInfo.gameObject.activeSelf) {
			uPopupInfo.open (uGameManager.getCharacterInfo ());
		}
	}
}
