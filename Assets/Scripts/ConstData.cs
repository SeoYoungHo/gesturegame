﻿using UnityEngine;
using System.Collections;
using System;

public class ConstData {
	public enum DataFile {
		kCharacterList = 1,
		kCharacterInfo_unuse
	}

	public static string getDataFile(DataFile tFile) {
		string tReturnValue = string.Empty;
		switch (tFile) {
		case DataFile.kCharacterList:
			tReturnValue = "CharacterList.data";
			break;
		case DataFile.kCharacterInfo_unuse:
			tReturnValue = "캐릭터이름.data";
			break;
		default :
			throw new Exception ("");
		}
		return tReturnValue;
	}
}
