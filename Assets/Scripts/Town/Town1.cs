﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using BicJSON;

public class Town1 : MonoBehaviour {

	void Awake() {
		if (DataSingleton.getData ("CharacterName").AsString.Length <= 0) {
			SceneManager.LoadScene ("CharacterSelect");
		}
	}

	public void onClickedToDungeon() {
		SceneManager.LoadScene ("Dungeon1");
	}
}
